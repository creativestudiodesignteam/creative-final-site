<?php

namespace App\Controllers\Site;

use \App\Controllers\Site\Common;
use App\Models\Entities\Blog;

/**
 * Description of Home
 *
 * @author oseas¹
 */
class Blogs extends Common{

    protected $pagelink = '';

    public function index($page = 1) {
        $this->start_session();
        $this->view->page = 'header-blog';
        $this->view->titlehead = 'Blog Creative';
        $this->image = '/templates/site/default/img/singlework/summer-1.jpg';
        $this->title_post = $this->view->titlehead;

        //paginacao
        $total = Blog::where('status', '=', 'a')
                     ->where('posting_date', '<=', date('Y-m-d'))
                     ->orderBy('posting_date', 'DESC')->count();

        //seta a quantidade de itens por página
        $registros =  6;

        //calcula o número de páginas arredondando o resultado para cima

        $numPaginas = ceil($total/$registros);

        //variavel para calcular o início da visualização com base na página atual
        $inicio = ($registros*$page) - $registros;

        $this->view->blogs = Blog::where('status', '=', 'a')
                                 ->where('posting_date', '<=', date('Y-m-d'))
                                 ->orderBy('posting_date', 'DESC')->limit($registros)->offset($inicio)->get();

        $this->view->numPaginas = $numPaginas;
        $this->view->paginacao = $page;
        $this->view->registros = $registros;
        $this->view->max_links = 10;
        $this->view->total = $total;

        $this->render('index', $this->folder, $this->page);
    }

    public function post($text, $id){
        $this->start_session();
        $this->view->page = 'header-blog';
        $this->view->obj = Blog::find($id);
        $this->view->titlehead = $this->view->obj->title;
        $this->image = $this->view->obj->image;
        $this->title_post = $this->view->obj->title;
        $this->view->blogs = Blog::where('status', '=', 'a')
                                    ->where('idblog', '<>', $id)
                                    ->where('posting_date', '<=', date('Y-m-d'))
                                    ->orderBy('posting_date', 'DESC')
                                    ->limit(3)
                                    ->get();

        $this->render('post', $this->folder, $this->page);
    }
}
