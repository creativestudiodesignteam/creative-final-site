<!DOCTYPE HTML>
<html>
<head>
	<?php include("includes/head.php");?>
	
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
</head>
<body class="main-page pc">

<a href="https://wa.me/5511999706346?text=Olá%20tudo%20bem%20?%20Eu%20gostaria%20de%20solicitar%20um%20orçamento%20referente%20aos%20serviços%20da%20Agência." Style="position:fixed;width:60px;height:60px;bottom:40px;right:40px;background-color:#25d366;color:#FFF;border-radius:50px;text-align:center;font-size:30px;box-shadow: 1px 1px 2px #888;
  z-index:1000;" target="_blank">
<i style="margin-top:16px" class="fab fa-whatsapp"></i>
</a>
	<div class="tp-banner-container">
		<div class="tp-banner">
			<ul>
				<li>
					<img src="images/slider/first-slide.jpg" alt>
					<div class="v-center">
						<h4 class="title" style="font-size: 44px;line-height: 44px;font-family: 'Anton', sans-serif; text-transform:uppercase;color: #fff;">QUER VENDER MAIS?</h4>
						<a href="https://wa.me/5511999706346?text=Olá%20tudo%20bem%20?%20Eu%20gostaria%20de%20solicitar%20um%20orçamento%20referente%20aos%20serviços%20da%20Agência." class="call-action" target="_blank">Peça um orçamento agora</a>
					</div>
				   </li>
				   
				   <li>
						<img src="images/slider/second-slide.jpg" alt>
						<div class="v-center">
							<h4 class="title" style="font-size: 44px;line-height: 44px;font-family: 'Anton', sans-serif; text-transform:uppercase;color: #fff;">QUER UM SITE PROFISSIONAL COM O MELHOR CUSTO-BENEFÍCIO?</h4>
							<a href="https://wa.me/5511999706346?text=Olá%20tudo%20bem%20?%20Eu%20gostaria%20de%20solicitar%20um%20orçamento%20referente%20aos%20serviços%20da%20Agência." class="call-action" target="_blank">Peça um orçamento agora</a>
						</div>
				   </li>
				   
				   <li>
						<img src="images/slider/third-slide.jpg" alt>
						<div class="v-center">
							<h4 class="title" style="font-size: 44px;line-height: 44px;font-family: 'Anton', sans-serif; text-transform:uppercase;color: #fff;">SOMOS ESPECIALISTAS EM REDES SOCIAIS</h4>
							<a href="https://wa.me/5511999706346?text=Olá%20tudo%20bem%20?%20Eu%20gostaria%20de%20solicitar%20um%20orçamento%20referente%20aos%20serviços%20da%20Agência." class="call-action" target="_blank">Peça um orçamento agora</a>
						</div>
   				</li>
			</ul>
			<!-- <div class="scroll-down-button">
				<img src="images/down-button.png" class="down-button" alt>
				<i class="fa fa-angle-down"></i>
			</div> -->
		</div>
	</div>
	<!-- <section class="section section-main position-relative section-main-2 bg-dark dark" id="home">
           
            <div class="bg-video" data-property="{videoURL:'https://youtu.be/SuYi_1RmFRY', showControls: false, containment:'self',startAt:1,stopAt:39,mute:true,autoPlay:true,loop:true,opacity:0.3,quality:'hd1080'}"></div>
            <div class="bg-image bg-video-placeholder zooming"><img src="images/bg-video.jpg" alt=""></div>
			<div class="v-vertical">
				<div id="owl-video">
					<div class="item">
						<h4 class="title" style="font-size: 44px;line-height: 44px;font-family: 'Anton', sans-serif; text-transform:uppercase;">QUER VENDER MAIS?</h4>
						<a href="https://wa.me/5511999706346?text=Olá%20tudo%20bem%20?%20Eu%20gostaria%20de%20solicitar%20um%20orçamento%20referente%20aos%20serviços%20da%20Agência." class="call-action" target="_blank">Peça um orçamento agora</a>
					</div>
					<div class="item">
						<h4 class="title" style="font-size: 44px;line-height: 44px;font-family: 'Anton', sans-serif; text-transform:uppercase;">QUER UM SITE PROFISSIONAL COM O MELHOR CUSTO-BENEFÍCIO?</h4>
						<a href="https://wa.me/5511999706346?text=Olá%20tudo%20bem%20?%20Eu%20gostaria%20de%20solicitar%20um%20orçamento%20referente%20aos%20serviços%20da%20Agência." class="call-action" target="_blank">Peça um orçamento agora</a>
					</div>
					<div class="item">
						<h4 class="title" style="font-size: 44px;line-height: 44px;font-family: 'Anton', sans-serif; text-transform:uppercase;">SOMOS ESPECIALISTAS EM REDES SOCIAIS</h4>
						<a href="https://wa.me/5511999706346?text=Olá%20tudo%20bem%20?%20Eu%20gostaria%20de%20solicitar%20um%20orçamento%20referente%20aos%20serviços%20da%20Agência." class="call-action" target="_blank">Peça um orçamento agora</a>
					</div>
					
				</div>				
			</div>
    </section> -->
	<!-- page header -->
	<?php include("includes/menu.php")?>
	<!--/ pade header -->
	<!-- page content -->
	<section id="about" class="about-us padding-section" style="margin-top: 0;">
		<div class="grid-row">
			<div class="grid-col-row clear">
				<div class="grid-col grid-col-6 col-sd-12">
					<!-- section title -->
					<div class="title">
						<span class="main-title">SOMOS UMA</span>
						<span class="slash-icon">/<!-- <i class="fa fa-angle-double-right"></i> --></span><br/>
						AGÊNCIA CRIATIVA
					</div>
					<!-- section title -->
					<dl>
						<dt><b>O que fazemos</b><br/>	
						Agência Momento, porque o momento é de transformação. Transformação na forma de fazer o seu negócio crescer. Transformação na forma de se comunicar com seus clientes. </dt>
						<dt><b>Como fazemos</b><br/>	
						Como fazemos essa mágica? Somos uma agência 360° que vai na contra mão do mercado, oferecendo soluções acessíveis para você: construímos marca, desenvolvemos soluções de CRM para você converter mais clientes e garantimos sua presença no ambiente online e offline.</dt>
							
					</dl>
					
				</div>
				<div class="grid-col grid-col-6 content-img col-sd-12">
					<img class="ipad" src="images/ipad.png" alt>
					<img id="splash-1" src="img/splash-1.png" alt>
					<img id="splash-2" src="img/splash-2.png" alt>
				</div>
			</div>
		</div>
		
		
		<div class="grid-row clear">
			<div class="grid-col-row">
				<div class="grid-col grid-col-6 col-sd-12">
					<div class="title">
						<span class="main-title">CONHEÇA NOSSOS</span>
						<span class="slash-icon">/</span><br/>SERVIÇOS
					</div>
				</div>
			</div>
		</div>
		<div class="grid-row clear">
			<div class="grid-col-row">
					<div class="item-example grid-col grid-col-4">
						<div class="rectangle"><i class="fa fa-globe" aria-hidden="true"></i></div>
						<h3>SITES E E-COMMERCE</h3>
						<div class="line"></div>
						<!-- <p>Desenvolvemos websites e e-commerces de diversos segmentos dentro dos padrões mais atuais de tecnologia. Todos eles são responsivos, ou seja, se adaptam ao tamanho da tela de notebooks, tablets e celulares com layout exclusivo, porém sempre respeitando a identidade visual, logotipo e paleta de cores da sua empresa. Todas as artes, textos e designs iniciais são criados e inseridos pelo nosso time.</p> -->
					</div>
					<div class="item-example grid-col grid-col-4">
						<div class="rectangle"><i class="fa fa-share-alt" aria-hidden="true"></i></div>
						<h3>REDES SOCIAIS</h3>
						<div class="line"></div>
						<!-- <p>Se você acha que fazer postagens é gerenciar as Redes Sociais da sua empresa, está muito errado. Aqui na Agência Momento dividimos essa gestão em 5 etapas: 1 - Definição da estratégia e do posicionamento; 2 - Planejamento de publicações; 3- Execução até o monitoramento; 4- Levantamento dos resultados; 5- Otimização das campanhas. Se suas Redes Sociais forem bem cuidadas com planejamento e estratégias definidas os benefícios serão reais.</p> -->
					</div>
					<div class="item-example grid-col grid-col-4">
						<div class="rectangle"><i class="" aria-hidden="true"><img src="images/download.png" style="margin: 0 auto;    width: 71px;"/></i> </div>
						
						<h3>GOOGLE ADS</h3>
						<div class="line"></div>
						<!-- <p>Seu site na primeira página do Google! Alcance potenciais clientes exatamente no momento em que eles estão procurando o seu serviço/produto. A Agência Momento conta, somente, com profissionais certificados pelo Google, isso faz com que os resultados gerados sejam sempre os melhores para sua empresa.</p> -->
					</div>
					
					
			</div>
		</div>

		<div class="grid-row clear">
			<div class="grid-col-row">
					<div class="item-example grid-col grid-col-4">
							<div class="rectangle"><i class="fa fa-puzzle-piece" aria-hidden="true"></i></div>
							<h3>SEO - OTIMIZAÇÃO DE BUSCA NO GOOGLE</h3>
							<div class="line"></div>
							<!-- <p>Análise completa seguidas de ações corretivas em todos os fatores relevantes para que seu site/negócio tenha destaque nos principais buscadores, aparecendo nas primeiras páginas do Google.</p> -->
						</div>

					<div class="item-example grid-col grid-col-4">
						<div class="rectangle"><i class="fa fa-picture-o" aria-hidden="true"></i></div>
						<h3>LOGOTIPO, BRANDING E NAMING</h3>
						<div class="line"></div>
						<!-- <p>Desenvolvimento de Logotipos e identificação visual e execução de estratégias "share of mind" e manual de marca.</p> -->
					</div>
					<div class="item-example grid-col grid-col-4">
						<div class="rectangle"><i class="fa fa-code" aria-hidden="true"></i></div>
						<h3>DESENVOLVIMENTO DE APP E SOFTWARES</h3>
						<div class="line"></div>
						<!-- <p>Desenvolvimento de aplicativos e programas com as melhores ferramentas, experiência e criatividade.</p> -->
					</div>
			</div>
			<center>
				<a href="https://wa.me/5511999706346?text=Olá%20tudo%20bem%20?%20Eu%20gostaria%20de%20solicitar%20um%20orçamento%20referente%20aos%20serviços%20da%20Agência." class="call-action" target="_blank">Peça um orçamento agora</a>
			</center>
		
		</div>
	</section>

	<section class="text-section" style="background-color: #ff9a00;margin: 50px auto;padding-bottom: 50px;">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 offset-lg-1 align-self-center">
					<img src="images/conn.png" class="img-fluid">
				</div>
				<div class="col-lg-6 offset-lg-1 align-self-center">
					<div class="counter-name -aux1" style="color: #fff; text-align: left;font-size: 20px;">
						<b>CONECTIVIDADE COM OS SEUS CLIENTES</b><br><br>
					</div>

					<ul class="list-section -aux1 -color">
						<li><p style="color: #ffffff;">Estratégia de conteúdo mensal</p></li>
						<li><p style="color: #ffffff;">Criação de artes gráficas promocionais</p></li>
						<li><p style="color: #ffffff;">Agendamento das publicações</p></li>
						<li><p style="color: #ffffff;">Criação de legendas vendedoras</p></li>
						<li><p style="color: #ffffff;">Gerenciamos anúncios</p></li>
					</ul>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-12 text-center">
					<a href="https://wa.me/5511999706346?text=Olá%20tudo%20bem%20?%20Eu%20gostaria%20de%20solicitar%20um%20orçamento%20referente%20aos%20serviços%20da%20Agência." class="call-action" target="_blank">Peça um orçamento agora</a>
				</div>
			</div>
		</div>
	</section>

	<section class="text-section ">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 offset-lg-1 align-self-center">
					<img src="images/wifi.jpg" class="size-image img-fluid">
				</div>
				<div class="col-lg-6 offset-lg-1 align-self-center">
					<div class="counter-name -aux1" style="color: #000; text-align: left;font-size: 20px;">
						<b>PRESENÇA DIGITAL</b><br><br>
					</div>

					<ul class="list-section -aux1">
						<li><p>Conteúdo</p></li>
						<li><p>Relacionamento</p></li>
						<li><p>Plataforma user-friendly</p></li>
						<li><p>Relevância</p></li>
					</ul>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-12 text-center mb-5">
					<a href="https://wa.me/5511999706346?text=Olá%20tudo%20bem%20?%20Eu%20gostaria%20de%20solicitar%20um%20orçamento%20referente%20aos%20serviços%20da%20Agência." class="call-action" target="_blank">Peça um orçamento agora</a>
				</div>
			</div>
		</div>
	</section>

	<!-- <section class="text-section parallaxed">
		<div class="parallax-image" data-parallax-left="0.5" data-parallax-top="0.3" data-parallax-scroll-speed="0.5">
			<img src="images/parallax-new.jpg" alt="">
		</div>
		<div class="container">
			<div class="row">
				<div class="col-lg-12 text-center">
					<div class="parallax-content-second">
						<div style="color: #fff;font-size: 20px;">
							<b>CRIAÇÃO DE WEBSITES E LOJAS VIRTUAIS</b><br><br>
						</div>

						<ul class="list-section2">
							<li><p>Criamos do zero ou reformulamos seu antigo</p></li>
							<li><p>1 mês de hospedagem Grátis Responsivo (Desktop, notebook e celular)</p></li>
							<li><p>Formulários personalizados</p></li>
							<li><p>Botões de WhatsApp e ligação direta (Mobile)</p></li>
						</ul>

						<a href="https://wa.me/5511999706346?text=Olá%20tudo%20bem%20?%20Eu%20gostaria%20de%20solicitar%20um%20orçamento%20referente%20aos%20serviços%20da%20Agência." class="call-action" target="_blank">Peça um orçamento agora</a>
					</div>
				</div>
			</div>
		</div>
	</section> -->

	<section class="text-section" style="background-color: #ff9a00;margin: 50px auto;padding-bottom: 50px;">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 offset-lg-1 align-self-center">
					<img src="images/web.png" class="img-fluid">
				</div>
				<div class="col-lg-6 offset-lg-1 align-self-center">
					<div class="counter-name -aux1" style="color: #fff; text-align: left;font-size: 20px;">
						<b>CRIAÇÃO DE WEBSITES E LOJAS VIRTUAIS</b></b><br><br>
					</div>

					<ul class="list-section -aux1 -color">
						<li><p style="color: #ffffff;">Criamos do zero ou reformulamos seu antigo</p></li>
						<li><p style="color: #ffffff;">1 mês de hospedagem Grátis Responsivo (Desktop, notebook e celular)</p></li>
						<li><p style="color: #ffffff;">Formulários personalizados</p></li>
						<li><p style="color: #ffffff;">Botões de WhatsApp e ligação direta (Mobile)</p></li>
					</ul>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-12 text-center mb-5">
					<a href="https://wa.me/5511999706346?text=Olá%20tudo%20bem%20?%20Eu%20gostaria%20de%20solicitar%20um%20orçamento%20referente%20aos%20serviços%20da%20Agência." class="call-action" target="_blank">Peça um orçamento agora</a>
				</div>
			</div>
		</div>
	</section>

	<section class="text-section ">
		<!-- <div class="grid-row">
			<div class="grid-col-row clear">
				<div class="grid-col grid-col-4">
					<img src="images/grafico.jpg" style="width: 100%">
				</div>
				<div class="grid-col grid-col-8">
					<div class="counter-block text-new">
						<div class="counter-name" style="color: #000; text-align: left;"><b>CAMPANHAS PATROCINADAS</b><br><br>
						-Determinamos seu público - alvo<br>
						-Realizamos pesquisa de palavras-chave<br>
						-Realizamos analise de concorrência<br>
						-Definimos orçamentos e lances<br>
						-Configuramos estratégias de remarketing (Anuncios perseguem seus clientes após entrar no seu site)<br>
						</div>
					</div>
				</div>
			</div>
			<center>
				<a href="https://wa.me/5511999706346?text=Olá%20tudo%20bem%20?%20Eu%20gostaria%20de%20solicitar%20um%20orçamento%20referente%20aos%20serviços%20da%20Agência." class="call-action" target="_blank">Peça um orçamento agora</a>
			</center>
		</div> -->
		<div class="container">
			<div class="row">
				<div class="col-lg-4 offset-lg-1 align-self-center">
					<img src="images/grafico.jpg" class="size-image img-fluid">
				</div>
				<div class="col-lg-6 offset-lg-1 align-self-center">
					<div class="counter-name -aux1" style="color: #000; text-align: left;font-size: 20px;">
						<b>CAMPANHAS PATROCINADAS</b><br><br>
					</div>

					<ul class="list-section -aux1">
						<li><p>Determinamos seu público - alvo</p></li>
						<li><p>Realizamos pesquisa de palavras-chave</p></li>
						<li><p>Realizamos analise de concorrência</p></li>
						<li><p>Definimos orçamentos e lances</p></li>
						<li><p>Configuramos estratégias de remarketing (Anuncios perseguem seus clientes após entrar no seu site)</p></li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12 text-center">
					<a href="https://wa.me/5511999706346?text=Olá%20tudo%20bem%20?%20Eu%20gostaria%20de%20solicitar%20um%20orçamento%20referente%20aos%20serviços%20da%20Agência." class="call-action" target="_blank">Peça um orçamento agora</a>
				</div>
			</div>
		</div>
	</section>

	<section id="team" class="our-team padding-section">
		<div class="grid-row">
			<div class="grid-col-row clear">
				<div class="grid-col grid-col-6 col-sd-12">
					<div class="title">
						<span class="main-title">CONHEÇA</span>
						<span class="slash-icon">/</span><br/>NOSSO TIME
					</div>
					<!-- <dl>
						<dt>Suspendisse interdum tortor augue, in tempus urna ultricies</dt>
							<dd>Vivamus fringilla eu nisl non laoreet.Maecenas ac velit condimentum, condimentum ante at, sodales justo. Vivamus suscipit nunc ut condimentum feugiat. Praesent imperdiet sollicitudin pulvinar. Pellentesque interdum sem a ligula scelerisque bibendum. Donec aliquam mattis neque quis pretium pellentesque </dd>
					</dl> -->
					<br />
					<div class="carousel-nav">
						<div class="carousel-button">
							<div class="prev"><i class="fa fa-chevron-left"></i></div>
							<div class="next"><i class="fa fa-chevron-right"></i></div>
						</div>
						<div class="carousel-line"></div>
					</div>
						<div id="tabs-carousel" class="owl-carousel choose-team">
							<div class="team-item">
								<div class="border-img">
									<a id="tab1" class="active">
										<div class="window-tabs"><div class="overflow-block"></div><img src="images/team/img-team-1.jpg" alt></div>
									</a>
								</div>
								<h2>Igor Afonso</h2>
								<p>Diretor Executivo</p>
							</div>
							<div class="team-item">
								<div class="border-img">
									<a id="tab2"><div class="window-tabs">
										<div class="overflow-block"></div><img src="images/team/img-team-2.jpg" alt></div>
									</a>
								</div>
								<h2>Daniel Pawel</h2>
								<p>CTO</p>
							</div>
							<div class="team-item">
								<div class="border-img">
									<a id="tab3">
										<div class="window-tabs"><div class="overflow-block"></div><img src="images/team/img-team-3.jpg" alt></div>
									</a>
								</div>
								<h2>Bruno Chahad</h2>
								<p>COO</p>
							</div>
							<!-- <div class="team-item">
								<div class="border-img">
									<a id="tab4">
										<div class="window-tabs"><div class="overflow-block"></div><img src="images/team/img-team-4.jpg" alt></div>
									</a>
								</div>
								<h2>Leonardo Cruz</h2>
								<p>Designer</p>
							</div> -->
							<div class="team-item">
								<div class="border-img">
									<a id="tab4">
										<div class="window-tabs"><div class="overflow-block"></div><img src="images/team/img-team-5.jpg" alt></div>
									</a>
								</div>
								<h2>Leonardo Cruz</h2>
								<p>Designer</p>
							</div>
							<div class="team-item">
								<div class="border-img">
									<a id="tab5">
										<div class="window-tabs"><div class="overflow-block"></div><img src="images/team/img-team-6.jpg" alt></div>
									</a>
								</div>
								<h2>Leonardo Ferroni</h2>
								<p>Analista de Mídia</p>
							</div>
							<div class="team-item">
								<div class="border-img">
									<a id="tab6">
										<div class="window-tabs"><div class="overflow-block"></div><img src="images/team/img-team-7.jpg" alt></div>
									</a>
								</div>
								<h2>Mariana Rebesco</h2>
								<p>Conteúdo</p>
							</div>
							<div class="team-item">
								<div class="border-img">
									<a id="tab7">
										<div class="window-tabs"><div class="overflow-block"></div><img src="images/team/img-team-8.jpg" alt></div>
									</a>
								</div>
								<h2>Larissa Camargo</h2>
								<p>RP & Jornalista</p>
							</div>
							<div class="team-item">
								<div class="border-img">
									<a id="tab8">
										<div class="window-tabs"><div class="overflow-block"></div><img src="images/team/img-team-9.jpg" alt></div>
									</a>
								</div>
								<h2>Thiago Miranda</h2>
								<p>Webdesigner</p>
							</div>
							<div class="team-item">
								<div class="border-img">
									<a id="tab9">
										<div class="window-tabs"><div class="overflow-block"></div><img src="images/team/img-team-10.jpg" alt></div>
									</a>
								</div>
								<h2>Brunno Gonçalvez</h2>
								<p>Designer</p>
							</div>
							<div class="team-item">
								<div class="border-img">
									<a id="tab10">
										<div class="window-tabs"><div class="overflow-block"></div><img src="images/team/img-team-11.jpg" alt></div>
									</a>
								</div>
								<h2>Breno Inkratas</h2>
								<p>Designer</p>
							</div>
							<div class="team-item">
								<div class="border-img">
									<a id="tab11">
										<div class="window-tabs"><div class="overflow-block"></div><img src="images/team/img-team-12.jpg" alt></div>
									</a>
								</div>
								<h2>Igor Doz</h2>
								<p>Designer</p>
							</div>
							<div class="team-item">
								<div class="border-img">
									<a id="tab12">
										<div class="window-tabs"><div class="overflow-block"></div><img src="images/team/img-team-13.jpg" alt></div>
									</a>
								</div>
								<h2>Guilherme Moraes</h2>
								<p>Vendas</p>
							</div>

							<div class="team-item">
								<div class="border-img">
									<a id="tab13">
										<div class="window-tabs"><div class="overflow-block"></div><img src="images/team/img-team-14.jpg" alt></div>
									</a>
								</div>
								<h2>Matheus Chacon</h2>
								<p>Designer</p>
							</div>

							<div class="team-item">
								<div class="border-img">
									<a id="tab14">
										<div class="window-tabs"><div class="overflow-block"></div><img src="images/team/img-team-15.jpg" alt></div>
									</a>
								</div>
								<h2>Gioanna Santato</h2>
								<p>Conteúdo</p>
							</div>
						</div>
				</div>
				<div class="grid-col grid-col-6 col-sd-12">
					<div class="border-img">
						<div class="window-tabs big-window-tab">
							<div class="choose-team">
								<div id="con_tab1" class="active">
										<div class="overflow-block">
											<div class="inform-item">
												<div class="item-name">Igor Afonso<br/><p>Diretor Executivo</p></div>
											</div>
										</div>
										<img src="images/team/img-team-1big.jpg" alt>
									</div>
								<div id="con_tab2">
									<div class="overflow-block">
										<div class="inform-item">
											<div class="item-name">Daniel Pawel<br/><p>CTO</p></div>
										</div>
									</div>
									<img src="images/team/img-team-2big.jpg" alt>
								</div>
								<div id="con_tab3">
									<div class="overflow-block">
										<div class="inform-item">
											<div class="item-name">Bruno Chahad<br/><p>COO</p></div>
										</div>
									</div>
									<img src="images/team/img-team-3big.jpg" alt>
								</div>
								<div id="con_tab4">
									<div class="overflow-block">
										<div class="inform-item">
											<div class="item-name">Leonardo Cruz<br/><p>Designer</p></div>
										</div>
									</div>
									<img src="images/team/img-team-5big.jpg" alt>
								</div>

								<div id="con_tab5">
									<div class="overflow-block">
										<div class="inform-item">
											<div class="item-name">Leonardo Ferroni<br/><p>Analista de Mídia</p></div>
										</div>
									</div>
									<img src="images/team/img-team-6big.jpg" alt>
								</div>

								<div id="con_tab6">
									<div class="overflow-block">
										<div class="inform-item">
											<div class="item-name">Mariana Rebesco<br/><p>Conteúdo</p></div>
										</div>
									</div>
									<img src="images/team/img-team-7big.jpg" alt>
								</div>

								<div id="con_tab7">
									<div class="overflow-block">
										<div class="inform-item">
											<div class="item-name">Larissa Camargo<br/><p>RP & Jornalista</p></div>
										</div>
									</div>
									<img src="images/team/img-team-8big.jpg" alt>
								</div>

								<div id="con_tab8">
									<div class="overflow-block">
										<div class="inform-item">
											<div class="item-name">Thiago Miranda<br/><p>Webdesigner</p></div>
										</div>
									</div>
									<img src="images/team/img-team-9big.jpg" alt>
								</div>

								<div id="con_tab9">
									<div class="overflow-block">
										<div class="inform-item">
											<div class="item-name">Brunno Gonçalvez<br/><p>Designer</p></div>
										</div>
									</div>
									<img src="images/team/img-team-10big.jpg" alt>
								</div>

								<div id="con_tab10">
									<div class="overflow-block">
										<div class="inform-item">
											<div class="item-name">Breno Inkratas<br/><p>Designer</p></div>
										</div>
									</div>
									<img src="images/team/img-team-11big.jpg" alt>
								</div>

								<div id="con_tab11">
									<div class="overflow-block">
										<div class="inform-item">
											<div class="item-name">Igor Doz<br/><p>Designer</p></div>
										</div>
									</div>
									<img src="images/team/img-team-12big.jpg" alt>
								</div>

								<div id="con_tab12">
									<div class="overflow-block">
										<div class="inform-item">
											<div class="item-name">Guilherme Moraes<br/><p>Vendas</p></div>
										</div>
									</div>
									<img src="images/team/img-team-13big.jpg" alt>
								</div>

								<div id="con_tab13">
									<div class="overflow-block">
										<div class="inform-item">
											<div class="item-name">Matheus Chacon<br/><p>Designer</p></div>
										</div>
									</div>
									<img src="images/team/img-team-14big.jpg" alt>
								</div>

								<div id="con_tab14">
									<div class="overflow-block">
										<div class="inform-item">
											<div class="item-name">Giovanna Santato<br/><p>Conteúdo</p></div>
										</div>
									</div>
									<img src="images/team/img-team-15big.jpg" alt>
								</div>
							</div>
						</div>
						<img id="splash-3" src="img/splash-3.png" alt>
					</div>
				</div>
			</div>
		</div>
	</section>

	<hr>

	<!-- <section id="portfolio" class="portfolio padding-section">
		<div class="grid-row">
			<div class="portfolio-filter clear">
				<div class="title">
					<a href="#" class="all-iso-item active" data-filter=".item">ALL</a>
					<span class="main-title">PORTFÓLIO</span>
					<span class="slash-icon">/</span>
					<a href="#" data-filter=".photo">FOTOGRAFIA</a>
					<a href="#" data-filter=".design">DESIGN</a>
					<a href="#" data-filter=".design">REDES SOCIAIS</a>
					<a href="#" data-filter=".video">VÍDEO</a>
					<a href="#" data-filter=".web">WEB</a>
					
				</div>
				
			</div>
		</div>
		<div class="isotope">
			<div class="item design photo video web">
				<div class="portfolio-hover">
					<div class="portfolio-info">
						<a href=""><div class="portfolio-title">Future timeline</div></a>
						<div class="portfolio-divider"></div>
						<div class="portfolio-description">Photography, Art</div>
					</div>
				</div>
				<img src="images/isotop/iso1.jpg" alt>
			</div>
			<div class="item illust video">
				<div class="portfolio-hover">
					<div class="portfolio-info">
						<a href=""><div class="portfolio-title">Sit Amet</div></a>
						<div class="portfolio-divider"></div>
						<div class="portfolio-description">Video, Art</div>
					</div>
				</div>
				<img src="images/isotop/iso3.jpg" alt>
			</div>
			<div class="item design illust photo web">
				<div class="portfolio-hover">
					<div class="portfolio-info">
						<a href=""><div class="portfolio-title">Sed Laoreete</div></a>
						<div class="portfolio-divider"></div>
						<div class="portfolio-description">Video, Photography</div>
					</div>
				</div>
				<img src="images/isotop/iso5.jpg" alt>
			</div>
			<div class="item illust photo">
				<div class="portfolio-hover">
					<div class="portfolio-info">
						<a href=""><div class="portfolio-title">Amet Interdum</div></a>
						<div class="portfolio-divider"></div>
						<div class="portfolio-description">Music, Art</div>
					</div>
				</div>
				<img src="images/isotop/iso8.jpg" alt>
			</div>
			<div class="item design video web">
				<div class="portfolio-hover">
					<div class="portfolio-info">
						<a href=""><div class="portfolio-title">Cras varius</div></a>
						<div class="portfolio-divider"></div>
						<div class="portfolio-description">Photography, Art</div>
					</div>
				</div>
				<img src="images/isotop/iso10.jpg" alt>
			</div>
			<div class="item illust photo video">
				<div class="portfolio-hover">
					<div class="portfolio-info">
						<a href=""><div class="portfolio-title">Maecenas Semper</div></a>
						<div class="portfolio-divider"></div>
						<div class="portfolio-description">Business, Art</div>
					</div>
				</div>
				<img src="images/isotop/iso2.jpg" alt>
			</div>
			<div class="item illust video web music">
				<div class="portfolio-hover">
					<div class="portfolio-info">
						<a href=""><div class="portfolio-title">Scelerisque Venen</div></a>
						<div class="portfolio-divider"></div>
						<div class="portfolio-description">Music, Art</div>
					</div>
				</div>
				<img src="images/isotop/iso6.jpg" alt>
			</div>
			<div class="item design photo music">
				<div class="portfolio-hover">
					<div class="portfolio-info">
						<a href=""><div class="portfolio-title">Libero Luctus</div></a>
						<div class="portfolio-divider"></div>
						<div class="portfolio-description">Music, Art</div>
					</div>
				</div>
				<img src="images/isotop/iso9.jpg" alt>
			</div>
			<div class="item photo web music">
				<div class="portfolio-hover">
					<div class="portfolio-info">
						<a href=""><div class="portfolio-title">Dlandit Cursus</div></a>
						<div class="portfolio-divider"></div>
						<div class="portfolio-description">Business, Art</div>
					</div>
				</div>
				<img src="images/isotop/iso4.jpg" alt>
			</div>
			<div class="item photo video web music">
				<div class="portfolio-hover">
					<div class="portfolio-info">
						<a href=""><div class="portfolio-title">Future timeline</div></a>
						<div class="portfolio-divider"></div>
						<div class="portfolio-description">Music, Photography</div>
					</div>
				</div>
				<img src="images/isotop/iso7.jpg" alt>
			</div>
			<div class="item design video music">
				<div class="portfolio-hover">
					<div class="portfolio-info">
						<a href=""><div class="portfolio-title">Sed Tempor</div></a>
						<div class="portfolio-divider"></div>
						<div class="portfolio-description">Business, Art</div>
					</div>
				</div>
				<img src="images/isotop/iso11.jpg" alt>
			</div>
		</div>
	</section> -->
	
	<section id="innovation" class="innovation parallaxed">
		<!-- <div class="parallax-image" data-parallax-left="0.5" data-parallax-top="0.3" data-parallax-scroll-speed="0.5">
			<img src="images/parallax-3.jpg" alt="">
		</div> -->
		<div class="grid-row">
			<div class="parallax-content-third">
				<div id="client-carousel" class="owl-carousel">
					<div>
						<p class="testimonial-title">O QUE FALAM SOBRE NÓS</p>
						<div class="testimonial-separator"></div>
						<p class="testimonial-text">“Desde o momento da concepção do nosso projeto até a entrega final, foram muito profissionais e pontuais com os prazos!”</p>
						<p class="testimonial-author">- ANA PAULA</p>
					</div>
					<div>
						<p class="testimonial-title">O QUE FALAM SOBRE NÓS</p>
						<div class="testimonial-separator"></div>
						<p class="testimonial-text">“A Agência Momento gerencia as redes sociais da minha empresa há um ano, o crescimento das páginas são maravilhosos e o faturamento do meu business é proporcional.”</p>
						<p class="testimonial-author">- RICARDO JABOUR</p>
					</div>
					<div>
						<p class="testimonial-title">O QUE FALAM SOBRE NÓS</p>
						<div class="testimonial-separator"></div>
						<p class="testimonial-text">“O Design e os resultados são o ponto forte da agência. Já trocamos de agências 3 vezes, agora não trocaremos por um bom tempo.”</p>
						<p class="testimonial-author">- VERÔNICA MARTINS</p>
					</div>
				</div>
			</div>
		</div>
		
	</section>

	<section id="clientes" class="our-team padding-section">
		<div class="grid-row clear">
			<div class="grid-col-row">
				<div class="grid-col grid-col-6 col-sd-12">
					<div class="title">
						<span class="main-title">ALGUNS</span>
						<span class="slash-icon">/</span><br/>CLIENTES
					</div>
				</div>
			</div>
		</div>
		<div class="grid-row">
			<div class="parallax-content-third">
				<div id="owl-client" class="owl-carousel">
					<div>
						<img src="images/clients/imagem1.png" />
					</div>

					<div>
						<img src="images/clients/imagem2.png" />
					</div>

					<div>
						<img src="images/clients/imagem3.png" />
					</div>

					<div>
						<img src="images/clients/imagem4.png" />
					</div>

					<div>
						<img src="images/clients/imagem5.png" />
					</div>

					<div>
						<img src="images/clients/imagem6.png" />
					</div>

					<div>
						<img src="images/clients/imagem7.png" />
					</div>

					<div>
						<img src="images/clients/imagem8.png" />
					</div>

					<div>
						<img src="images/clients/imagem9.png" />
					</div>

				</div>
			</div>
		</div>
	</section>

	
	<section id="contact" class="contact padding-section">
		<div class="contact-head">
			<div class="grid-row">
				<div class="title">
					<span class="main-title">CONTATO</span> <span class="slash-icon">/<!-- <i class="fa fa-angle-double-right"></i> --></span> <h5>AGUARDAMOS O SEU CONTATO</h5>
				</div>
			</div>
		</div>	
			<div class="grid-row clear">
				<div class="grid-contact">
					<i class="fa  fa-globe"></i>
					<div class="contact-content">
						<div class="kind-contact">Estamos na:</div>
						<p>Rua Atuaú, 69 - Pinheiros</p>
					</div>
				</div>
				<div class="grid-contact">
					<i class="fa fa-phone"></i>
					<div class="contact-content">
						<div class="kind-contact">Telefone:</div>
						<p style="color: #6d6d6d;">(11)99970-6346</p>
					</div>
				</div>
				<div class="grid-contact">
					<i class="fas fa-envelope"></i>
					<div class="contact-content">
						<div class="kind-contact">E-mail de contato:</div>
						<p>igor@agenciamomento.com.br</p>
					</div>
				</div>
				<div class="grid-contact">
					<i class="fa  fa-power-off"></i>
					<div class="contact-content">
						<div class="kind-contact">Funcionamento:</div>
						<p>Seg - Sex 07:00 - 19:00</p>
					</div>
				</div>
			</div>

			<div class="grid-row clear">
				<div class="grid-col grid-col-12">
					<div class="composer">
						<div class="info-boxes error-message" id="feedback-form-errors">
							<div class="info-box-icon"><i class="fa fa-times"></i></div>
							<strong>Error box</strong><br />
							<div class="message"></div>
						</div>
						<div class="info-boxes confirmation-message" id="feedback-form-success">
							<div class="info-box-icon"><i class="fa fa-check"></i></div>
							<strong>Confirmation box</strong><br />Vestibulum sodales pellentesque nibh quis imperdiet
							<div class="close-button"><i class="fa fa-times"></i></div>
						</div>
					</div>
					<div class="email_server_responce"></div>
					<form action="php/contacts-process.php" method="post" id="feedback-form" class="message-form clear">
						<p class="message-form-author">
							<label for="name">Seu nome <span class="required">*</span></label>
							<input id="name" name="name" type="text" value="" size="30" aria-required="true">
						</p>
						<p class="message-form-email">
							<label for="email">Seu e-mail <span class="required">*</span></label>
							<input id="email" name="email" type="text" value="" size="30" aria-required="true">
						</p>
						<p class="message-form-website">
							<label for="phone">Telefone <span class="required">*</span></label>
							<input id="phone" name="phone" type="text" value="" size="30" aria-required="true">
						</p>
						<p class="message-form-message">
							<label for="message">Mensagem</label>
							<textarea id="message" name="message" cols="45" rows="10" aria-required="true"></textarea>
						</p>
						<p class="form-submit rectangle-button green medium">
							<input name="submit" type="submit" id="submit" value="Enviar">
						</p>
					</form>
				</div>
			</div>
			
			<!-- <div class="subscribe">
				<div class="grid-row clear">
					<div class="them-mask"></div>
					<div class="subscribe-header">subscribe</div>
					<form action="#" class="clear">
						<input type="email" placeholder="Your Email Address">
						<button type="submit" class="button-send">Send</button>
					</form>
				</div>
			</div> -->
		</section>
		<section class="text-section parallaxed padding-section">
				<div class="parallax-image" data-parallax-left="0.5" data-parallax-top="0.3" data-parallax-scroll-speed="0.5">
					<img src="images/parallax-2.jpg" alt="">
				</div>
				<div class="grid-row">
					<div class="parallax-content-second">
						<div class="grid-col-row clear">
							<div class="grid-col grid-col-3">
								<div class="counter-block">
								<!-- <i class="fas fa-sort-amount-up"></i> -->
									<div class="counter number-3" data-count="3"></div>
									<div class="counter-name">De aumento em suas vendas</div>
								</div>
							</div>
							<div class="grid-col grid-col-3">
								<div class="counter-block">
									<!-- <i class="fa fa-group"></i> -->
									<div class="counter" data-count="124"></div>
									<div class="counter-name">Clientes ativos</div>
								</div>							
							</div>
							<div class="grid-col grid-col-3">
								<div class="counter-block">
									<!-- <i class="fa fa-trophy" aria-hidden="true"></i> -->
									
									<div class="counter" data-count="5"></div>
									<div class="counter-name">Prêmios recebidos</div>
								</div>
							</div>
							
							<div class="grid-col grid-col-3">
								<div class="counter-block last">
								<!-- <i class="far fa-window-maximize"></i> -->
									<div class="counter" data-count="144"></div>
									<div class="counter-name">Sites entregues em 2019</div>
								</div>
							</div>
						</div>
					</div>
				<center>
					<a href="https://wa.me/5511999706346?text=Olá%20tudo%20bem%20?%20Eu%20gostaria%20de%20solicitar%20um%20orçamento%20referente%20aos%20serviços%20da%20Agência." class="call-action" target="_blank">Peça um orçamento agora</a>
				</center>
				</div>
			</section>
		<!--/ page content -->
		<div class="map" style="margin-top: 0;">
			<!-- <div id="map" class="google-map"></div> -->
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3657.0649670564244!2d-46.69934298507229!3d-23.56611006765263!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce57a661c96945%3A0xb96c16ac4962171d!2sR.+Atuau%2C+69+-+Pinheiros%2C+S%C3%A3o+Paulo+-+SP%2C+05428-030!5e0!3m2!1spt-BR!2sbr!4v1565109261997!5m2!1spt-BR!2sbr" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>
		
		<?php include("includes/footer.php");?>
		<?php include("includes/scripts.php");?>
		<script src="vendor/jquery.mb.ytplayer/dist/jquery.mb.YTPlayer.min.js"></script>
		<script>
		$(document).ready(function() {
 
			$("#owl-client").owlCarousel({

				autoPlay: 3000, //Set AutoPlay to 3 seconds

				items : 5,
				itemsDesktop : [1199,3],
				itemsDesktopSmall : [979,3],
				itemsTablet: [768, 2],
    			itemsMobile: [480, 2]

			});

			});

			$(document).ready(function() {
 
				$("#owl-video").owlCarousel({

					autoPlay: 6000, //Set AutoPlay to 3 seconds

					items : 1,
					itemsDesktop : [1199,3],
					itemsDesktopSmall : [979,3]

				});

			});
			$(document).ready(function(){
				// Image
				$('.bg-image, .post-wide .post-image, .post.single .post-image').each(function(){
					var src = $(this).children('img').attr('src');
					$(this).css('background-image','url('+src+')').children('img').hide();
				});
				
				//Video 
				var $bgVideo = $('.bg-video');
				if($bgVideo) {
					$bgVideo.YTPlayer();
				}
				if($(window).width() < 1200 && $bgVideo) {
					$bgVideo.prev('.bg-video-placeholder').show();
					$bgVideo.remove()
				}

			});
		</script>
	</body>
</html>