<!-- page footer -->
<footer id="footer">
	<div class="grid-row clear">
		<div class="footer">
			<div id="copyright">Copyright<span> Momento</span> 2019 - Todos os direitos reservados</div>
			<a href="index.html" class="footer-logo"><img src="images/logo.png" alt=""></a>
			<div class="social">
				<a href="https://www.facebook.com/agenciamomentomkt" target="_BLANK"><div class="contact-round"><i class="fab fa-facebook-f"></i></div></a>
				<a href="https://www.instagram.com/agencia_momento/"><div class="contact-round" target="_BLANK"><i class="fab fa-instagram"></i></div></a>
				<a href="#"><div class="contact-round"><i class="fab fa-youtube"></i></div></a>
			</div>
		</div>
	</div>
</footer>
<!--/ page footer -->
<!-- <a href="#" id="scroll-top" class="scroll-top"><i class="fa fa-angle-up"></i></a> -->