<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("includes/head.php")?>
</head>
<body>

<?php include("includes/menu-side-bar.php")?>
<?php include("includes/menu-bottom.php")?>

<header id="header-detalhe-card">
  <div class="d-flex justify-content-between menu-top">
      <div class="p-2 align-self-center">
          <a class="btn-back" href="#"><i class="material-icons">arrow_back</i> Voltar</a>
      </div>
      <div class="p-2 align-self-center">
          <a href="#" class="peoples">Oséas Moreto</a>
      </div>
      <div class="p-2 align-self-center">
          <ul class="list-unstyled list-inline m-0 list-icons-menu">
              <li class="list-inline-item"><a href="#" class="edit-icon"><i class="material-icons">edit</i></a></li>
          </ul>
      </div>
  </div>

  <div class="container-fluid">
        <div class="row">
          <div class="col-12 text-center">
            <h4 class="title-header mt-2 mb-3">Vamos colocar um status na conversa com <br><span class="bg-text-gradient">Oséas Moreto</span> ?</h4>
          </div>
        </div>
      </div>
</header>

<section id="section-add-status-01">
  <div class="container-fluid">
    <div class="row mt-4">
      <div class="col-12 text-center align-self-center">
        <h4 class="title mt-0 mb-3">novo status de conversa:</h4>
        <form action="">
            <div class="form-group">
                <textarea name="status" rows="3" class="form-control" placeholder="Digite aqui"></textarea>
            </div>

            <a href="#" class="defaut-btn-gradient2 border-0">Enviar para Oséas</a>
        </form>
      </div>
    </div>
  </div>
</section>

<section id="section-add-status-02">
  <div class="container-fluid">
    <div class="row mt-4">
      <div class="col-10 align-self-center">
        <h4 class="title mt-0 mb-3">Passando...</h4>
      </div>
    </div>

    <div class="row mb-100">
      <div class="col-12">
          <div class="card mb-2">
            <div class="card-body pb-4 position-relative">
             <div class="overlay-content">
                 <p class="card-subtitle mb-1 mt-1"><strong>15/05/2019 - 15:33</strong> Oi André?</p>
                 <p class="card-subtitle mb-1 -active"><strong>15/05/2019 - 15:33</strong> Oi André?</p>
                 <p class="card-subtitle mb-1"><strong>15/05/2019 - 15:33</strong> Oi André?</p>
                 <p class="card-subtitle mb-1"><strong>15/05/2019 - 15:33</strong> Oi André?</p>
                 <p class="card-subtitle mb-1"><strong>15/05/2019 - 15:33</strong> Oi André?</p>
                 <p class="card-subtitle mb-1"><strong>15/05/2019 - 15:33</strong> Oi André?</p>
                 <p class="card-subtitle mb-1"><strong>15/05/2019 - 15:33</strong> Oi André?</p>
                 <p class="card-subtitle mb-1"><strong>15/05/2019 - 15:33</strong> Oi André?</p>
             </div>
             <a href="#" class="btn-circle-gradient -aux"><i class="material-icons">add</i></a>
            </div>
          </div>
      </div>
    </div>
  </div>
</section>

<?php include("includes/script.php")?>
</body>
</html>