<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("includes/head.php")?>
</head>
<body>
<?php include("includes/menu-top-bar.php")?>
<?php include("includes/menu-side-bar.php")?>
<?php include("includes/menu-bottom.php")?>

<header id="header-promocao">
  <div class="container-fluid">
    <div class="row">
        <div class="col-12 text-center">
            <div class="box-info">
                <div class="bar-big"></div>
                <h1 class="title"><span>Olá Felipe,</span><br>Antes de fechar seu pedido Veja este vídeo</h1>
            </div>
        </div>
    </div>

    <div class="row mt-3">
        <div class="col-12 text-center">
            <div class="box-video">
                <iframe width="100%" height="215" src="https://www.youtube.com/embed/7H0pKlziMto"
                    frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen></iframe>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 text-center">
            <div class="box-info-2">
                <div class="bar-big"></div>
                <h1 class="title">Aproveite a promoção!<br><span>Quer 1, 2 ou 3?</span></h1>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-4 text-center">
            <div class="box-sup">
                <img src="assets/images/suporte-1.png" class="img-fluid"/>
                <p class="price">R$23,90<br><span>cada</span></p>
                <a href="#" class="defaut-btn-gradient">Adicionar</a>
            </div>
        </div>

        <div class="col-4 text-center">
            <div class="box-sup">
                <img src="assets/images/suporte-2.png" class="img-fluid"/>
                <p class="price">R$19,90<br><span>cada</span></p>
                <a href="#" class="defaut-btn-gradient">Adicionar</a>
            </div>
        </div>

        <div class="col-4 text-center">
            <div class="box-sup">
                <img src="assets/images/suporte-3.png" class="img-fluid"/>
                <p class="price">R$18,90<br><span>cada</span></p>
                <a href="#" class="defaut-btn-gradient">Adicionar</a>
            </div>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-5 align-self-center">
            <a href="#" class="defaut-btn-outline">Eu não quero</a>
        </div>

        <div class="col-7 align-self-center">
            <div class="row">
                <div class="col-3 align-self-center"><i class="flaticon-stopwatch timer"></i></div>
                <div class="col-9 align-self-center"><div id="getting-started"></div></div>
            </div>
        </div>
    </div>
  </div>
</header>


<?php include("includes/script.php")?>
</body>
</html>