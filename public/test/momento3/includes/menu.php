<header id="home">
    <!-- sticky menu -->
    <div class="stick-wrapper">
        <div class="sticky clear">
            <div class="grid-row">
                <img class="splash" src="img/splash.png" alt="">
                <div class="logo">
                    <a href="index.php"><img src="images/logo.png" alt=""></a>
                </div>
                <nav class="nav">
                    <a href="#" class="switcher">
                        <i class="fa fa-bars"></i>
                    </a>
                    <ul class="clear">
                        <li><a href="#home" class="active-link">Home</a></li>
                        <li><a href="#about">Sobre</a></li>
                        <li><a href="#team">Equipe</a></li>
                        <li><a href="#clientes">Clientes</a></li>
                        <li><a href="#contact">Contato</a></li>
                        <!-- <li class="last"><a href="briefing.php">Briefing</a></li> -->
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <!--/ sticky menu -->
</header>