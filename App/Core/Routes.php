<?php
namespace App\Core;

/**
 *
 */
class Routes {
      public function initroute($folder, $controller, $method, $params = []){
          require_once '../App/Controllers/'.$folder.'/'. ucfirst($controller) . '.php';
          $class = "App\\Controllers\\".$folder."\\".ucfirst($controller);
          $contr = new $class;

          if(method_exists($contr, $method)){
              call_user_func_array(array($contr, $method), $params);
          }
      }
}


?>
