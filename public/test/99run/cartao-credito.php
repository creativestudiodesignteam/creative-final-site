<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("includes/head.php")?>
</head>
<body>
<?php include("includes/menu-top-bar.php")?>
<?php include("includes/menu-side-bar.php")?>
<?php include("includes/menu-bottom.php")?>

<header id="header-credito">
  <div class="container-fluid">
    <div class="row">
        <div class="col-12 text-center">
            <div class="box-info">
                <h1 class="title"><span>Valor da compra:</span><br>R$ 76,00</h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card-wrapper mt-4"></div>

            <div class="form-container active mt-4">
                <form action="promocao.php">
                    <div class="form-row form-container active mt-4">
                        <div class="col-6 pr-0">
                            <input class="form-control rounded-top border-bottom border-right" placeholder="Número do cartão" type="tel" name="number">
                        </div>

                        <div class="col-6 pl-0">
                            <input class="form-control border-bottom" placeholder="Nome do titular rounded-right" type="text" name="name">
                        </div>
                        
                        <div class="col-4 pr-0">
                            <input class="form-control" placeholder="MM/AA" type="tel" name="expiry">
                        </div>
                        
                        <div class="col-4 p-0">
                            <input class="form-control" placeholder="CVC" type="number" name="cvc">
                        </div>

                        <div class="col-4 pl-0">
                            <input class="form-control" placeholder="parcelas">
                        </div>
                        
                        <div class="col-12">
                            <input class="form-control button-pay rounded-bottom rounded-bottom" value="Pagar agora" placeholder="CVC" type="submit">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-12 text-center"><img src="assets/images/ssl.png" /></div>
    </div>
    

  </div>
</header>

<?php include("includes/script.php")?>
<script>
        new Card({
            form: document.querySelector('form'),
            container: '.card-wrapper',
            placeholders: {
                number: '•••• •••• •••• ••••',
                name: 'Nome do titular',
                expiry: '••/••',
                cvc: '•••'
            },
        });
    </script>
</body>
</html>