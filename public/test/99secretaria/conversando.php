<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("includes/head.php")?>
</head>
<body>

<?php include("includes/menu-side-bar.php")?>
<?php include("includes/menu-bottom.php")?>

<header id="header-conversando">
  <div class="d-flex justify-content-between menu-top">
      <div class="p-2 align-self-center">
          <a id="show-sidebar" class="btn" href="#">
              <span class="bars"></span>
              <span class="bars"></span>
              <span class="bars"></span>
          </a>
      </div>
      <div class="p-2 align-self-center">
          <a href="#" class="peoples">8 Conversando</a>
      </div>
      <div class="p-2 align-self-center">
          <ul class="list-unstyled list-inline m-0 list-icons-menu">
              <li class="list-inline-item"><a href="#"><i class="material-icons">add</i></a></li>
          </ul>
      </div>
  </div>

  <div class="container-fluid">
        <div class="row">
          <div class="col-12 text-center">
            <form class="form-inline form-filter">
              <div class="form-group mb-2">
                <input type="text"  class="form-control" placeholder="Pesquisar">
              </div>

              <button type="submit" class="defaut-btn-gradient border-0 btn-filter">Filtrar</button>
            </form>

            <h4 class="title-header mt-2 mb-3">Você está falando com <span class="bg-text-gradient">08</span> pessoas</h4>
          </div>
        </div>
      </div>
</header>

<section id="section-conversando-01">
  <div class="container-fluid">
    <div class="row mt-4">
      <div class="col-12 align-self-center box-filters">
        <a href="#" class="defaut-btn-gradient -aux border-0 btn-filter">Conversando</a>
        <a href="#" class="defaut-btn-gradient no-bg -aux border-0 btn-filter">Finalizado</a>
        <a href="#" class="defaut-btn-gradient no-bg -aux border-0 btn-filter">Excluido</a>
      </div>
    </div>

    <div class="row mt-1">
      <div class="col-6 align-self-center">
        <p class="title-filter m-0">Ordernar por:</p>
      </div>

      <div class="col-6 align-self-center text-right">
        <div id="mydiv" class="dropdown transparentbar" style="z-index:4">
          <button class="btn btn-default dropdown-toggle" type="button" id="mybyn" data-toggle="dropdown"
          aria-haspopup="true" aria-expanded="true">Selecione <span class="caret"></span></button>
          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="#">maior</a>
            <a class="dropdown-item" href="#">Menor</a>
            <a class="dropdown-item" href="#">Recente</a>
          </div>
        </div>
      </div>
    </div>

    <div class="row mb-100">
      <div class="col-12">
        <a href="detalhe-card.php">
          <div class="card mb-2 position-relative card-border-alert">
            <div class="card-body">
              <h3 class="card-title mb-1">Oseas moreto (C&A)</h3>
              <h4 class="text-uppercase text-data">Sem data</h4>
              <p class="card-subtitle mb-2">15/05/2019 14:35</p>
              <p class="card-text">Como está o orçamento do kit?</p>
              <div class="row mt-3">
                <div class="col-8 align-self-center">
                  <div class="dropdown">
                    <button class="defaut-dropdown-gradient border-0 dropdown-toggle" type="button" id="dropdownMenuButton"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Escolha uma opção
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                      <a class="dropdown-item" href="#">Como estão as coisas?</a>
                      <a class="dropdown-item" href="#">Outra ação</a>
                      <a class="dropdown-item" href="#">Alguma coisa aqui</a>
                    </div>
                  </div>
                </div>

                <div class="col-4 pl-0 align-self-center">
                  <a href="#" class="btn btn-outline-secondary"><img src="assets/images/whats.png" width="19px" > Falar</a>
                </div>
              </div>
            </div>
          </div>
        </a>

        <a href="detalhe-card.php">
          <div class="card mb-2 position-relative">
            <div class="card-body">
              <h3 class="card-title mb-1">Oseas moreto (C&A)</h3>
              <p class="card-subtitle mb-2">15/05/2019 14:35</p>
              <p class="card-text">Como está o orçamento do kit?</p>
              <div class="row mt-3">
                <div class="col-8 align-self-center">
                  <div class="dropdown">
                    <button class="defaut-dropdown-gradient border-0 dropdown-toggle" type="button" id="dropdownMenuButton"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Escolha uma opção
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                      <a class="dropdown-item" href="#">Como estão as coisas?</a>
                      <a class="dropdown-item" href="#">Outra ação</a>
                      <a class="dropdown-item" href="#">Alguma coisa aqui</a>
                    </div>
                  </div>
                </div>

                <div class="col-4 pl-0 align-self-center">
                  <a href="#" class="btn btn-outline-secondary"><img src="assets/images/whats.png" width="19px" > Falar</a>
                </div>
              </div>
            </div>
          </div>
        </a>

        <a href="detalhe-card.php">
          <div class="card mb-2 position-relative">
            <span class="alert-card">2</span>
            <div class="card-body">
              <h3 class="card-title mb-1">Oseas moreto (C&A)</h3>
              <p class="card-subtitle mb-2">15/05/2019 14:35</p>
              <p class="card-text">Como está o orçamento do kit?</p>
              <div class="row mt-3">
                <div class="col-8 align-self-center">
                  <div class="dropdown">
                    <button class="defaut-dropdown-gradient border-0 dropdown-toggle" type="button" id="dropdownMenuButton"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Escolha uma opção
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                      <a class="dropdown-item" href="#">Como estão as coisas?</a>
                      <a class="dropdown-item" href="#">Outra ação</a>
                      <a class="dropdown-item" href="#">Alguma coisa aqui</a>
                    </div>
                  </div>
                </div>

                <div class="col-4 pl-0 align-self-center">
                  <a href="detalhe-card.php" class="btn btn-outline-secondary"><img src="assets/images/whats.png" width="19px" > Falar</a>
                </div>
              </div>
            </div>
          </div>
        </a>

        <a href="detalhe-card.php">
          <div class="card mb-2 position-relative">
            <div class="card-body">
              <h3 class="card-title mb-1">Oseas moreto (C&A)</h3>
              <p class="card-subtitle mb-2">15/05/2019 14:35</p>
              <p class="card-text">Como está o orçamento do kit?</p>
              <div class="row mt-3">
                <div class="col-8 align-self-center">
                  <div class="dropdown">
                    <button class="defaut-dropdown-gradient border-0 dropdown-toggle" type="button" id="dropdownMenuButton"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Escolha uma opção
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                      <a class="dropdown-item" href="#">Como estão as coisas?</a>
                      <a class="dropdown-item" href="#">Outra ação</a>
                      <a class="dropdown-item" href="#">Alguma coisa aqui</a>
                    </div>
                  </div>
                </div>

                <div class="col-4 pl-0 align-self-center">
                  <a href="#" class="btn btn-outline-secondary"><img src="assets/images/whats.png" width="19px" > Falar</a>
                </div>
              </div>
            </div>
          </div>
        </a>

        <a href="detalhe-card.php">
          <div class="card mb-5 position-relative">
            
            <div class="card-body">
              <h3 class="card-title mb-1">Oseas moreto (C&A)</h3>
              <p class="card-subtitle mb-2">15/05/2019 14:35</p>
              <p class="card-text">Como está o orçamento do kit?</p>
              <div class="row mt-3">
                <div class="col-8 align-self-center">
                  <div class="dropdown">
                    <button class="defaut-dropdown-gradient border-0 dropdown-toggle" type="button" id="dropdownMenuButton"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Escolha uma opção
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                      <a class="dropdown-item" href="#">Como estão as coisas?</a>
                      <a class="dropdown-item" href="#">Outra ação</a>
                      <a class="dropdown-item" href="#">Alguma coisa aqui</a>
                    </div>
                  </div>
                </div>

                <div class="col-4 pl-0 align-self-center">
                  <a href="#" class="btn btn-outline-secondary"><img src="assets/images/whats.png" width="19px" > Falar</a>
                </div>
              </div>
            </div>
          </div>
        </a>
      </div>
    </div>
  </div>
</section>


<?php include("includes/script.php")?>
</body>
</html>