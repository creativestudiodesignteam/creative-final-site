<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("includes/head.php")?>
</head>
<body>
<?php include("includes/menu-top-bar.php")?>
<?php include("includes/menu-side-bar.php")?>
<?php include("includes/menu-bottom.php")?>

<header id="header-loja">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="dropdown d-flex justify-content-end mt-2 mb-3">
            <button class="dropdown-toggle drop-menu" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-filter"></i> Ordenar por:
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                <button class="dropdown-item" type="button">Menor preço</button>
                <button class="dropdown-item" type="button">Maior preço</button>
                <button class="dropdown-item" type="button">Mais vendidos</button>
            </div>
        </div>
      </div>
    </div>

    <div class="row">
        <div class="col-6">
            <a href="produto.php">
                <figure class="card card-product">
                    <div class="img-wrap text-center">
                        <img src="assets/images/arqueiro-site.png" class="img-fluid" width="100px" />
                    </div>
                    <figcaption class="info-wrap">
                        <h5 class="title m-0">Medalha 6º Guerreiro</h5>
                    </figcaption>
                    <div class="bottom-wrap">
                        <a href="carrinho.php" class="float-right btn-add-cart">Adicionado</a>	
                        <div class="price-wrap h5">
                            <span class="price-new">R$36,00</span>
                        </div> 
                    </div>
                </figure>
            </a>
        </div>

        <div class="col-6">
            <a href="produto.php">
                <figure class="card card-product">
                    <div class="img-wrap text-center">
                        <img src="assets/images/arqueiro-site.png" class="img-fluid" width="100px" />
                    </div>
                    <figcaption class="info-wrap">
                        <h5 class="title m-0">Medalha 6º Guerreiro</h5>
                    </figcaption>
                    <div class="bottom-wrap">
                        <a href="carrinho.php" class="float-right btn-circle-cart -aux"><i class="flaticon-shopping-cart"></i></a>	
                        <div class="price-wrap h5">
                        <span class="price-new">R$36,00</span>
                        </div> 
                    </div>
                </figure>
            </a>
        </div>
    </div>

    <div class="row">
        <div class="col-6">
            <a href="produto.php">
                <figure class="card card-product">
                    <div class="img-wrap text-center">
                        <img src="assets/images/arqueiro-site.png" class="img-fluid" width="100px" />
                    </div>
                    <figcaption class="info-wrap">
                        <h5 class="title m-0">Medalha 6º Guerreiro</h5>
                    </figcaption>
                    <div class="bottom-wrap">
                        <a href="carrinho.php" class="float-right btn-circle-cart -aux"><i class="flaticon-shopping-cart"></i></a>	
                        <div class="price-wrap h5">
                        <span class="price-new">R$36,00</span>
                        </div> 
                    </div>
                </figure>
            </a>
        </div>

        <div class="col-6">
            <a href="produto.php">
                <figure class="card card-product">
                    <div class="img-wrap text-center">
                        <img src="assets/images/arqueiro-site.png" class="img-fluid" width="100px" />
                    </div>
                    <figcaption class="info-wrap">
                        <h5 class="title m-0">Medalha 6º Guerreiro</h5>
                    </figcaption>
                    <div class="bottom-wrap">
                        <a href="carrinho.php" class="float-right btn-circle-cart -aux"><i class="flaticon-shopping-cart"></i></a>	
                        <div class="price-wrap h5">
                        <span class="price-new">R$36,00</span>
                        </div> 
                    </div>
                </figure>
            </a>
        </div>
    </div>

    <div class="row">
        <div class="col-6">
            <a href="produto.php">
                <figure class="card card-product">
                    <div class="img-wrap text-center">
                        <img src="assets/images/arqueiro-site.png" class="img-fluid" width="100px" />
                    </div>
                    <figcaption class="info-wrap">
                        <h5 class="title m-0">Medalha 6º Guerreiro</h5>
                    </figcaption>
                    <div class="bottom-wrap">
                        <a href="carrinho.php" class="float-right btn-circle-cart -aux"><i class="flaticon-shopping-cart"></i></a>	
                        <div class="price-wrap h5">
                        <span class="price-new">R$36,00</span>
                        </div> 
                    </div>
                </figure>
            </a>
        </div>

        <div class="col-6">
            <a href="produto.php">
                <figure class="card card-product">
                    <div class="img-wrap text-center">
                        <img src="assets/images/arqueiro-site.png" class="img-fluid" width="100px" />
                    </div>
                    <figcaption class="info-wrap">
                        <h5 class="title m-0">Medalha 6º Guerreiro</h5>
                    </figcaption>
                    <div class="bottom-wrap">
                        <a href="carrinho.php" class="float-right btn-circle-cart -aux"><i class="flaticon-shopping-cart"></i></a>	
                        <div class="price-wrap h5">
                        <span class="price-new">R$36,00</span>
                        </div> 
                    </div>
                </figure>
            </a>
        </div>
    </div>
  </div>
</header>

<?php include("includes/script.php")?>
</body>
</html>