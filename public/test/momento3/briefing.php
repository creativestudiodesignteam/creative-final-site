<!DOCTYPE HTML>
<html>

<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/wizard.css">
    <?php include("includes/head.php")?>
</head>
<body class="contact-page pc">
	<!-- page header -->
	<?php include("includes/menu.php")?>
	<!--/ page header  -->
	<div class="top-bg">
		
    	<img src="img/splash-top.png" class="splash-top" alt>
		<div class="page-title zoomIn animated">contact</div>
	</div>
	<!-- page content -->
	<div class="page-content">
		<div class="grid-row">
			<div id="content" role="main">
				<!-- <div class="title">
					<span class="main-title">CONTACT</span><span class="slash-icon">/<i class="fa fa-angle-double-right"></i></span><h5>GET IN TOUCH WITH US</h5>
                </div> -->
                
                <div class="wizard-main">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12 login-sec">
                                <div class="login-sec-bg">
                                    <h2 class="text-center">Formulário de briefing</h2>
                                    <div class="composer">
                                        <div class="info-boxes error-message" id="feedback-form-errors">
                                            <div class="info-box-icon"><i class="fa fa-times"></i></div>
                                            <strong>Error box</strong><br />
                                            <div class="message"></div>
                                        </div>
                                        <div class="info-boxes confirmation-message" id="feedback-form-success">
                                            <div class="info-box-icon"><i class="fa fa-check"></i></div>
                                            <strong>Confirmation box</strong><br />Vestibulum sodales pellentesque nibh quis imperdiet
                                            <div class="close-button"><i class="fa fa-times"></i></div>
                                        </div>
                                    </div>
                                    
                                    <div class="email_server_responce"></div>

                                    <form id="example-advanced-form" action="php/contacts-process.php"  method="post" style="display: none;">
                                        <h3>Cadastro de cliente</h3>
                                        <fieldset class="form-input">
                                            <!-- <h4>Account Information</h4> -->

                                            <label for="nomeFantasiaEmpresa">Nome Fantasia da Empresa</label>
                                            <input id="nomeFantasiaEmpresa" name="nomeFantasiaEmpresa" type="text" class="form-control">
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <label for="nomeEmpresa">Nome da Empresa</label>
                                                    <input id="confirm" name="nomeEmpresa" type="text" class="form-control">
                                                </div>

                                                <div class="col-lg-6">
                                                    <label for="cpf">CNPJ ou CPF</label>
                                                    <input id="confirm" name="cpf" type="text" class="form-control">
                                                </div>
                                            </div>
                                           
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <label for="nomeCliente">Nome do Cliente</label>
                                                    <input id="confirm" name="nomeCliente" type="text" class="form-control">
                                                </div>
                                                
                                                <div class="col-lg-6">         
                                                    <label for="email">E-mail</label>
                                                    <input id="confirm" name="email" type="text" class="form-control">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <label for="whatsapp">Whatsapp</label>
                                                    <input id="confirm" name="whatsapp" type="text" class="form-control">
                                                </div>
                                                
                                                <div class="col-lg-6">         
                                                    <label for="telefoneFixo">Telefone fixo</label>
                                                    <input id="confirm" name="telefoneFixo" type="text" class="form-control">
                                                </div>
                                            </div>

                                            <label for="enderecoFisico">Endereço físico</label>
                                            <input id="confirm" name="enderecoFisico" type="text" class="form-control">
                                            
                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <label for="possuiSite">Possui algum site hoje?</label>
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="possuiSiteSim" name="possuiSite" class="custom-control-input">
                                                        <label class="custom-control-label" for="possuiSiteSim">Sim</label>
                                                    </div>

                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="possuiSiteNao" name="possuiSite" class="custom-control-input">
                                                        <label class="custom-control-label" for="possuiSiteNao">Não</label>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-lg-9">         
                                                    <label for="qualSite">Se sim qual?</label>
                                                    <input id="confirm" name="qualSite" type="text" class="form-control">
                                                </div>
                                            </div>
                                            

                                            <label for="confirm">Horário de funcionamento:</label><br>
                                            <label>segunda</label>
                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <input type="time" id="appt" name="segundaAbre" class="form-control">
                                                </div>
                                                <div class="col-lg-3">
                                                    <input type="time" id="appt" name="segundaFecha" class="form-control">
                                                </div>
                                            </div>

                                            <label>Terça</label>
                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <input type="time" id="appt" name="tercaAbre" class="form-control">
                                                </div>
                                                <div class="col-lg-3">
                                                    <input type="time" id="appt" name="tercaFecha" class="form-control">
                                                </div>
                                            </div>

                                            <label>Quarta</label>
                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <input type="time" id="appt" name="quartaAbre" class="form-control">
                                                </div>
                                                <div class="col-lg-3">
                                                    <input type="time" id="appt" name="quartaFecha" class="form-control">
                                                </div>
                                            </div>

                                            <label>Quinta</label>
                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <input type="time" id="appt" name="quintaAbre" class="form-control">
                                                </div>
                                                <div class="col-lg-3">
                                                    <input type="time" id="appt" name="quintaFecha" class="form-control">
                                                </div>
                                            </div>

                                            <label>Sexta</label>
                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <input type="time" id="appt" name="sextaAbre" class="form-control">
                                                </div>
                                                <div class="col-lg-3">
                                                    <input type="time" id="appt" name="sextaFecha" class="form-control">
                                                </div>
                                            </div>

                                            <label>Sábado</label>
                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <input type="time" id="appt" name="sabadoAbre" class="form-control">
                                                </div>
                                                <div class="col-lg-3">
                                                    <input type="time" id="appt" name="sabadoFecha" class="form-control">
                                                </div>
                                            </div>

                                            <label>Domingo</label>
                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <input type="time" id="appt" name="domingoAbre" class="form-control">
                                                </div>
                                                <div class="col-lg-3">
                                                    <input type="time" id="appt" name="domingoFecha" class="form-control">
                                                </div>
                                            </div>
                                            
                                            <label>Segmento de atuação</label>
                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <select class="form-control" name="segmentoAtuacao" id="segmentoAtuacao">
                                                        <option disabled selected>selecione um segmento</option>
                                                        <option value="alimentos-bebidas">Alimentos e Bebidas</option>
                                                        <option value="agricultura-pecuaria-piscicultura">Agricultura / Pecuária / Piscicultura</option>
                                                        <option value="arte-antiguidades">Arte e Antiguidades</option>
                                                        <option value="artigos-religiosos">Artigos Religiosos</option>
                                                        <option value="assinaturas-revistas">Assinaturas e Revistas</option>
                                                        <option value="automoveis-veiculos">Automóveis e Veículos</option>
                                                        <option value="bebes-cia">Bebês e Cia</option>
                                                        <option value="brinquedos-games">Brinquedos e Games</option>
                                                        <option value="casa-decoracao">Casa e Decoração</option>
                                                        <option value="compras-coletivas">Compras Coletivas</option>
                                                        <option value="construcao-ferramentas">Construção e Ferramentas</option>
                                                        <option value="cosmeticos-perfumaria">Cosméticos e Perfumaria</option>
                                                        <option value="cursos-educacao">Cursos e Educação</option>
                                                        <option value="eletrodomesticos">Eletrodomésticos</option>
                                                        <option value="eletronicos">Eletrônicos</option>
                                                        <option value="empresas-telemarketing">Empresas de Telemarketing</option>
                                                        <option value="esporte-lazer">Esporte e Lazer</option>
                                                        <option value="flores-cestas-presentes">Flores, Cestas e Presentes</option>
                                                        <option value="fotografia">Fotografia</option>
                                                        <option value="industria-comercio-negocios">Indústria, Comércio e Negócios</option>
                                                        <option value="informatica">Informática</option>
                                                        <option value="ingressos">Ingressos</option>
                                                        <option value="instrumentos-musicais">Instrumentos Musicais</option>
                                                        <option value="joalheria">Joalheria</option>
                                                        <option value="livros">Livros</option>
                                                        <option value="moda-acessorios">Moda e Acessórios</option>
                                                        <option value="moteis">Motéis</option>
                                                        <option value="musica">Música</option>
                                                        <option value="papelaria-escritorio">Papelaria e Escritório</option>
                                                        <option value="pet-shop">Pet Shop</option>
                                                        <option value="saude-bemestar">Saúde e Bem Estar</option>
                                                        <option value="servico-advocatucios">Serviço Advocaticios</option>
                                                        <option value="servico-estacionamento">Serviços de Estacionamentos</option>
                                                        <option value="servicos-exportacao-importacao">Serviços de Exportação / Importação</option>
                                                        <option value="servicos-limpeza">Serviços de limpeza</option>
                                                        <option value="servicos-seguradoras">Serviços de Seguradoras</option>
                                                        <option value="servicos-seguranca">Serviços de Segurança</option>
                                                        <option value="servicos-alimentacao">Serviços de Alimentação</option>
                                                        <option value="servicos-ecologia-meio-ambiente">Serviços em Ecologia / Meio Ambiente</option>
                                                        <option value="servicos-festas-eventos">Serviços em Festas / Eventos</option>
                                                        <option value="servicos-financeiros">Serviços Financeiros</option>
                                                        <option value="servicos-funerarios">Serviços Funerários</option>
                                                        <option value="sex-shop">Sex Shop</option>
                                                        <option value="shopping-centers">Shopping Centers</option>
                                                        <option value="tabacaria">Tabacaria</option>
                                                        <option value="telefonia">Telefonia</option>
                                                        <option value="turismo">Turismo</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </fieldset>

                                        <h3>Geral</h3>
                                        <fieldset class="form-input">
                                            <h4>Contexto da Marca </h4>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <label for="comoSurgiu">Como surgiu?</label>
                                                    <input id="confirm" name="comoSurgiu" type="text" class="form-control">
                                                </div>
                                                
                                                <div class="col-lg-6">         
                                                    <label for="oqueFaz">O que faz?</label>
                                                    <input id="confirm" name="oqueFaz" type="text" class="form-control">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <label for="quaisProdutos">Quais produtos?</label>
                                                    <input id="confirm" name="quaisProdutos" type="text" class="form-control">
                                                </div>

                                                <div class="col-lg-6">         
                                                    <label for="produtosMaisVendidos">Produtos mais vendidos?</label>
                                                    <input id="confirm" name="produtosMaisVendidos" type="text" class="form-control">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <label for="produtosPromocao">Produtos em promoção?</label>
                                                    <input id="confirm" name="produtosPromocao" type="text" class="form-control">
                                                </div>

                                                <div class="col-lg-6">
                                                    <label for="produtosPromocao">Quais serviços oferece?</label>
                                                    <input id="confirm" name="produtosPromocao" type="text" class="form-control">
                                                </div>
                                                
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <label for="produtosPromocao">Serviços mais vendidos?</label>
                                                    <input id="confirm" name="produtosPromocao" type="text" class="form-control">
                                                </div>

                                                <div class="col-lg-6">
                                                    <label for="produtosPromocao">Serviços em promoção?</label>
                                                    <input id="confirm" name="produtosPromocao" type="text" class="form-control">
                                                </div>
                                            </div>

                                            <h4>Concorrentes: </h4>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <label for="produtosPromocao">Nome:</label>
                                                    <input id="confirm" name="produtosPromocao" type="text" class="form-control">
                                                </div>

                                                <div class="col-lg-6">
                                                    <label for="produtosPromocao">Site:</label>
                                                    <input id="confirm" name="produtosPromocao" type="text" class="form-control">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <label for="produtosPromocao">Nome:</label>
                                                    <input id="confirm" name="produtosPromocao" type="text" class="form-control">
                                                </div>

                                                <div class="col-lg-6">
                                                    <label for="produtosPromocao">Site:</label>
                                                    <input id="confirm" name="produtosPromocao" type="text" class="form-control">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <label for="produtosPromocao">Nome:</label>
                                                    <input id="confirm" name="produtosPromocao" type="text" class="form-control">
                                                </div>

                                                <div class="col-lg-6">
                                                    <label for="produtosPromocao">Site:</label>
                                                    <input id="confirm" name="produtosPromocao" type="text" class="form-control">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <label for="produtosPromocao">Qual é o objetivo do negócio? <br>(aumentar as vendas de clientes novos? Aumentar recompra dos consumidores? Mudar/Criar percepção de marca?)</label>
                                                    <input id="confirm" name="produtosPromocao" type="text" class="form-control">
                                                </div>
                                                
                                                <div class="col-lg-12">
                                                    <label for="produtosPromocao">Tem uma meta de faturamento no ano?</label>
                                                    <input id="confirm" name="produtosPromocao" type="text" class="form-control">
                                                </div>
                                            </div>

                                            <h4>Público-alvo </h4>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <label for="confirm">Quem é (Gênero):</label>
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                                                        <label class="custom-control-label" for="customRadio1">masculino</label>
                                                    </div>

                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                                                        <label class="custom-control-label" for="customRadio2">Feminino</label>
                                                    </div>

                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                                                        <label class="custom-control-label" for="customRadio2">Outros</label>
                                                    </div>
                                                </div>

                                                <div class="col-lg-6">
                                                    <label for="confirm">Qual a Faixa Etária:</label>
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                                                        <label class="custom-control-label" for="customRadio1">Todas as idades</label>
                                                    </div>

                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                                                        <label class="custom-control-label" for="customRadio2">1-5 anos</label>
                                                    </div>

                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                                                        <label class="custom-control-label" for="customRadio2">6-13 anos</label>
                                                    </div>

                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                                                        <label class="custom-control-label" for="customRadio2">13-17 anos</label>
                                                    </div>

                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                                                        <label class="custom-control-label" for="customRadio2">18-24 anos</label>
                                                    </div>

                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                                                        <label class="custom-control-label" for="customRadio2">25-34 anos</label>
                                                    </div>

                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                                                        <label class="custom-control-label" for="customRadio2">35-44 anos</label>
                                                    </div>

                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                                                        <label class="custom-control-label" for="customRadio2">45-54 anos</label>
                                                    </div>

                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                                                        <label class="custom-control-label" for="customRadio2">55-64 anos</label>
                                                    </div>

                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                                                        <label class="custom-control-label" for="customRadio2">64+</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <label for="produtosPromocao">Onde seu cliente reside?</label>
                                                    <input id="confirm" name="produtosPromocao" type="text" class="form-control">
                                                </div>
                                                
                                                <div class="col-lg-12">   
                                                    <label for="produtosPromocao">O que seu cliente valoriza na hora de comprar o seu produto, qual é o canal que ele costuma comprar/consumir conteúdo?</label>
                                                    <input id="confirm" name="produtosPromocao" type="text" class="form-control">  
                                                </div>

                                                <div class="col-lg-12">
                                                    <label for="produtosPromocao">Como você é percebido pelo seu público e como você gostaria de ser percebido?</label>
                                                    <input id="confirm" name="produtosPromocao" type="text" class="form-control"> 
                                                </div>

                                                <div class="col-lg-12">
                                                    <label for="produtosPromocao">Que mensagem sua marca NÃO deve transmitir para seus clientes?</label>
                                                    <input id="confirm" name="produtosPromocao" type="text" class="form-control">
                                                </div>
                                            </div>

                                            <h4 style="text-transform: inherit;">Me ajude a entender como sua marca deve se comportar visualmente, fornecendo algumas características básicas. Selecione 3 opções que melhor representam sua marca: </h4>
                                           
                                            <div class="row">
                                                <div class="col-lg-6 mt-3">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="customCheck" name="example1">
                                                        <label class="custom-control-label" for="customCheck">moderna</label>
                                                    </div>

                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="customCheck2" name="example2">
                                                        <label class="custom-control-label" for="customCheck2">séria</label>
                                                    </div>

                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="customCheck3" name="example3">
                                                        <label class="custom-control-label" for="customCheck3">autêntica</label>
                                                    </div>

                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="customCheck4" name="example4">
                                                        <label class="custom-control-label" for="customCheck4">conservadora</label>
                                                    </div>

                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="customCheck5" name="example5">
                                                        <label class="custom-control-label" for="customCheck5">descontraída</label>
                                                    </div>

                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="customCheck6" name="example6">
                                                        <label class="custom-control-label" for="customCheck6">singular</label>
                                                    </div>

                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="customCheck7" name="example7">
                                                        <label class="custom-control-label" for="customCheck7">marcante</label>
                                                    </div>

                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="customCheck8" name="example8">
                                                        <label class="custom-control-label" for="customCheck8">sofisticada</label>
                                                    </div>

                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="customCheck9" name="example9">
                                                        <label class="custom-control-label" for="customCheck9">diferentona</label>
                                                    </div>

                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="customCheck10" name="example10">
                                                        <label class="custom-control-label" for="customCheck10">minimalista</label>
                                                    </div>

                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="customCheck11" name="example11">
                                                        <label class="custom-control-label" for="customCheck11">extravagante</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>

                                        <h3>Escopo de trabalho</h3>
                                        <fieldset class="form-input">
                                            <h4>Profile Information</h4>
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio1">Design Gráfico</label>
                                            </div>

                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio2">Logotipo</label>
                                            </div>

                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio2">Papel Timbrado</label>
                                            </div>

                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio2">Papel Timbrado</label>
                                            </div>

                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio2">Apresentação</label>
                                            </div>

                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio2">Flyer</label>
                                            </div>

                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio2">Banner</label>
                                            </div>

                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio2">Vídeo</label>
                                            </div>

                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio2">Cartão Digital</label>
                                            </div>
                                            
                                            <h4 style="text-transform: inherit;">Precisa de um manual de identidade da marca? (É um manual com normas técnicas, recomendações e especificações para a utilização da identidade de uma determinada marca, o qual serve para facilitar a memorização e reconhecimento de uma determinada marca ao preservar suas propriedades visuais em todas suas manifestações.</h4>
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio2">Sim</label>
                                            </div>

                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio2">não</label>
                                            </div>

                                            <label for="comoSurgiu">Outros</label>
                                            <input id="confirm" name="comoSurgiu" type="text" class="form-control">

                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio1">Web</label>
                                            </div>
                                            
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio1">Website institucional</label>
                                            </div>
                                            
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio1">E-commerce</label>
                                            </div>
                                            
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio1">Facebook</label>
                                            </div>
                                            
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio1">Instagram</label>
                                            </div>
                                            
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio1">Linkedin</label>
                                            </div>
                                            
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio1">Google Meu Negócio</label>
                                            </div>
                                            
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio1">Instagram Inteligente</label>
                                            </div>
                                            
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio1">E-mail Marketing</label>
                                            </div>
                                            
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio1">Blog</label>
                                            </div>

                                            <label for="confirm">Vamos Fazer Campanhas Patrocinadas?</label>
                                            
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio1">Sim</label>
                                            </div>
                                            
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio1">não</label>
                                            </div>

                                            <div class="col-lg-12">
                                                <label for="comoSurgiu">Qual o Valor mensal de Investimento?</label>
                                                <input id="confirm" name="comoSurgiu" type="text" class="form-control">
                                            </div>

                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio1">Boleto</label>
                                            </div>
                                            
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio1">Cartão</label>
                                            </div>

                                            <label for="confirm">Links Patrocinados</label>

                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio1">Google Adwords</label>
                                            </div>
                                            
                                            <div class="col-lg-12">
                                                <label for="comoSurgiu">Qual o Valor mensal de Investimento? </label>
                                                <input id="confirm" name="comoSurgiu" type="text" class="form-control">
                                            </div>

                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio1">Boleto</label>
                                            </div>
                                            
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio1">Cartão</label>
                                            </div>

                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio1">SEO</label>
                                            </div>

                                            <div class="col-lg-12">
                                                <label for="comoSurgiu">Sua empresa já tem identidade visual? </label>
                                                <input id="confirm" name="comoSurgiu" type="text" class="form-control">
                                            </div>

                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio1">Sim</label>
                                            </div>
                                            
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio1">não</label>
                                            </div>
                                        </fieldset>

                                        <h3>Logotipo</h3>
                                        <fieldset class="form-input">
                                            <h4>Qual tipo de logo representa melhor sua marca? imagem bugada</h4>
                                            <!-- <input type="radio" name="imagem" id="i1" class="input-radius-image"/>
                                            <label for="i1"><img src="http://vkontakte.ru/images/gifts/256/81.jpg" alt=""></label> -->
                                            
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <label for="confirm">Pretende registrar legalmente sua marca:</label>
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                                                        <label class="custom-control-label" for="customRadio1">Sim</label>
                                                    </div>
                                                    
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                                                        <label class="custom-control-label" for="customRadio1">não</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <label for="confirm">Sua marca possui um slogan:</label>
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                                                        <label class="custom-control-label" for="customRadio1">Sim</label>
                                                    </div>
                                                    
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                                                        <label class="custom-control-label" for="customRadio1">não</label>
                                                    </div>

                                                    <label for="nomeEmpresa">Qual?</label>
                                                    <input id="confirm" name="nomeEmpresa" type="text" class="form-control">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <label for="confirm">Possui alguma preferência de cor:</label>
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                                                        <label class="custom-control-label" for="customRadio1">Sim</label>
                                                    </div>
                                                    
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                                                        <label class="custom-control-label" for="customRadio1">não</label>
                                                    </div>

                                                    <label for="nomeEmpresa">Qual?</label>
                                                    <input id="confirm" name="nomeEmpresa" type="text" class="form-control">

                                                    <label for="nomeEmpresa">Existe alguma cor que não gostaria que estivesse presente no logotipo?</label>
                                                    <input id="confirm" name="nomeEmpresa" type="text" class="form-control">
                                                </div>
                                            </div>

                                        </fieldset>

                                        <h3>Site</h3>
                                        <fieldset class="form-input">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <label for="confirm">Já possui o domínio?</label>
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                                                        <label class="custom-control-label" for="customRadio1">Sim</label>
                                                    </div>
                                                    
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                                                        <label class="custom-control-label" for="customRadio1">não</label>
                                                    </div>
                                                    
                                                    <h4 style="text-transform: inherit;">Acesso ao domínio</h4>
                                                    <label for="confirm">Link</label>
                                                    <input id="confirm" name="confirm" type="text" class="form-control">

                                                    <label for="confirm">Usuário</label>
                                                    <input id="confirm" name="confirm" type="text" class="form-control">

                                                    <label for="confirm">Senha</label>
                                                    <input id="confirm" name="confirm" type="text" class="form-control">

                                                    <h4 style="text-transform: inherit;">Acesso à hospedagem:</h4>
                                                    <label for="confirm">Link</label>
                                                    <input id="confirm" name="confirm" type="text" class="form-control">

                                                    <label for="confirm">Usuário</label>
                                                    <input id="confirm" name="confirm" type="text" class="form-control">

                                                    <label for="confirm">Senha</label>
                                                    <input id="confirm" name="confirm" type="text" class="form-control">

                                                    <h4 style="text-transform: inherit;">Acesso ao painel administrativo do site </h4>
                                                    <label for="confirm">Link</label>
                                                    <input id="confirm" name="confirm" type="text" class="form-control">

                                                    <label for="confirm">Usuário</label>
                                                    <input id="confirm" name="confirm" type="text" class="form-control">

                                                    <label for="confirm">Senha</label>
                                                    <input id="confirm" name="confirm" type="text" class="form-control">

                                                    <h4 style="text-transform: inherit;">Acesso FTP ao servidor (Host, Usuário e Senha) </h4>
                                                    <label for="confirm">Link</label>
                                                    <input id="confirm" name="confirm" type="text" class="form-control">

                                                    <label for="confirm">Usuário</label>
                                                    <input id="confirm" name="confirm" type="text" class="form-control">

                                                    <label for="confirm">Senha</label>
                                                    <input id="confirm" name="confirm" type="text" class="form-control">

                                                    <label for="confirm">Qual é o objetivo do site:</label>
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="customCheck100" name="example100">
                                                        <label class="custom-control-label" for="customCheck100">Presença digital</label>
                                                    </div>

                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="customCheck1002" name="example2">
                                                        <label class="custom-control-label" for="customCheck1002">Geração de conteúdo</label>
                                                    </div> 

                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="customCheck1002" name="example2">
                                                        <label class="custom-control-label" for="customCheck1002">Captação de leads</label>
                                                    </div> 

                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="customCheck1002" name="example2">
                                                        <label class="custom-control-label" for="customCheck1002">Vendas diretas</label>
                                                    </div> 

                                                    <h4 style="text-transform: inherit;">Redes sociais </h4>
                                                    <label for="confirm">Link da página do Facebook</label>
                                                    <input id="confirm" name="confirm" type="text" class="form-control">
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                                                        <label class="custom-control-label" for="customRadio1">Não temos</label>
                                                    </div>

                                                    <label for="confirm">Link do perfil do Instagram</label>
                                                    <input id="confirm" name="confirm" type="text" class="form-control">
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                                                        <label class="custom-control-label" for="customRadio1">Não temos</label>
                                                    </div>

                                                    <label for="confirm">Link da página do Linkedin</label>
                                                    <input id="confirm" name="confirm" type="text" class="form-control">
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                                                        <label class="custom-control-label" for="customRadio1">Não temos</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </form>			
                                </div>
                            </div>			
                        </div>
                    </div>
                </div>
			</div>
		</div>
    </div>
    
        <?php include("includes/footer.php");?>
		<?php include("includes/scripts.php");?>

	
</body>
</html>