<?php

namespace App\Controllers\Sistema;

use \App\Controllers\Sistema\Common;
use \App\Models\Entities\Budget;
use \App\Services\ServiceBudget;

/**
 * Description of Home
 *
 * @author oseas
 */
class PdfGenerator extends Common
{

    protected $pagelink = 'sistema/pdfgenerator';

    public function __construct()
    {
        $this->start_session();
        $this->logado();
        $this->view = new \stdClass();
        $this->service = new ServiceBudget();
        $this->obj = new Budget();
    }

    public function index() {
        $this->start_session();

        $this->titulo_pagina = 'Orçamentos';
        $objs = Budget::all();

        $breadcrumb['url'][0] = '#';
        $breadcrumb['label'][0] = 'Lista de Orçamentos ';
        $breadcrumb['active'][0] = 'active';
        $this->view->breadcrumb = $breadcrumb;
        $this->view->url = '/'.$this->pagelink;
        $this->view->titulo = 'Lista de Orçamentos';
        $this->view->objs = $objs;

        if(isset($_SESSION['message'])){
            $this->view->errormessage = $_SESSION['message'];
            $this->view->classe = $_SESSION['classe'];
            unset($_SESSION['message']);
            unset($_SESSION['classe']);
        }

        $this->render('index', $this->folder, $this->page);
    }

    public function create(){
        $this->start_session();
        $this->titulo_pagina = 'Orçamentos - Cadastro';
        $this->id_class  = 'cadastrar-anuncio';

        $breadcrumb['url'][0] = '/'.$this->pagelink;
        $breadcrumb['label'][0] = 'Lista de Orçamentos';
        $breadcrumb['active'][0] = '';
        $breadcrumb['url'][1] = '#';
        $breadcrumb['label'][1] = 'Cadastrar';
        $breadcrumb['active'][1] = 'active';
        $this->view->breadcrumb = $breadcrumb;

        $this->view->url = '/'.$this->pagelink;
        $this->view->action = '/'.$this->pagelink.'/create';
        $this->view->titulo = 'Lista de Orçamentos';
        $this->view->label = 'Cadastrar';
        $this->view->class_btn = 'btn btn-success';
        $this->view->obj = $this->obj;

        if($_POST){
            $_POST['date_create'] = date('Y-m-d H:i:s');
            $_POST['date_update'] = date('Y-m-d H:i:s');
            $request = $this->service->create($_POST);
            $obj = $request['success'] ? $request['data'] : null;

            if($request['success']){
                $_SESSION['message'] = "Orçamento <strong>{$obj->client}</strong> Inserido com sucesso!";
                $_SESSION['classe'] = 'alert-success';
                $return['response']['mensagem'] = $_SESSION['message'];
                $return['response']['classe'] = $_SESSION['classe'];
                $return['response']['result'] = 'success';
                $return['response']['redirect'] = '/'.$this->pagelink.'/update/'.$obj->idbudget;
            }else{
                $return['response']['mensagem'] = $request['message'];
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';
            }

            print_r(json_encode($return));
            exit();
        }

        $this->render('form', $this->folder, $this->page);
    }

    public function update($id, $data = []){
        $this->start_session();
        $this->titulo_pagina = 'Orçamentos - Editar';
        $this->id_class  = 'cadastrar-anuncio';
        $obj = Budget::find($id);

        $breadcrumb['url'][0] = '/'.$this->pagelink;
        $breadcrumb['label'][0] = 'Lista de Orçamentos ';
        $breadcrumb['active'][0] = '';
        $breadcrumb['url'][1] = '#';
        $breadcrumb['label'][1] = 'Alterar';
        $breadcrumb['active'][1] = 'active';
        $this->view->breadcrumb = $breadcrumb;

        $this->view->url = '/'.$this->pagelink;
        $this->view->action = '/'.$this->pagelink.'/update/'.$id;
        $this->view->titulo = 'Lista de Orçamentos';
        $this->view->label = 'Alterar';
        $this->view->class_btn = 'btn btn-warning';
        $this->view->obj = $obj;


        if($_POST){
            $_POST['date_create'] = $obj->date_create;
            $_POST['date_update'] = date('Y-m-d H:i:s');
            $request = $this->service->update($_POST);
            $obj = $request['success'] ? $request['data'] : null;

            if($request['success']){
                $_SESSION['message'] = "Orçamento <strong>{$obj->client}</strong> alterado com sucesso!";
                $_SESSION['classe'] = 'alert-success';
                $return['response']['mensagem'] = $_SESSION['message'];
                $return['response']['classe'] = $_SESSION['classe'];
                $return['response']['result'] = 'success';
                $return['response']['redirect'] = '/'.$this->pagelink.'/update/'.$id;
            }else{
                $return['response']['mensagem'] = $request['message'];
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';
            }

            print_r(json_encode($return));
            exit();
        }


        $this->render('form', $this->folder, $this->page);
    }

    public function destroy($id){
        $this->start_session();
        $obj = Budget::find($id);
        $request = $this->service->destroy($id);

        if($request['success']){
            $_SESSION['message'] = "Orçamento <strong>{$obj->client}</strong> excluido com sucesso!";
            $_SESSION['classe'] = 'alert-success';
        }else{
            $_SESSION['message'] = "Não foi possivel excluir o Orçamento <strong>{$obj->client}</strong>";
            $_SESSION['classe'] = 'alert-danger';
        }

        print_r(json_encode($request));
        exit();

    }

    public function generate($id)
    {
        set_time_limit(600);
        
        try {

            $obj = Budget::find($id);

            $css = '<style>

            * { margin: 0; padding: 0; }
            h1,h2, h3, h4,h5, h6, p, a{
                color: #3a3a3a;
            }
    
            .container{
                width: 100%;
                height: 768px;
                margin: 0 auto;
            }
    
            .position-relative{
                //position: relative;
            }
    
            
    
            .site-bar{
                position: fixed;
                left: 0;
                margin-top: 110px;
            }
    
            .centered {
                position: fixed;
                margin-top: 130px;
                margin-left: 70px;
            }

            .centered-2 {
                position: fixed;
                margin-top: 130px;
                margin-left: 70px;
            }
    
            .info{
    
            }
            
    
            .logo{
                position: fixed;
                margin-left: 70px;
                margin-top: 30px;
            }
    
            .number{
                position: fixed;
                right: 70px;
                top: 30px;
                font-family: "GothamBold";
                border-radius: 50%;
                width: 45px;
                height: 45px;
                color: #fff;
                line-height: 45px;
                text-align: center;
                display: block;
                font-size: 20px;
                background: #6b3bbd;
                background: -moz-linear-gradient(45deg, #6b3bbd 0%, #91378a 35%, #a8346e 64%, #e02f26 100%);
                background: -webkit-gradient(left bottom, right top, color-stop(0%, #6b3bbd), color-stop(35%, #91378a), color-stop(64%, #a8346e), color-stop(100%, #e02f26));
                background: -webkit-linear-gradient(45deg, #6b3bbd 0%, #91378a 35%, #a8346e 64%, #e02f26 100%);
                background: -o-linear-gradient(45deg, #6b3bbd 0%, #91378a 35%, #a8346e 64%, #e02f26 100%);
                background: -ms-linear-gradient(45deg, #6b3bbd 0%, #91378a 35%, #a8346e 64%, #e02f26 100%);
                background: linear-gradient(45deg, #6b3bbd 0%, #91378a 35%, #a8346e 64%, #e02f26 100%);
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr="#6b3bbd", endColorstr="#e02f26", GradientType=1 );
            }
    
            .name{
                font-family: "GothamBook";
                font-size: 35px;
                line-height: 35px;
            }

            .name-2{
                font-family: "GothamBook";
                font-size: 35px;
                line-height: 30px;
            }
    
            .bold{
                font-family: "Gotham-bold";
            }
    
            .gradient-text{
                color: #c63147;
            }
    
            .quote{
                margin-top: 5px;
            }
    
            .shape{
                position: fixed;
                right: 0;
                top: 100px;
                background-image: url(https://www.creativedd.com.br/orcamento_pdf/shape-03.png);
                background-size: contain;
                background-repeat: no-repeat;
                width: 100%;
                background-position: right;
                height: 500px;
            }

            .shape-2{
                position: fixed;
                right: 0;
                top: 0;
                background-image: url(https://www.creativedd.com.br/orcamento_pdf/bg-book.png);
                background-size: contain;
                background-repeat: no-repeat;
                width: 100%;
                background-position: right;
                height: 100%;
                z-index: 1;
            }

            .shape-3{
                position: fixed;
                right: 50px;
                top: 0px;
                font-family: "GothamBold";
                border-radius: 0 0 50px 50px;
                width: 450px;
                height: 600px;
                color: #fff;
                line-height: 45px;
                text-align: center;
                display: block;
                font-size: 20px;
                background: #6b3bbd;
                background: -moz-linear-gradient(45deg, #6b3bbd 0%, #91378a 35%, #a8346e 64%, #e02f26 100%);
                background: -webkit-gradient(left bottom, right top, color-stop(0%, #6b3bbd), color-stop(35%, #91378a), color-stop(64%, #a8346e), color-stop(100%, #e02f26));
                background: -webkit-linear-gradient(45deg, #6b3bbd 0%, #91378a 35%, #a8346e 64%, #e02f26 100%);
                background: -o-linear-gradient(45deg, #6b3bbd 0%, #91378a 35%, #a8346e 64%, #e02f26 100%);
                background: -ms-linear-gradient(45deg, #6b3bbd 0%, #91378a 35%, #a8346e 64%, #e02f26 100%);
                background: linear-gradient(45deg, #6b3bbd 0%, #91378a 35%, #a8346e 64%, #e02f26 100%);
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr="#6b3bbd", endColorstr="#e02f26", GradientType=1 );
                
            }

            .m-0{
                margin: margin: 0;
            }

            .descrip{
            }

            .referencias{
                text-decoration: underline;
            }

            .descricao{
                font-size: 22px;
            }

            .description{
                font-size: 17px;
                font-family: "Gotham-book";
            }

            .ref{
                font-size: 20px;
            }

            .referencias{
                font-size: 17px;
            }

            .texts .title-proposta{
                margin-top: 50px;
                color: #fff;
                line-height: 20px;
                display: block;
            }

            .texts .date-proposta{
                color: #fff;
                font-size: 20px;
                font-family: "Gotham-book";
            }
            
            .texts .valor-proposta{
                color: #fff;
                line-height: 20px;
                display: block;
            }

            .texts .pagamento{
                color: #fff;
                font-size: 20px;
                font-family: "Gotham-book";
            }
            
            .texts .valor-desconto{
                color: #fff;
                font-size: 20px;
                font-family: "Gotham-book";
            }
        </style>';

            $html = '<!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <title>Orçamento - '.$obj->project.'</title>
            '.$css.'
        </head>
        <body style="height: 100vh;" class="body position-relative">
            <img src="https://www.creativedd.com.br/orcamento_pdf/logo.png" class="logo" />
            <div class="number bold">1</div>
            <div class="shape"></div>
            <img src="https://www.creativedd.com.br/orcamento_pdf/site.png" style="
            position: fixed;
            left: 0;
            top: 50%;
            margin-top: 115px;
            float: left;
            width: 45px;">

            <div class="container">
                <div class="centered">
                    <div class="info">
                        <h1 class="name">Olá! <span class="gradient-text">'.$obj->client.'</span>,<br/>
                        <span class="bold">Seja bem-vindo a<br> Creative Dev & Design</span>
                        <br>
                            <img src="https://www.creativedd.com.br/orcamento_pdf/quote01.png" class="quote" width="240px"/>
                        </h1>
                    </div>
                </div>
            </div>

        </body>
        </html>';

        $references = explode(',', $obj->references);

        $referencias = '';

        for($i=0; $i<count($references); $i++){
            $referencias .= '<span href="'.$references[$i].'" target="_BLANK" class="referencias m-0">'.$references[$i].'</span><br>';
        }

        $html2 = '<!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <title>Orçamento - '.$obj->project.'</title>
            '.$css.'
        </head>
        <body style="height: 100vh;" class="body position-relative">
            <img src="https://www.creativedd.com.br/orcamento_pdf/logo.png" class="logo" />
            <div class="number bold">4</div>
            <div class="shape-2" style="z-index:-1;"></div>
            <img src="https://www.creativedd.com.br/orcamento_pdf/site.png" style="
            position: fixed;
            left: 0;
            top: 50%;
            margin-top: 115px;
            float: left;
            width: 45px;">

            <div class="container">
                <div class="centered-2">
                    <div class="info">
                        <h1 class="name-2">Objetivo do <span class="gradient-text">projeto</span><br><br>
                        
                                <span class="descricao">Desenvolver o '.$obj->type.' da '.$obj->project.'. </span><br>

                                <span class="description m-0">'.$obj->description.'</span><br>

                                <span class=" bold ref m-0">Referencias:</span><br>

                                '.$referencias.'
                        
                        </h1>
                    </div>
                </div>
            </div>

        </body>
        </html>';

        $html3 = '<!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <title>Orçamento - '.$obj->project.'</title>
            '.$css.'
        </head>
        <body style="height: 100vh;" class="body position-relative">
            <img src="https://www.creativedd.com.br/orcamento_pdf/logo.png" class="logo" />
            <div class="shape-3">
                <div class="texts">
                    <h1 class="title-proposta" style="padding-top: 80px;">Investimento<br> <span class="date-proposta">válido até '.date("d/Y", strtotime($obj->shelflife)).'</span></h1>
                    <h1 class="valor-proposta" style="padding-top: 130px;font-size: 45px;">R$ '.number_format($obj->value, 2, ',', '.').'<br> <span class="pagamento">'.$obj->payment.'</span></h1>
                    <h1 class="valor-desconto" style="padding-top: 150px;">'.$obj->promotion.'</h1>
                </div>
            </div>
            <img src="https://www.creativedd.com.br/orcamento_pdf/site.png" style="
            position: fixed;
            left: 0;
            top: 50%;
            margin-top: 115px;
            float: left;
            width: 45px;">

            <div class="container">
                <div class="centered">
                    <div class="info">
                        <h1 class="name"><span class="gradient-text">Proposta</span> Comercial,<br/>
                        <span class="bold">qualquer dúvida estamos<br>a disposição</span>
                        
                        </h1>
                    </div>
                </div>
            </div>

        </body>
        </html>';


            $defaultConfig = (new \Mpdf\Config\ConfigVariables())->getDefaults();
            $fontDirs = $defaultConfig['fontDir'];

            $defaultFontConfig = (new \Mpdf\Config\FontVariables())->getDefaults();
            $fontData = $defaultFontConfig['fontdata'];

            $mpdf = new \Mpdf\Mpdf([
                'mode' => 'utf-8',
                'format' => [300, 170],
                'margin_left' => 0,
                'margin_right' => 0,
                'margin_top' => 0,
                'margin_bottom' => 0,
                'margin_header' => 0,
                'margin_footer' => 0,
                'fontDir' => array_merge($fontDirs, [
                    $_SERVER['DOCUMENT_ROOT'] . '/fontes',
                ]),
                'fontdata' => $fontData + [
                    'gotham' => [
                        'B' => 'GothamBold.ttf',
                        'R' => 'GothamBook.ttf',
                    ],
                ],
                'default_font' => 'gotham',
            ]);
            $mpdf->SetDisplayMode('fullpage');
            $mpdf->AddPage();
            $mpdf->WriteHTML($html);
            
            $mpdf->AddPage();
            $mpdf->WriteHTML('<img src="http://oseasmoreto.com/orcamento/2.png" width: 100%>');

            $mpdf->AddPage();
            $mpdf->WriteHTML('<img src="http://creativedd.com.br/orcamento_pdf/3.png" width: 100%>');

            $mpdf->AddPage();
            $mpdf->WriteHTML($html2);

            $mpdf->AddPage();
            $mpdf->WriteHTML('<img src="http://creativedd.com.br/orcamento_pdf/5.png" width: 100%>');

            $mpdf->AddPage();
            $mpdf->WriteHTML($html3);

            $mpdf->AddPage();
            $mpdf->WriteHTML('<img src="http://creativedd.com.br/orcamento_pdf/7.png" width: 100%>');

            
            if(isset($_GET['download'])){
                $mpdf->Output('orcamento-'.$this->url_generate($obj->project).'.pdf', 'D');
            }else{
                $mpdf->Output();
            }
            exit();
        } catch (\Mpdf\MpdfException $e) { // Note: safer fully qualified exception name used for catch
            // Process the exception, log, print etc.
            echo $e->getMessage();
        }

    }


}
