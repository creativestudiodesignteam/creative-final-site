<?php

namespace App\Models\Entities;

use App\Core\Models;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Arquivos
 *
 * @author oseas
 */
class SiteBanner extends Models {
    protected $table = 'site_banners';
    protected $primaryKey = 'idbanner';
    public $timestamps = false;
    protected $fillable = [ 'text1', 'text2', 'text3', 'description', 'link', 'image', 'status'];
    protected $guarded = [];

}
