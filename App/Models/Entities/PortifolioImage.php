<?php

namespace App\Models\Entities;

use App\Core\Models;
use App\Models\Entities\Portifolio;


class PortifolioImage extends Models
{
    protected $table = 'portifolio_image';
    protected $primaryKey = 'idimage';
    public $timestamps = false;
    protected $fillable = ['image', 'idportifolio'];
    protected $guarded = [];

    public function portifolio()
    {
        return $this->belongsTo(Portifolio::class, 'idportifolio');
    }
}
