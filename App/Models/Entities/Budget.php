<?php

namespace App\Models\Entities;

use App\Core\Models;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Blog
 *
 * @author oseas
 */
class Budget extends Models {
    protected $table = 'budgets';
    protected $primaryKey = 'idbudget';
    public $timestamps = false;
    protected $fillable = [ 'client', 'date_create', 'date_update', 'project', 'type', 'references', 'value', 'payment', 'promotion', 'shelflife'];
    protected $guarded = [];
}
