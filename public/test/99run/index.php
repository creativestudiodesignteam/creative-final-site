<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("includes/head.php")?>
</head>
<body>
<?php include("includes/menu-top-bar.php")?>
<?php include("includes/menu-side-bar.php")?>
<?php include("includes/menu-bottom.php")?>

<header id="header-dashboard">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <img src="assets/images/thanks-box.jpg" class="card-img" />
          <div class="card-img-overlay">
            <h5 class="card-title user text-uppercase">bom-dia! <span>Felipe</span></h5>
            <h4 class="card-text title text-uppercase">Frases do dia</h4>
            <p class="card-text description m-0">Se esperarmos estar prontos, ficaremos esperando o resto de nossas vidas.</p>
          </div>
          <a href="detalhe-desafio.php" class="btn-circle-gradient -border-btn -btn-aux"><i class="flaticon-winner"></i></a>
        </div>
      </div>
    </div>
  </div>
</header>

<section id="section-01">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <h6 class="title mt-4">Preparado para<br>encarar novos desafios?</h6>
        <div class="bar-little"></div>
      </div>

      <div class="col-12">
        <div class="slider-challenges owl-carousel owl-theme">
          <div class="item">
              <div class="box">
                <a href="inscricao.php">
                  <span class="badget-price -aux-price">A partir de<br><strong>29,90</strong></span>
                  <img src="assets/images/fenomenos-naturais.jpg" class="challenges-bg"/>
                  <div class="infos">
                    <h6 class="title">Desafio<br><span>Fenêmenos naturais</span></h6>
                    <h6 class="title">Clique e inscreva-se <i class="flaticon-right"></i></h6>
                  </div>
                </a>
              </div>
          </div>

          <div class="item">
              <div class="box">
                <span class="badget-price -aux-price">A partir de<br><strong>29,90</strong></span>
                <img src="assets/images/fenomenos-naturais.jpg" class="challenges-bg"/>
                <div class="infos">
                  <h6 class="title">Desafio<br><span>Fenêmenos naturais</span></h6>
                  <a href="inscricao.php" class="defaut-btn-outline">Clique e inscreva-se <i class="flaticon-right"></i></a>
                </div>
              </div>
          </div>

          <div class="item">
              <div class="box">
                <span class="badget-price -aux-price">A partir de<br><strong>29,90</strong></span>
                <img src="assets/images/fenomenos-naturais.jpg" class="challenges-bg"/>
                <div class="infos">
                  <h6 class="title">Desafio<br><span>Fenêmenos naturais</span></h6>
                  <a href="inscricao.php" class="defaut-btn-outline">Clique e inscreva-se <i class="flaticon-right"></i></a>
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="section-02">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <h6 class="title mt-4">Produtos em destaque</h6>
        <div class="bar-little"></div>
      </div>

      <div class="col-12">
        <div class="slider-product owl-carousel owl-theme">
          <div class="item">
              <figure class="card card-product">
                <div class="img-wrap">
                  <img src="assets/images/arqueiro-site.png" class="img-fluid" width="100px" />
                </div>
                <figcaption class="info-wrap">
                    <h5 class="title m-0">Medalha 6º Guerreiro</h5>
                </figcaption>
                <div class="bottom-wrap">
                  <a href="carrinho.php" class="float-right btn-circle-cart"><i class="flaticon-shopping-cart"></i></a>	
                  <div class="price-wrap h5">
                    <span class="price-new">R$36,00</span> <del class="price-old">$26,00</del>
                  </div> 
                </div>
              </figure>
          </div>

          <div class="item">
              <figure class="card card-product">
                <div class="img-wrap">
                  <img src="assets/images/arqueiro-site.png" class="img-fluid" width="100px" />
                </div>
                <figcaption class="info-wrap">
                    <h5 class="title m-0">Medalha 6º Guerreiro</h5>
                </figcaption>
                <div class="bottom-wrap">
                  <a href="carrinho.php" class="float-right btn-circle-cart"><i class="flaticon-shopping-cart"></i></a>	
                  <div class="price-wrap h5">
                    <span class="price-new">R$36,00</span> <del class="price-old">$26,00</del>
                  </div> 
                </div>
              </figure>
          </div>

          <div class="item">
              <figure class="card card-product">
                <div class="img-wrap">
                  <img src="assets/images/arqueiro-site.png" class="img-fluid" width="100px" />
                </div>
                <figcaption class="info-wrap">
                    <h5 class="title m-0">Medalha 6º Guerreiro</h5>
                </figcaption>
                <div class="bottom-wrap">
                  <a href="carrinho.php" class="float-right btn-circle-cart"><i class="flaticon-shopping-cart"></i></a>	
                  <div class="price-wrap h5">
                    <span class="price-new">R$36,00</span> <del class="price-old">$26,00</del>
                  </div> 
                </div>
              </figure>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php include("includes/script.php")?>
</body>
</html>