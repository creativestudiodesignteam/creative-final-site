<div class="d-flex justify-content-between menu-top">
    <div class="p-2 align-self-center">
        <a id="show-sidebar" class="btn" href="#">
            <span class="bars"></span>
            <span class="bars"></span>
            <span class="bars"></span>
        </a>
    </div>
    <div class="p-2 align-self-center">
        <div class="dropdown">
            <button class="dropdown-toggle drop-menu" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Dashboard
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                <button class="dropdown-item" type="button">Action</button>
                <button class="dropdown-item" type="button">Another action</button>
                <button class="dropdown-item" type="button">Something else here</button>
            </div>
        </div>
    </div>
    <div class="p-2 align-self-center">
        <ul class="list-unstyled list-inline m-0 list-icons-menu">
            <li class="list-inline-item"><a href="#."><i class="flaticon-magnifying-glass"></i></a></li>
            <li class="list-inline-item"><a href="carrinho.php"><i class="flaticon-shopping-cart"></i><span class="badge pulsate">2</span></a></li>
        </ul>
    </div>
</div>