<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("includes/head.php")?>
</head>
<body>
<?php include("includes/menu-top-bar.php")?>
<?php include("includes/menu-side-bar.php")?>
<?php include("includes/menu-bottom.php")?>

<header id="header-outros-apps">
  <div class="container-fluid">
    
    <div class="row">
        <div class="col-12 text-center">
            <div class="box-info mb-4">
                <i class="flaticon-smartphone"></i>
                <h1 class="title">Celular</h1>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 text-center">
            <p>Passo 1:<br><span>Corra utilizando qualquer  aplicativo de corrida, exemplos: Nike Running, Runkeeper...</span></p>
            <p>Passo 2:<br><span>Tire um print do seu celular comprovando que correu a distância  da prova no tempo estipulado</span></p>
        </div>
    </div>

    <div class="row mt-3">
        <div class="col-12 text-center">
            <div class="box-video">
                <iframe width="100%" height="215" src="https://www.youtube.com/embed/7H0pKlziMto"
                    frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen></iframe>
                    <a href="#" class="defaut-btn-gradient">Validar</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 text-center">
            <div class="box-info-2">
                <div class="bar-big"></div>
                <h1 class="title">Saiba seus próximos passos<br><span>o que você utiliza para correr</h1>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-4 text-center">
            <a href="celuar.php">
                <div class="box-pass" data-mh="my-group">
                    <i class="flaticon-smartphone"></i>
                </div>
                <p class="title-pass">Celuar</p>
            </a>
        </div>

        <div class="col-4 text-center">
            <a href="gps.php">
                <div class="box-pass" data-mh="my-group">
                    <i class="flaticon-smartwatch"></i>
                </div>
                <p class="title-pass">Relogio GPS</p>
            </a>
        </div>

        <div class="col-4 text-center">
            <a href="esteira.php">
                <div class="box-pass" data-mh="my-group">
                    <i class="flaticon-treadmill"></i>
                </div>
                <p class="title-pass">Esteira</p>
            </a>
        </div>
    </div>
  </div>
</header>


<?php include("includes/script.php")?>
</body>
</html>