<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("includes/head.php")?>
</head>
<body>

<?php include("includes/menu-side-bar.php")?>
<?php include("includes/menu-bottom.php")?>

<header id="header-adicionar-contato">
  <div class="d-flex justify-content-between menu-top">
      <div class="p-2 align-self-center">
          <a class="btn-back" href="#"><i class="material-icons">arrow_back</i> Voltar</a>
      </div>
      <div class="p-2 align-self-center text-center">
          <a href="#" class="secretaria">Sofia<br><span class="secretaria">Secretária</span></a>
      </div>
      <div class="p-2 align-self-center">
          <ul class="list-unstyled list-inline m-0 list-icons-menu">
              <li class="list-inline-item"><a href="#" class="edit-icon"><img src="assets/images/avatar-secretaria.png"></a></li>
          </ul>
      </div>
  </div>

  <div class="container-fluid">
        <div class="row">
          <div class="col-12 text-center position-relative">
            <h4 class="title-header mt-2 mb-3">Boa tarde <span class="bg-text-gradient">Sr. Felipe</span> Quem você gostaria de lembrar?</h4>

            <div class="mt-2 mb-4">
                <a href="#" class="defaut-btn-gradient2 border-0 btn-open-contact">Abrir contatos</a>
            </div>
          </div>
        </div>
      </div>
</header>

<section id="section-detalhe-card-01">
  <div class="container-fluid">
    <div class="row mt-4">
      <div class="col-12 text-center align-self-center">
        <h4 class="title mt-0 mb-3">ou</h4>
      </div>
    </div>

    <div class="row">
      <div class="col-12 text-center">
      <form>
        <div class="form-row">
            <div class="form-group col-md-6">
            <input type="text" class="form-control" id="inputEmail4" placeholder="Digite seu nome">
            </div>
            <div class="form-group col-md-6">
            <input type="phone" class="form-control" id="inputPassword4" placeholder="Digite seu telefone">
            </div>
        </div>

        <button type="submit" class="defaut-btn-gradient2 border-0 ">Continuar</button>
        </form>
      </div>
    </div>
  </div>
</section>

<?php include("includes/script.php")?>
</body>
</html>