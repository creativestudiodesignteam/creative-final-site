<?php

namespace App\Controllers\Site;

use \App\Controllers\Site\Common;
use App\Models\Entities\Video;

/**
 * Description of Home
 *
 * @author oseas¹
 */
class Videos extends Common{

    protected $pagelink = '';

    public function index($page = 1) {
        $this->start_session();
        $this->view->page = 'header-video';
        $this->view->titlehead = 'Videos IOPEM';
        $this->image = '/templates/site/default/assets/images/blog/bg/iopem2.jpg';
        $this->title_post = $this->view->titlehead;
        $this->removetitle = true;
        //paginacao
        $total = Video::where('status', '=', 'a')
                     ->where('posting_date', '<=', date('Y-m-d'))
                     ->orderBy('posting_date', 'DESC')->count();

        //seta a quantidade de itens por página
        $registros =  6;

        //calcula o número de páginas arredondando o resultado para cima

        $numPaginas = ceil($total/$registros);

        //variavel para calcular o início da visualização com base na página atual
        $inicio = ($registros*$page) - $registros;

        $this->view->blogs = Video::where('status', '=', 'a')
                                 ->where('posting_date', '<=', date('Y-m-d'))
                                 ->orderBy('posting_date', 'DESC')->limit($registros)->offset($inicio)->get();

        $this->view->numPaginas = $numPaginas;
        $this->view->paginacao = $page;
        $this->view->registros = $registros;
        $this->view->max_links = 10;
        $this->view->total = $total;

        $this->render('index', $this->folder, $this->page);
    }
}
