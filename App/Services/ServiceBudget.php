<?php

/**
 * Created by PhpStorm.
 * User: oseas
 * Date: 08/12/2018
 * Time: 00:38
 */

namespace App\Services;

use \App\Models\Entities\Budget;


class ServiceBudget
{

    public function create($request)
    {
        if ($request) {
            $obj = new Budget();
            $this->save($request, $obj);

            $return = [];
            if ($obj->idbudget <> '') {
                $return['success'] = true;
                $return['data'] = $obj;
            } else {
                $return['success'] = false;
                $return['message'] = "Não foi possivel inserir o orçamento <strong>{$obj->project}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function update($request)
    {
        if ($request) {
            $obj = Budget::find($request['id']);
            $this->save($request, $obj);

            $return = [];
            if ($obj->idbudget <> '') {

                $return['success'] = true;
                $return['data'] = $obj;
            } else {
                $return['success'] = false;
                $return['message'] = "Não foi possivel atualizar o orçamento <strong>{$obj->project}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function destroy($id)
    {
        $obj = Budget::find($id);
        $resp = $obj->delete();

        $return = [];

        if ($resp) {
            $return['success'] = true;
        } else {
            $return['success'] = false;
        }

        return $return;
    }

    public function save($request, $obj)
    { 
        $obj->project     = $request['project'];
        $obj->client     = $request['client'];
        $obj->description     = $request['description'];
        $obj->type    = $request['type'];
        $obj->references   = $request['references'];
        $obj->value     = $request['value'];
        $obj->payment     = $request['payment'];
        $obj->promotion = $request['promotion'];
        $obj->shelflife     = $request['shelflife'];
        $obj->date_create    = $request['date_create'];
        $obj->date_update      = $request['date_update'];

        $obj->save();
    }
}
