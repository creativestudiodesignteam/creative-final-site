<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("includes/head.php")?>
</head>
<body>
<?php include("includes/menu-top-bar.php")?>
<?php include("includes/menu-side-bar.php")?>
<?php include("includes/menu-bottom.php")?>

<header id="header-contato">
  <div class="container-fluid">
    <div class="row">
        <div class="col-12 text-center">
            <div class="box-info">
                <div class="bar-big"></div>
                <h1 class="title"><span>olá felipe</span><br>Precisando de ajuda? entre em contato</h1>
            </div>
        </div>
    </div>

    <form class="form-address mt-4">
        <div class="form-group row">
            <label for="staticEmail" class="col-2 col-form-label">Nome</label>
            <div class="col-10">
                <input type="text" readonly class="form-control-plaintext" id="staticEmail">
            </div>
        </div>

        <div class="form-group row">
            <label for="staticEmail" class="col-4 col-form-label">e-mail</label>
            <div class="col-8">
                <input type="text" class="form-control-plaintext" id="staticEmail">
            </div>
        </div>

        <div class="form-group row">
            <label for="staticEmail" class="col-3 col-form-label">Telefone</label>
            <div class="col-9">
                <input type="text" class="form-control-plaintext" id="staticEmail">
            </div>
        </div>

        <div class="form-group row">
            <label for="staticEmail" class="col-3 col-form-label">Assunto</label>
            <div class="col-9">
                <input type="text" class="form-control-plaintext" id="staticEmail">
            </div>
        </div>
        
        <div class="form-group row">
            <label for="staticEmail" class="col-3 col-form-label">Mensagem</label>
            <div class="col-9">
                <textarea class="form-control-plaintext" id="staticEmail" rows="5"></textarea>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-12 text-right">
                <input type="submit" class="border-0 defaut-btn-gradient" value="Enviar contato">
            </div>
        </div>
    </form>
  </div>
</header>


<?php include("includes/script.php")?>
</body>
</html>