<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("includes/head.php")?>
</head>
<body>
<?php include("includes/menu-top-bar.php")?>
<?php include("includes/menu-side-bar.php")?>
<?php include("includes/menu-bottom.php")?>

<header id="header-gps">
  <div class="container-fluid">
    <div class="row">
        <div class="col-12 text-center">
            <div class="box-info">
                <div class="bar-big"></div>
                <h1 class="title"><span>Olá Felipe,</span><br>Este é seu dessafio de hoje! está preparado?</h1>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 text-center">
            <div class="box-info-2">
                <h1 class="title"><span>meta</span><br>do desafio</h1>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-6 text-center align-self-center">
            <div class="box-challenges">
                <i class="flaticon-stopwatch timer"></i>
                <p id="getting-started"></p>
                <span>Abaixo de</span>
            </div>
        </div>

        <div class="col-6 text-center align-self-center">
            <div class="box-challenges">
                <i class="flaticon-speed"></i>
                <p>12.0 km</p>
                <span>Correr</span>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-6 text-center align-self-center">
            <div class="box-challenges-info">
                <span>Tempo de prova</span>
                <p>00:18:32</p>
            </div>
        </div>

        <div class="col-6 text-center align-self-center">
            <div class="box-challenges-info">
                <span>Distância</span>
                <p>0.10km</p>
            </div>
        </div>

        <div class="col-12 mt-4">
            <div class="progress" style="height: 15px;">
                <div class="progress-bar" role="progressbar" style="width: 77%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <span class="label">1% do desafio</span>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-12 text-center">
            <a href="#" class="defaut-btn-gradient"><i class="flaticon-pause"></i> Pausar</a>
            <a href="#" class="defaut-btn-outline">Desistir</a>
        </div>
    </div>
  </div>
</header>


<?php include("includes/script.php")?>
</body>
</html>