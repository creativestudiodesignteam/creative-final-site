<?php
/**
 * Created by PhpStorm.
 * User: oseas
 * Date: 08/12/2018
 * Time: 00:38
 */

namespace App\Services;

use \App\Models\Entities\Configuration;


class ServiceConfiguration{

    public function create($request) {
        if($request){
            $obj = new Configuration();
            $this->save($request, $obj);

            $return = [];
            if($obj->idconfig <> ''){
                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel inserir o <strong>Registro</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function update($request) {
        if($request){
            $obj = Configuration::find($request['id']);
            $this->save($request, $obj);

            $return = [];
            if($obj->idconfig <> ''){

                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel atualizar o <strong>Registro</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function destroy($id){
        $obj = Configuration::find($id);
        $obj->status = 'd';
        $resp = $obj->save();

        $return = [];

        if($resp){
            $return['success'] = true;
        }else{
            $return['success'] = false;
        }

        return $return;
    }

    public function save($request, $obj){
        $obj->smtp = $request['smtp'];
        $obj->emailuser = $request['emailuser'];
        $obj->emailpassword = $request['emailpassword'];
        $obj->port = $request['port'];
        $obj->emailsend = $request['emailsend'];
        $obj->title = $request['title'];
        $obj->students = $request['students'];
        $obj->copyright = $request['copyright'];
        $obj->logo = $request['logo'];
        $obj->bd_host = $request['bd_host'];
        $obj->bd_user = $request['bd_user'];
        $obj->bd_password = $request['bd_password'];
        $obj->bd_name = $request['bd_name'];

        $obj->save();

    }

}
