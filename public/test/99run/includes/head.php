<meta charset="utf-8" />
<meta http-equiv="x-ua-compatible" content="ie=edge" />
<meta content="" name="description" />
<meta content="" name="keywords" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<title>99RUN</title>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">
<link rel="stylesheet" href="assets/vendor/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/vendor/owl-carousel/owl.carousel.min.css">
<link rel="stylesheet" href="assets/css/main.css" />
<link rel="stylesheet" href="assets/fontes/flaticon.css" />
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">