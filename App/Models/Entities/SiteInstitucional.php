<?php

namespace App\Models\Entities;

use App\Core\Models;
use App\Models\Entities\Motel;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Arquivos
 *
 * @author oseas
 */
class SiteInstitucional extends Models {
    protected $table = 'site_institucional';
    protected $primaryKey = 'idinstitucional';
    public $timestamps = false;
    protected $fillable = ['titulo', 'imagem', 'texto', 'resumo', 'ordem', 'url', 'ativo'];
    protected $guarded = [];

    public function motel(){
        return $this->belongsTo(Motel::class, 'id_motel');
    }
}
