<div class="page-wrapper chiller-theme">
  <nav id="sidebar" class="sidebar-wrapper">
    <div class="sidebar-content">
      <div class="sidebar-brand">
        <a href="#"><i class="flaticon-power"></i> Sair</a>
        <div id="close-sidebar">
          <i class="flaticon-right"></i>
        </div>
      </div>

      <div class="sidebar-menu mt-5">
        <ul>
          <li>
            <a href="#." data-toggle="modal" data-target="#modal-mensagem">
              <!-- <i class="flaticon-shopping-cart"></i> -->
              <span>popup mensagem</span>
            </a>
          </li>
          <li>
            <a href="#." data-toggle="modal" data-target="#modal-lembrete">
              <!-- <i class="flaticon-shopping-cart"></i> -->
              <span>popup lembrete</span>
            </a>
          </li>
          <li>
            <a href="#." data-toggle="modal" data-target="#modal-lembrete-descricao">
              <!-- <i class="flaticon-shopping-cart"></i> -->
              <span>popup lembrete descrição</span>
            </a>
          </li>
          <li>
            <a href="#." data-toggle="modal" data-target="#modal-final-1">
              <!-- <i class="flaticon-shopping-cart"></i> -->
              <span>popup final</span>
            </a>
          </li>
          <li>
          <a href="#." data-toggle="modal" data-target="#modal-final-2">
              <!-- <i class="flaticon-shopping-cart"></i> -->
              <span>popup final 2</span>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <!-- <main class="page-content">
    
  </main> -->
</div>