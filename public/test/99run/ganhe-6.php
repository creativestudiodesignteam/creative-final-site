<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("includes/head.php")?>
</head>
<body>
<?php include("includes/menu-top-bar.php")?>
<?php include("includes/menu-side-bar.php")?>
<?php include("includes/menu-bottom.php")?>

<header id="header-ganhe-6">
  <div class="container-fluid">
    <div class="card mb-3">
      <div class="row">
          <div class="col-4 offset-1 align-self-center text-center">
            <i class="flaticon-piggy-bank"></i>
          </div>
          <div class="col-7 pl-0 align-self-center">
            <div class="card-block">
              <h5 class="card-title text-uppercase">meu saldo<br><span>R$6,00</span></h5>
            </div>
          </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 text-center">
            <div class="box-info">
                <div class="bar-big"></div>
                <h1 class="title"><span>Olá Felipe,</span><br>de 6,00 e ganhe 6,00</h1>
            </div>
        </div>
    </div>

    <div class="row mt-3">
        <div class="col-4 text-center -bg">
            <div class="box">
                <i class="flaticon-smartphone"></i>
                <p class="m-0">Envie seu código</p>
            </div>
        </div>
        
        <div class="col-4 text-center -bg">
            <div class="box">
                <i class="flaticon-medal"></i>
                <p class="m-0">Seu amigo pede a 1º medalha</p>
            </div>
        </div>
        
        <div class="col-4 text-center -bg">
            <div class="box">
                <i class="flaticon-friendship"></i>
                <p class="m-0">Ele ganha 6R$ e você ganha 6R$</p>
            </div>
        </div>
    </div>

    <div class="row mt-3">
        <div class="col-12 text-center">
            <div class="box-video">
                <iframe width="100%" height="215" src="https://www.youtube.com/embed/7H0pKlziMto"
                    frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen></iframe>
                    <a href="#" class="defaut-btn-gradient"><i class="fab fa-whatsapp"></i> Compartilhar</a>
            </div>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-12 text-center">
            <div class="box-info">
                <div class="bar-big"></div>
                <h1 class="title"><span>Veja o</span><br>seu cupom aqui</h1>
                <h1 class="ticket">Dani15991</h1>
            </div>
        </div>
    </div>
  </div>
</header>


<?php include("includes/script.php")?>
</body>
</html>