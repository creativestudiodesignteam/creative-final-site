<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("includes/head.php")?>
</head>
<body>
<?php include("includes/menu-top-bar.php")?>
<?php include("includes/menu-side-bar.php")?>
<?php include("includes/menu-bottom.php")?>

<header id="header-detalhe-desafio">
  <div class="container-fluid">
    <div class="card mb-3">
      <div class="row">
          <div class="col-4 offset-1 align-self-center text-center">
            <i class="flaticon-running"></i>
          </div>
          <div class="col-7 pl-0 align-self-center">
            <div class="card-block">
              <h5 class="card-title">Distância<br><span>16.87</span><span class="km"> km</span></h5>
              <span class="old-run">12.74 km (22%) <i class="fas fa-long-arrow-alt-up"></i></span>
            </div>
          </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 text-center">
            <div class="box-info">
                <h1 class="title">18º<br><span>Ranking Geral</span></h1>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-4 text-center">
            <div class="box-rank" data-mh="my-group">
                <i class="flaticon-female"></i>
                <h1 class="title">54º</h1>
                <span>Feminino</span>
            </div>
        </div>

        <div class="col-4 text-center">
            <div class="box-rank" data-mh="my-group">
                <i class="flaticon-place"></i>
                <h1 class="title">18º</h1>
                <span>SP</span>
            </div>
        </div>

        <div class="col-4 text-center">
            <div class="box-rank" data-mh="my-group">
                <i class="flaticon-cake"></i>
                <h1 class="title">23º</h1>
                <span>21 ~ 32</span>
            </div>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-4 text-center align-self-center">
            <div class="box-challenges">
                <i class="flaticon-speed"></i>
                <p>9.28</p>
                <span>Média</span>
            </div>
        </div>

        <div class="col-4 text-center align-self-center">
            <div class="box-challenges">
                <i class="flaticon-stopwatch timer"></i>
                <p id="getting-started"></p>
                <span>tempo</span>
            </div>
        </div>

        <div class="col-4 text-center align-self-center">
            <div class="box-challenges">
                <i class="flaticon-calendar"></i>
                <p>05/mar</p>
                <span>data</span>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 p-0 mt-4">
            <div class="ct-chart ct-perfect-fourth"></div>
        </div>
    </div>

    <div class="row">
      <div class="col-12">
        <div class="box-men">
            <img src="assets/images/men.png" class="img-fluid men"/>
            <img src="assets/images/99-run.png" class="img-fluid run"/>
            <img src="assets/images/shapes.png" class="img-fluid shapes"/>

            <a href="desafiar-amigo.php" class="defaut-btn-gradient">Desafiar um amigo <i class="flaticon-right"></i></a>
        </div>
      </div>
    </div>

    <div class="row">
        <div class="col-12 text-center">
            <div class="box-info2">
                <div class="bar-big"></div>
                <h1 class="title"><span>parabéns Felipe,</span><br>Veja aqui sua medalha</h1>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 text-center">
            <div class="box-medal">
                <img src="assets/images/medal.png" width="200" alt="img-fluid" />
                <div class="circle"></div>
            </div>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-7">
            <div class="box-info3 mb-4">
                <h1 class="title">Veja aqui seus<br>desafios concluídos</h1>
                <div class="bar-little"></div>
            </div>
        </div>

        <div class="col-5">
          <a href="desafios-concluidos.php" class="defaut-btn-gradient">Ver todos <i class="flaticon-right"></i></a>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <a href="detalhe-desafio.php" class="box-line">
                <div class="row">
                    <div class="col-2 align-self-center">
                        <i class="flaticon-quality"></i>
                    </div>

                    <div class="col-6 align-self-center">
                        <p class="challenge-name"> Desafio<br><span>Fenômenos naturais</span></p>
                    </div>

                    <div class="col-4 align-self-center">
                        <p class="km">10km</p>
                    </div>
                </div>
            </a>

            <a href="detalhe-desafio.php" class="box-line">
                <div class="row">
                    <div class="col-2 align-self-center">
                        <i class="flaticon-quality"></i>
                    </div>

                    <div class="col-6 align-self-center">
                        <p class="challenge-name"> Desafio<br><span>Fenômenos naturais</span></p>
                    </div>

                    <div class="col-4 align-self-center">
                        <p class="km">10km</p>
                    </div>
                </div>
            </a>

            <a href="detalhe-desafio.php" class="box-line">
                <div class="row">
                    <div class="col-2 align-self-center">
                        <i class="flaticon-quality"></i>
                    </div>

                    <div class="col-6 align-self-center">
                        <p class="challenge-name"> Desafio<br><span>Fenômenos naturais</span></p>
                    </div>

                    <div class="col-4 align-self-center">
                        <p class="km">10km</p>
                    </div>
                </div>
            </a>

            <a href="detalhe-desafio.php" class="box-line">
                <div class="row">
                    <div class="col-2 align-self-center">
                        <i class="flaticon-quality"></i>
                    </div>

                    <div class="col-6 align-self-center">
                        <p class="challenge-name"> Desafio<br><span>Fenômenos naturais</span></p>
                    </div>

                    <div class="col-4 align-self-center">
                        <p class="km">10km</p>
                    </div>
                </div>
            </a>
        </div>
    </div>
  </div>
</header>


<?php include("includes/script.php")?>
<script>
var chart = new Chartist.Line('.ct-chart', {
  labels: ['seg', 'ter', 'qua', 'qui', 'sex', 'sab', 'dom'],
  // Naming the series with the series object array notation
  series: [{
    name: 'series-1',
    data: [1, 3, 2, 5, 4, 5, 2,]
  }, {
    name: 'series-2',
    data: [1, 1, 3, 4, 3, 2, 4]
  }, {
    name: 'series-3',
    data: [1, 2, 3, 2, 4, 3, 2]
  }]
}, {
  fullWidth: true,
  axisX: {
    showGrid: false,
    showLabel: true,
    offset: 100,
  },
  axisY: {
    showGrid: false,
    showLabel: false,
    offset: 0,
  },
  chartPadding: 20,
  low: 0,
  series: {
    'series-1': {
      lineSmooth: Chartist.Interpolation.simple(),
      showPoint: true,
      showArea: true
    },
    'series-2': {
      lineSmooth: Chartist.Interpolation.simple(),
      showPoint: false
      
    },
    'series-3': {
      lineSmooth: Chartist.Interpolation.simple(),
      showPoint: false,
      
    }
  }
}, [
  // You can even use responsive configuration overrides to
  // customize your series configuration even further!
 /*  ['screen and (max-width: 320px)', {
    series: {
      'series-1': {
        lineSmooth: Chartist.Interpolation.none()
      },
      'series-2': {
        lineSmooth: Chartist.Interpolation.none(),
        showArea: false
      },
      'series-3': {
        lineSmooth: Chartist.Interpolation.none(),
        showPoint: true
      }
    }
  }] */
]);
</script>
</body>
</html>