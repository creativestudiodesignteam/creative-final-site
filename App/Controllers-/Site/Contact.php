<?php

namespace App\Controllers\Site;

use App\Classes\Mail;
use \App\Controllers\Site\Common;
use App\Models\Entities\Blog;
use App\Models\Entities\Contacts;
use App\Models\Entities\PortifolioType;
/**
 * Description of Home
 *
 * @author oseas¹
 */
class Contact extends Common{

    protected $pagelink = '';

    public function index() {
        $this->start_session();
        $this->view->page = 'header-home';
        $this->view->titlehead = 'Contato';
        $this->render('index', $this->folder, $this->page);
    }

    public function post(){
        if($_POST){

            $contact = new Contacts();
            $contact->name = $_POST['name'];
            $contact->email = $_POST['email'];
            $contact->phone = isset($_POST['phone']) ? $_POST['phone'] : '';
            $contact->subject = isset($_POST['subject']) ? $_POST['subject'] : '';
            $contact->text = $_POST['message'];
            $contact->date_create = date('Y-m-d H:i:s');
            $contact->view = 'n';

            $email[] = array('name' => 'Oseas', 'email' => 'oseas@creativedd.com.br');
            $email[] = array('name' => 'Gil', 'email' => 'gil@creativedd.com.br');
            $email[] = array('name' => 'Felipe', 'email' => 'felipe@creativedd.com.br');
            $email[] = array('name' => utf8_decode('Creative'), 'email' => $this->datasite['config']->emailsend);

            $contact->save();

            $mail = new Mail(
                $this->datasite['config']->emailuser, 
                $this->datasite['config']->emailpassword, 
                $this->datasite['config']->smtp, 
                $this->datasite['config']->port, 
                '#1d1d1d',
                utf8_decode($this->datasite['config']->title),
                'https://www.creativedd.com.br/'.$this->datasite['config']->logo
            );
            $mail->fromname = utf8_decode('Creative');
            $mail->recipient = $email;
            $mail->subject = addslashes(utf8_decode('Creative') . ' | Novo contato do site!');
            $mail->body = utf8_decode('
                <tr>
                <td style="word-wrap:break-word; font-size:0px; padding:0px; border-collapse:collapse" align="center">
                <div style="color: rgb(85, 85, 85); font-family: sans-serif, serif, EmojiFont; font-size: 16px; line-height: 1.5; text-align: center;">
                <b>Dados do Contato:</b><br><br>
                <b>Data: '.$contact->date_create.'</b><br>
                <b>Nome: '.$contact->name.'</b><br>
                <b>E-mail: '.$contact->email.'</b><br>
                <b>Telefone: '.$contact->phone.'</b><br>
                <b>Assunto: '.$contact->subject.'</b><br>
                <b>Mensagem: '.$contact->text.'</b><br>
                </div>
                </td>
                </tr>
                <tr>
                <td style="word-wrap:break-word; font-size:0px; padding:0px; border-collapse:collapse;" align="center">
                <div style="color: rgb(85, 85, 85); font-family: sans-serif, serif, EmojiFont; font-size: 16px; line-height: 1.5; text-align: center; margin-top: 10px">
                </div>
                </td>
                </tr>
            ');

            $mail->altbody = utf8_decode($this->datasite['config']->title.' | Novo contato do site');

            $send = $mail->send();

            if($send){
                $return['response']['mensagem'] = 'Mensagem enviada com sucesso!';
                $return['response']['classe'] = 'alert-success';
                $return['response']['result'] = 'success';
                $return['response']['redirect'] = '/contato';
            }else{
                $return['response']['mensagem'] = 'Não foi possivel enviar a mensagem!';
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';
            }

            print_r(json_encode($return));
            exit();
        }
    }
}
