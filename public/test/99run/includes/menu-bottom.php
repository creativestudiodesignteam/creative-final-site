<div class="menu-bottom text-center">
  <div class="d-flex justify-content-around">
    <div class="p-2">
        <a href="desafios.php" class="link -one">
          <i class="flaticon-winner"></i>
          <span>Desafios</span>
        </a>
    </div>

    <div class="p-2">
        <a href="detalhe-desafio.php" class="link -two">
          <i class="flaticon-trophy"></i>
          <span>Rankings</span>
        </a>
    </div>

    <div class="p-2">
        <a href="index.php" class="btn-circle-gradient btn-home">
          <i class="flaticon-home"></i>
        </a>
    </div>

    <div class="p-2">
        <a href="loja.php" class="link -three"><i class="flaticon-shopping-cart "></i>
          <span>Lojas</span>
        </a>
    </div>

    <div class="p-2">
        <a href="contato.php" class="link -four"><i class="flaticon-headphones"></i>
          <span>Suporte</span>
        </a>
    </div>
  </div>
</div>