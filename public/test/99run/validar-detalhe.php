<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("includes/head.php")?>
</head>
<body>
<?php include("includes/menu-top-bar.php")?>
<?php include("includes/menu-side-bar.php")?>
<?php include("includes/menu-bottom.php")?>

<header id="header-validar-detalhe">
  <div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="box-info mb-4">
                <h1 class="title">Escolha a prova<br>para efetuar a validação</h1>
                <div class="bar-little"></div>
            </div>
        </div>
    </div>
    <div class="row" id="medal">
        <div class="col-6 text-center">
            <a href="#." class="medal-selection active">
                <img src="assets/images/medalha01.png" class="img-fluid"/>
                <p>Água 10km<br>01h:30min</p>
            </a>
        </div>

        <div class="col-6 text-center">
            <a href="#." class="medal-selection">
                <img src="assets/images/medalha02.png" class="img-fluid"/>
                <p>Ar 10km<br>01h:30min</p>
            </a>
        </div>

        <div class="col-6 text-center">
            <a href="#." class="medal-selection">
                <img src="assets/images/medalha03.png" class="img-fluid"/>
                <p>Fogo 10km<br>01h:30min</p>
            </a>
        </div>

        <div class="col-6 text-center">
            <a href="#." class="medal-selection">
                <img src="assets/images/medalha04.png" class="img-fluid"/>
                <p>Terra 10km<br>01h:30min</p>
            </a>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-12 text-center">
            <a href="validar-desafio.php" class="defaut-btn-gradient">Validar desafio <i class="flaticon-right"></i></a>
        </div>
    </div>
  </div>
</header>


<?php include("includes/script.php")?>
</body>
</html>