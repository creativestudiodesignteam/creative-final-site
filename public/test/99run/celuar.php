<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("includes/head.php")?>
</head>
<body>
<?php include("includes/menu-top-bar.php")?>
<?php include("includes/menu-side-bar.php")?>
<?php include("includes/menu-bottom.php")?>

<header id="header-celular">
  <div class="container-fluid">
    
    <div class="row">
        <div class="col-12 text-center">
            <div class="box-info mb-4">
                <i class="flaticon-smartphone"></i>
                <h1 class="title">Celular</h1>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 text-center">
            <p>Qual aplicativo você utiliza para correr?</p>
        </div>
    </div>

    <div class="row">
        <div class="col-12 text-center">
            <a href="strava.php" class="defaut-btn-outline"><img src="assets/images/strava.png" class="img-fluid"/></a>
            <a href="outros-apps.php" class="defaut-btn-gradient">Outros apps</a>
        </div>
    </div>

    <div class="row">
        <div class="col-12 text-center">
            <div class="box-info-2">
                <div class="bar-big"></div>
                <h1 class="title">Saiba seus próximos passos<br><span>o que você utiliza para correr</h1>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-4 text-center">
            <a href="celuar.php">
                <div class="box-pass" data-mh="my-group">
                    <i class="flaticon-smartphone"></i>
                </div>
                <p class="title-pass">Celuar</p>
            </a>
        </div>

        <div class="col-4 text-center">
            <a href="gps.php">
                <div class="box-pass" data-mh="my-group">
                    <i class="flaticon-smartwatch"></i>
                </div>
                <p class="title-pass">Relogio GPS</p>
            </a>
        </div>

        <div class="col-4 text-center">
            <a href="esteira.php">
                <div class="box-pass" data-mh="my-group">
                    <i class="flaticon-treadmill"></i>
                </div>
                <p class="title-pass">Esteira</p>
            </a>
        </div>
    </div>
  </div>
</header>


<?php include("includes/script.php")?>
</body>
</html>