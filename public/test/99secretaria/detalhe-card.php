<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("includes/head.php")?>
</head>
<body>

<?php include("includes/menu-side-bar.php")?>
<?php include("includes/menu-bottom.php")?>

<header id="header-detalhe-card">
  <div class="d-flex justify-content-between menu-top">
      <div class="p-2 align-self-center">
          <a class="btn-back" href="#"><i class="material-icons">arrow_back</i> Voltar</a>
      </div>
      <div class="p-2 align-self-center">
          <a href="#" class="peoples">Oséas Moreto</a>
      </div>
      <div class="p-2 align-self-center">
          <ul class="list-unstyled list-inline m-0 list-icons-menu">
              <li class="list-inline-item"><a href="#" class="edit-icon"><i class="material-icons">edit</i></a></li>
          </ul>
      </div>
  </div>

  <div class="container-fluid">
        <div class="row">
          <div class="col-12 text-center">
            <h4 class="title-header mt-2 mb-3">Não quer falar com <span class="bg-text-gradient">Oséas</span> agora? Adie essa mensagem</h4>

            <div class="mt-2 mb-4">
                <a href="#" class="defaut-btn-gradient2 border-0">Adiar conversa</a>
                <a href="index.php" class="defaut-btn-outline2">Voltar</a>
            </div>
          </div>
        </div>
      </div>
</header>

<section id="section-detalhe-card-01">
  <div class="container-fluid">
    <div class="row mt-4">
      <div class="col-10 align-self-center">
        <h4 class="title mt-0 mb-3">Status da conversa:</h4>
      </div>
    </div>

    <div class="row">
      <div class="col-12">
          <div class="card mb-2">
            <div class="card-body pb-4 position-relative">
             <div class="overlay-content">
                 <p class="card-subtitle mb-1 mt-1"><strong>15/05/2019 - 15:33</strong> Oi André?</p>
                 <p class="card-subtitle mb-1 -active"><strong>15/05/2019 - 15:33</strong> Oi André?</p>
                 <p class="card-subtitle mb-1"><strong>15/05/2019 - 15:33</strong> Oi André?</p>
                 <p class="card-subtitle mb-1"><strong>15/05/2019 - 15:33</strong> Oi André?</p>
                 <p class="card-subtitle mb-1"><strong>15/05/2019 - 15:33</strong> Oi André?</p>
                 <p class="card-subtitle mb-1"><strong>15/05/2019 - 15:33</strong> Oi André?</p>
                 <p class="card-subtitle mb-1"><strong>15/05/2019 - 15:33</strong> Oi André?</p>
                 <p class="card-subtitle mb-1"><strong>15/05/2019 - 15:33</strong> Oi André?</p>
             </div>
             <a href="#" class="btn-circle-gradient -aux"><i class="material-icons">add</i></a>
            </div>
          </div>
      </div>
    </div>
  </div>
</section>

<section id="section-detalhe-card-02">
  <div class="container-fluid">
    <div class="row mt-4">
      <div class="col-10 align-self-center">
        <h4 class="title mt-0 mb-3">Tarefas em aberto:</h4>
      </div>
    </div>

    <div class="row ">
      <div class="col-12">
        <div class="card pb-4 position-relative">
            <ul class="list-group list-group-flush">
                <li class="list-group-item">
                    <h3 class="card-title mb-1">Oseas moreto (C&A)</h3>
                    <div>
                        <p class="card-subtitle checklist -complete m-0">Fazer arte do post</p>
                        <p class="card-text checklist -incomplete m-0">Ligar para Edu</p>
                    </div>
                </li>
                <hr class="m-0">
                <li class="list-group-item border-0">
                    <h3 class="card-title mb-1">Oseas moreto (C&A)</h3>
                    <div>
                        <p class="card-subtitle checklist -complete m-0">Fazer arte do post</p>
                        <p class="card-text checklist -incomplete m-0">Ligar para Edu</p>
                    </div>
                </li>
            </ul>
            <a href="#" class="defaut-btn-gradient2 border-0 text-center -aux">Enviar para Oséas</a>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="section-detalhe-card-03 mb-5">
  <div class="container-fluid">
    <div class="row mt-5">
      <div class="col-10 align-self-center">
        <p class="title-text mt-0 mb-1">Qual a próxima mensagem?</p>
      </div>
    </div>

    <div class="row mb-100">
      <div class="col-12">
        <div class="dropdown">
            <button class="defaut-dropdown-gradient border-0 dropdown-toggle w-100" type="button" id="dropdownMenuButton"
            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Escolha uma opção
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="#">Como estão as coisas?</a>
                <a class="dropdown-item" href="#">Outra ação</a>
                <a class="dropdown-item" href="#">Alguma coisa aqui</a>
            </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php include("includes/script.php")?>
</body>
</html>