<?php

namespace App\Routes;

use \App\Core\Routes;

\Slim\Slim::registerAutoloader();

class Intranet extends Routes
{

    protected $routes = [];
    protected $routescrud = [];

    public function __construct()
    {
        $app = new \Slim\Slim(array(
            'templates.path' => 'templates'
        ));

        // rotas index
        $this->routes = [
            '/',
            '/home',
            '/index',
            '/dashboard'
        ];

        //rotas sistema
        $app->group('/sistema', function () use ($app) {
            foreach ($this->routes as $route) {
                $app->get($route, function () use ($app) {
                    $this->initroute('Sistema', 'Home', 'index');
                });
            }

            $app->map('/perfil/:id', function ($id) use ($app) {
                $data[] = $id;
                $this->initroute('Sistema', 'Home', 'perfil', $data);
            })->via('GET', 'POST');

            $app->map('/uploads', function () use ($app) {
                $this->initroute('Sistema', 'Home', 'uploads');
            })->via('GET', 'POST');

            //rotas login
            $app->group('/login', function () use ($app) {

                $app->get('/', function () use ($app) {
                    $this->initroute('Sistema', 'Login', 'index');
                });

                $app->post('/logar', function () use ($app) {
                    $request = $app->request->getBody();
                    parse_str($request, $get_array);

                    $data[] = $get_array;

                    $this->initroute('Sistema', 'Login', 'logar', $data);
                });

                $app->post('/retrievepassword', function () use ($app) {
                    $request = $app->request->getBody();
                    parse_str($request, $get_array);

                    $data[] = $get_array;

                    $this->initroute('Sistema', 'Login', 'retrievepassword', $data);
                });

                $app->get('/logout', function () use ($app) {
                    $this->initroute('Sistema', 'Login', 'logout');
                });
            });

            //rotas contato
            $app->group('/contacts', function () use ($app) {
                $app->get('/', function () use ($app) {
                    $this->initroute('Sistema', 'Contact', 'index');
                });

                $app->get('/index', function () use ($app) {
                    $this->initroute('Sistema', 'Contact', 'index');
                });

                $app->map('/view/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Contact', 'view', $data);
                })->via('GET', 'POST');

                $app->get('/destroy/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Contact', 'destroy', $data);
                });
            });

            //rotas grupos
            $app->group('/gruposacesso', function () use ($app) {
                $app->get('/', function () use ($app) {
                    $this->initroute('Sistema', 'Groups', 'index');
                });

                $app->get('/index', function () use ($app) {
                    $this->initroute('Sistema', 'Groups', 'index');
                });

                $app->map('/create', function () use ($app) {
                    $this->initroute('Sistema', 'Groups', 'create');
                })->via('GET', 'POST');

                $app->map('/update/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Groups', 'update', $data);
                })->via('GET', 'POST');

                $app->get('/destroy/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Groups', 'destroy', $data);
                });
            });

            //rotas menus
            $app->group('/menus', function () use ($app) {
                $app->get('/', function () use ($app) {
                    $this->initroute('Sistema', 'Menus', 'index');
                });

                $app->get('/index', function () use ($app) {
                    $this->initroute('Sistema', 'Menus', 'index');
                });

                $app->map('/create', function () use ($app) {
                    $this->initroute('Sistema', 'Menus', 'create');
                })->via('GET', 'POST');

                $app->map('/update/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Menus', 'update', $data);
                })->via('GET', 'POST');

                $app->get('/destroy/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Menus', 'destroy', $data);
                });
            });

            //rotas users
            $app->group('/users', function () use ($app) {
                $app->get('/', function () use ($app) {
                    $this->initroute('Sistema', 'User', 'index');
                });

                $app->get('/index', function () use ($app) {
                    $this->initroute('Sistema', 'User', 'index');
                });

                $app->map('/create', function () use ($app) {
                    $this->initroute('Sistema', 'User', 'create');
                })->via('GET', 'POST');

                $app->map('/update/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'User', 'update', $data);
                })->via('GET', 'POST');

                $app->get('/destroy/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'User', 'destroy', $data);
                });

                $app->get('/block/:id/:s', function ($id, $s) use ($app) {
                    $data[] = $id;
                    $data[] = $s;
                    $this->initroute('Sistema', 'User', 'block', $data);
                });
            });

            //rotas settings
            $app->group('/settings', function () use ($app) {
                $app->map('/', function () use ($app) {
                    $this->initroute('Sistema', 'Settings', 'index');
                })->via('GET', 'POST');

                $app->map('/index', function () use ($app) {
                    $this->initroute('Sistema', 'Settings', 'index');
                })->via('GET', 'POST');

                $app->map('/create', function () use ($app) {
                    $this->initroute('Sistema', 'Settings', 'create');
                })->via('GET', 'POST');

                $app->map('/update/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Settings', 'update', $data);
                })->via('GET', 'POST');

                $app->map('/uploads', function () use ($app) {
                    $this->initroute('Sistema', 'Settings', 'uploads');
                })->via('GET', 'POST');

                $app->get('/destroy/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Settings', 'destroy', $data);
                });
            });

            //rotas slides
            $app->group('/slides', function () use ($app) {
                $app->get('/', function () use ($app) {
                    $this->initroute('Sistema', 'Slide', 'index');
                });

                $app->get('/index', function () use ($app) {
                    $this->initroute('Sistema', 'Slide', 'index');
                });

                $app->map('/create', function () use ($app) {
                    $this->initroute('Sistema', 'Slide', 'create');
                })->via('GET', 'POST');

                $app->map('/update/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Slide', 'update', $data);
                })->via('GET', 'POST');

                $app->map('/uploads', function () use ($app) {
                    $this->initroute('Sistema', 'Slide', 'uploads');
                })->via('GET', 'POST');

                $app->get('/destroy/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Slide', 'destroy', $data);
                });
            });

            //rotas institucional
            $app->group('/institucional', function () use ($app) {
                $app->get('/', function () use ($app) {
                    $this->initroute('Sistema', 'Institucional', 'index');
                });

                $app->get('/index', function () use ($app) {
                    $this->initroute('Sistema', 'Institucional', 'index');
                });

                $app->map('/create', function () use ($app) {
                    $this->initroute('Sistema', 'Institucional', 'create');
                })->via('GET', 'POST');

                $app->map('/update/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Institucional', 'update', $data);
                })->via('GET', 'POST');

                $app->map('/uploads', function () use ($app) {
                    $this->initroute('Sistema', 'Institucional', 'uploads');
                })->via('GET', 'POST');

                $app->get('/destroy/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Institucional', 'destroy', $data);
                });
            });

            //rotas team
            $app->group('/team', function () use ($app) {
                $app->get('/', function () use ($app) {
                    $this->initroute('Sistema', 'Teams', 'index');
                });

                $app->get('/index', function () use ($app) {
                    $this->initroute('Sistema', 'Teams', 'index');
                });

                $app->map('/create', function () use ($app) {
                    $this->initroute('Sistema', 'Teams', 'create');
                })->via('GET', 'POST');

                $app->map('/update/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Teams', 'update', $data);
                })->via('GET', 'POST');

                $app->map('/uploads', function () use ($app) {
                    $this->initroute('Sistema', 'Teams', 'uploads');
                })->via('GET', 'POST');

                $app->get('/destroy/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Teams', 'destroy', $data);
                });
            });

            //rotas portifolios
            $app->group('/portifolios', function () use ($app) {
                $app->get('/', function () use ($app) {
                    $this->initroute('Sistema', 'Portifolios', 'index');
                });

                $app->get('/index', function () use ($app) {
                    $this->initroute('Sistema', 'Portifolios', 'index');
                });

                $app->map('/create', function () use ($app) {
                    $this->initroute('Sistema', 'Portifolios', 'create');
                })->via('GET', 'POST');

                $app->map('/update/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Portifolios', 'update', $data);
                })->via('GET', 'POST');

                $app->map('/uploads', function () use ($app) {
                    $this->initroute('Sistema', 'Portifolios', 'uploads');
                })->via('GET', 'POST');

                $app->map('/uploadsimages', function () use ($app) {
                    $this->initroute('Sistema', 'Portifolios', 'uploadsimages');
                })->via('GET', 'POST');

                $app->get('/destroy/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Portifolios', 'destroy', $data);
                });

                $app->map( '/deleteimage', function () use ($app) {
                    $this->initroute('Sistema', 'Portifolios', 'deleteimage');
                })->via('GET', 'POST');

                //rotas types
                $app->group( '/types', function () use ($app) {
                    $app->get('/', function () use ($app) {
                        $this->initroute('Sistema', 'PortifoliosTypes', 'index');
                    });

                    $app->get('/index', function () use ($app) {
                        $this->initroute('Sistema', 'PortifoliosTypes', 'index');
                    });

                    $app->map('/create', function () use ($app) {
                        $this->initroute('Sistema', 'PortifoliosTypes', 'create');
                    })->via('GET', 'POST');

                    $app->map('/update/:id', function ($id) use ($app) {
                        $data[] = $id;
                        $this->initroute('Sistema', 'PortifoliosTypes', 'update', $data);
                    })->via('GET', 'POST');

                    $app->get('/destroy/:id', function ($id) use ($app) {
                        $data[] = $id;
                        $this->initroute('Sistema', 'PortifoliosTypes', 'destroy', $data);
                    });
                });
            });

            //rotas services
            $app->group( '/services', function () use ($app) {
                $app->get('/', function () use ($app) {
                    $this->initroute('Sistema', 'Services', 'index');
                });

                $app->get('/index', function () use ($app) {
                    $this->initroute('Sistema', 'Services', 'index');
                });

                $app->map('/create', function () use ($app) {
                    $this->initroute('Sistema', 'Services', 'create');
                })->via('GET', 'POST');

                $app->map('/update/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Services', 'update', $data);
                })->via('GET', 'POST');

                $app->get('/destroy/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Services', 'destroy', $data);
                });
            });

            //rotas blog
            $app->group('/blog', function () use ($app) {
                $app->get('/', function () use ($app) {
                    $this->initroute('Sistema', 'Blogs', 'index');
                });

                $app->get('/index', function () use ($app) {
                    $this->initroute('Sistema', 'Blogs', 'index');
                });

                $app->map('/create', function () use ($app) {
                    $this->initroute('Sistema', 'Blogs', 'create');
                })->via('GET', 'POST');

                $app->map('/update/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Blogs', 'update', $data);
                })->via('GET', 'POST');

                $app->map('/uploads', function () use ($app) {
                    $this->initroute('Sistema', 'Blogs', 'uploads');
                })->via('GET', 'POST');

                $app->map('/uploadsimages', function () use ($app) {
                    $this->initroute('Sistema', 'Blogs', 'uploadsimages');
                })->via('GET', 'POST');

                $app->get('/destroy/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Blogs', 'destroy', $data);
                });

                $app->map( '/deleteimage', function () use ($app) {
                    $this->initroute('Sistema', 'Blogs', 'deleteimage');
                })->via('GET', 'POST');
            });

            //rotas videos
            $app->group('/videos', function () use ($app) {
                $app->get('/', function () use ($app) {
                    $this->initroute('Sistema', 'Videos', 'index');
                });

                $app->get('/index', function () use ($app) {
                    $this->initroute('Sistema', 'Videos', 'index');
                });

                $app->map('/create', function () use ($app) {
                    $this->initroute('Sistema', 'Videos', 'create');
                })->via('GET', 'POST');

                $app->map('/update/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Videos', 'update', $data);
                })->via('GET', 'POST');

                $app->get('/destroy/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Videos', 'destroy', $data);
                });
            });

            //rotas seo
            $app->group('/seo', function () use ($app) {
                $app->get('/', function () use ($app) {
                    $this->initroute('Sistema', 'Seo', 'index');
                })->via('GET', 'POST');

                $app->map('/index', function () use ($app) {
                    $this->initroute('Sistema', 'Seo', 'index');
                })->via('GET', 'POST');
            });

        });

        $app->run();
    }
}
