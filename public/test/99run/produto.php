<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("includes/head.php")?>
</head>
<body>
<?php include("includes/menu-top-bar.php")?>
<?php include("includes/menu-side-bar.php")?>
<?php include("includes/menu-bottom.php")?>

<header id="header-produto">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12 text-center">
          <div class="slider-product-page owl-carousel owl-theme">
            <div class="item">
                <figure class="card card-product">
                  <img src="assets/images/camiseta.png" class="img-fluid product" width="100px" />
                </figure>
            </div>

            <div class="item">
                <figure class="card card-product">
                  <img src="assets/images/camiseta.png" class="img-fluid product" width="100px" />
                </figure>
            </div>

            <div class="item">
                <figure class="card card-product">
                  <img src="assets/images/camiseta.png" class="img-fluid product" width="100px" />
                </figure>
            </div>

            <div class="item">
                <figure class="card card-product">
                  <img src="assets/images/camiseta.png" class="img-fluid product" width="100px" />
                </figure>
            </div>

            <div class="item">
                <figure class="card card-product">
                  <img src="assets/images/camiseta.png" class="img-fluid product" width="100px" />
                </figure>
            </div>
          </div>
      </div>
    </div>
	<div class="row">
		<aside class="col-12">
      <article class="card-body">
        <h3 class="title mb-3">Camiseta de corrida Ecosport fit de lã</h3>

      <p class="price-detail-wrap"> 
        <span class="price h3 text-warning"> 
          <span class="currency">R$</span><span class="num">33,90</span>
        </span>
        <br> 
        <span class="parc">3x sem juros</span> 
      </p>

      <div class="row">
          <div class="col-12">
            <div class="param param-inline mb-4">
                <h6 class="title">Escolha uma cor: </h6>
                <div class="btn-group btn-group-toggle colors" data-toggle="buttons">
                  <label class="btn btn-color active">
                    <input type="radio" class="color" name="options" id="option1" autocomplete="off" checked>
                  </label>

                  <label class="btn btn-color">
                    <input type="radio" class="color" name="options" id="option2" autocomplete="off">
                  </label>

                  <label class="btn btn-color">
                    <input type="radio" class="color" name="options" id="option3" autocomplete="off">
                  </label>

                  <label class="btn btn-color">
                    <input type="radio" class="color" name="options" id="option4" autocomplete="off">
                  </label>

                  <label class="btn btn-color">
                    <input type="radio" class="color" name="options" id="option5" autocomplete="off">
                  </label>
                </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-12">
            <div class="item-property">
              <h5 class="title">Detalhes do produto</h5>
              <p>Here goes description consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco</p>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-12">
            <div class="param param-inline">
                <h6 class="title">Escolha um tamanho: </h6>
                <div class="btn-group btn-group-toggle sizes" data-toggle="buttons">
                  <label class="btn btn-size active">
                    <input type="radio" name="options" id="option1" autocomplete="off" checked> P
                  </label>

                  <label class="btn btn-size">
                    <input type="radio" name="options" id="option2" autocomplete="off"> M
                  </label>

                  <label class="btn btn-size">
                    <input type="radio" name="options" id="option3" autocomplete="off"> G
                  </label>

                  <label class="btn btn-size">
                    <input type="radio" name="options" id="option4" autocomplete="off"> GG
                  </label>

                  <label class="btn btn-size">
                    <input type="radio" name="options" id="option5" autocomplete="off"> XL
                  </label>
                </div>
            </div>
          </div>
        </div>
        <hr>

        <div class="row mt-3">
            <div class="col-12 text-center">
                <div class="box-video">
                    <iframe width="100%" height="215" src="https://www.youtube.com/embed/7H0pKlziMto"
                        frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
                </div>
            </div>
        </div>

        <div class="row">
          <div class="col-12 text-center">
          <h5 class="title mt-4">Depoimentos</h5>
            <div class="slider-depositions owl-carousel owl-theme">
                <div class="item">
                  <div class="item-property">
                    <p class="0">Here goes description consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco</p>
                    <div class="rating">
                      <input type="radio" id="star5" name="rating" value="5" /><label for="star5" title="Meh">5 stars</label>
                      <input type="radio" id="star4" name="rating" value="4" /><label for="star4" title="Kinda bad">4 stars</label>
                      <input type="radio" id="star3" name="rating" value="3" /><label for="star3" title="Kinda bad">3 stars</label>
                      <input type="radio" id="star2" name="rating" value="2" /><label for="star2" title="Sucks big tim">2 stars</label>
                      <input type="radio" id="star1" name="rating" value="1" /><label for="star1" title="Sucks big time">1 star</label>
                    </div>
                    <p class="text-light font-weight-bold">- Felipe Henrique</p>
                  </div>
                </div>

                <div class="item">
                  <div class="item-property">
                    <p class="0">Here goes description consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco</p>
                    <div class="rating">
                      <input type="radio" id="star5" name="rating" value="5" /><label for="star5" title="Meh">5 stars</label>
                      <input type="radio" id="star4" name="rating" value="4" /><label for="star4" title="Kinda bad">4 stars</label>
                      <input type="radio" id="star3" name="rating" value="3" /><label for="star3" title="Kinda bad">3 stars</label>
                      <input type="radio" id="star2" name="rating" value="2" /><label for="star2" title="Sucks big tim">2 stars</label>
                      <input type="radio" id="star1" name="rating" value="1" /><label for="star1" title="Sucks big time">1 star</label>
                    </div>
                    <p class="text-light font-weight-bold">- Felipe Henrique</p>
                  </div>
                </div>

                <div class="item">
                  <div class="item-property">
                    <p class="0">Here goes description consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco</p>
                    <div class="rating">
                      <input type="radio" id="star5" name="rating" value="5" /><label for="star5" title="Meh">5 stars</label>
                      <input type="radio" id="star4" name="rating" value="4" /><label for="star4" title="Kinda bad">4 stars</label>
                      <input type="radio" id="star3" name="rating" value="3" /><label for="star3" title="Kinda bad">3 stars</label>
                      <input type="radio" id="star2" name="rating" value="2" /><label for="star2" title="Sucks big tim">2 stars</label>
                      <input type="radio" id="star1" name="rating" value="1" /><label for="star1" title="Sucks big time">1 star</label>
                    </div>
                    <p class="text-light font-weight-bold">- Felipe Henrique</p>
                  </div>
                </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12 text-center">
            <a href="carrinho.php" class="defaut-btn-gradient mt-4">Adicionar ao carrinho</a>
          </div>
        </div>
      </article>
		</aside>
	</div>
  </div>
</header>

<?php include("includes/script.php")?>
</body>
</html>