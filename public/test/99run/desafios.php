<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("includes/head.php")?>
</head>
<body>
<?php include("includes/menu-top-bar.php")?>
<?php include("includes/menu-side-bar.php")?>
<?php include("includes/menu-bottom.php")?>

<header id="header-desafios">
  <div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="box-info mb-4">
                <h1 class="title">Preparado para<br>encarar novos desafios?</h1>
                <div class="bar-little"></div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="box validade-challenge text-center">
                <img src="assets/images/fenomenos-naturais.jpg" class="img-fluid challenges-bg"/>
                <div class="infos-validade">
                    <h6 class="title">Ainda faltam...</span></h6>
                    <div class="progress" style="height: 15px;">
                        <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
                    </div>
                    <a href="validar-detalhe.php" class="defaut-btn-gradient">Validar inscrição</a>
                </div>
            </div>

            <div class="box">
                <span class="badget-price -aux-price">A partir de<br><strong>29,90</strong></span>
                <img src="assets/images/fenomenos-naturais.jpg" class="img-fluid challenges-bg"/>
                <div class="infos">
                    <h6 class="title">Desafio<br><span>Fenêmenos naturais</span></h6>
                    <a href="inscricao.php" class="defaut-btn-gradient">Inscreva-se <i class="flaticon-right"></i></a>
                </div>
            </div>

            <div class="box">
                <span class="badget-price -aux-price">A partir de<br><strong>29,90</strong></span>
                <img src="assets/images/fenomenos-naturais.jpg" class="img-fluid challenges-bg"/>
                <div class="infos">
                    <h6 class="title">Desafio<br><span>Fenêmenos naturais</span></h6>
                    <a href="inscricao.php" class="defaut-btn-gradient">Inscreva-se <i class="flaticon-right"></i></a>
                </div>
            </div>

            <div class="box">
                <span class="badget-price -aux-price">A partir de<br><strong>29,90</strong></span>
                <img src="assets/images/fenomenos-naturais.jpg" class="img-fluid challenges-bg"/>
                <div class="infos">
                    <h6 class="title">Desafio<br><span>Fenêmenos naturais</span></h6>
                    <a href="inscricao.php" class="defaut-btn-gradient">Inscreva-se <i class="flaticon-right"></i></a>
                </div>
            </div>

            <div class="box">
                <span class="badget-price -aux-price">A partir de<br><strong>29,90</strong></span>
                <img src="assets/images/fenomenos-naturais.jpg" class="img-fluid challenges-bg"/>
                <div class="infos">
                    <h6 class="title">Desafio<br><span>Fenêmenos naturais</span></h6>
                    <a href="inscricao.php" class="defaut-btn-gradient">Inscreva-se <i class="flaticon-right"></i></a>
                </div>
            </div>

            <div class="box">
                <span class="badget-price -aux-price">A partir de<br><strong>29,90</strong></span>
                <img src="assets/images/fenomenos-naturais.jpg" class="img-fluid challenges-bg"/>
                <div class="infos">
                    <h6 class="title">Desafio<br><span>Fenêmenos naturais</span></h6>
                    <a href="inscricao.php" class="defaut-btn-gradient">Inscreva-se <i class="flaticon-right"></i></a>
                </div>
            </div>

            <div class="box">
                <span class="badget-price -aux-price">A partir de<br><strong>29,90</strong></span>
                <img src="assets/images/fenomenos-naturais.jpg" class="img-fluid challenges-bg"/>
                <div class="infos">
                    <h6 class="title">Desafio<br><span>Fenêmenos naturais</span></h6>
                    <a href="inscricao.php" class="defaut-btn-gradient">Inscreva-se <i class="flaticon-right"></i></a>
                </div>
            </div>

            <div class="box">
                <span class="badget-price -aux-price">A partir de<br><strong>29,90</strong></span>
                <img src="assets/images/fenomenos-naturais.jpg" class="img-fluid challenges-bg"/>
                <div class="infos">
                    <h6 class="title">Desafio<br><span>Fenêmenos naturais</span></h6>
                    <a href="inscricao.php" class="defaut-btn-gradient">Inscreva-se <i class="flaticon-right"></i></a>
                </div>
            </div>
        </div>
    </div>
  </div>
</header>


<?php include("includes/script.php")?>
</body>
</html>