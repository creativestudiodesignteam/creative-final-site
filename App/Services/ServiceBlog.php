<?php

/**
 * Created by PhpStorm.
 * User: oseas
 * Date: 08/12/2018
 * Time: 00:38
 */

namespace App\Services;

use \App\Models\Entities\Blog;


class ServiceBlog
{

    public function create($request)
    {
        if ($request) {
            $obj = new Blog();
            $this->save($request, $obj);

            $return = [];
            if ($obj->idblog <> '') {
                $return['success'] = true;
                $return['data'] = $obj;
            } else {
                $return['success'] = false;
                $return['message'] = "Não foi possivel inserir a Blog <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function update($request)
    {
        if ($request) {
            $obj = Blog::find($request['id']);
            $this->save($request, $obj);

            $return = [];
            if ($obj->idblog <> '') {

                $return['success'] = true;
                $return['data'] = $obj;
            } else {
                $return['success'] = false;
                $return['message'] = "Não foi possivel atualizar a Blog <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function destroy($id)
    {
        $departamento = Blog::find($id);
        $departamento->status = 'd';
        $resp = $departamento->save();

        $return = [];

        if ($resp) {
            $return['success'] = true;
        } else {
            $return['success'] = false;
        }

        return $return;
    }

    public function save($request, $obj)
    {
        $obj->title     = $request['title'];
        $obj->resume   = $request['resume'];

        if(isset($request['date_create'])){
            $obj->date_create = $request['date_create'];
        }
        
        $obj->date_update = $request['date_update'];
        $obj->posting_date = $request['posting_date'];
        $obj->image     = $request['image'];
        $obj->status    = $request['status'];
        $obj->text      = $request['text'];
        $obj->iduser      = $request['iduser'];

        $obj->save();
    }
}
