<?php

namespace App\Models\Entities;

use App\Core\Models;
use \App\Models\Entities\Unit;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Arquivos
 *
 * @author oseas
 */
class Contacts extends Models {
    protected $table = 'contacts';
    protected $primaryKey = 'idcontact';
    public $timestamps = false;
    protected $fillable = [ 'name', 'email', 'phone', 'subject', 'view', 'text', 'date_create', 'idunits'];
    protected $guarded = [];

    public function unit(){
        return $this->belongsTo(Unit::class, 'idunits');
    }
}
