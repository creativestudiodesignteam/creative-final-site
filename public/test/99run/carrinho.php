<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("includes/head.php")?>
</head>
<body>
<?php include("includes/menu-top-bar.php")?>
<?php include("includes/menu-side-bar.php")?>
<?php include("includes/menu-bottom.php")?>

<header id="header-carrinho">
  <div class="container-fluid">
    <div class="card mb-3">
      <div class="row">
          <div class="col-4 align-self-center">
            <img src="assets/images/camiseta.png" class="w-100"/>
          </div>
          <div class="col-8 px-3 align-self-center position-relative">
              <a href="#.">
                  <i class="flaticon-multiply"></i>
              </a>
            <div class="card-block px-3">
              <h5 class="card-title">Camiseta de corrida Ecosport fit de lã</h5>
              <h4 class="card-title">R$ 33,90</h4>
                <div class="form-group select">
                    <select class="form-control" id="exampleSelect1">
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                    </select>
                </div>
            </div>
          </div>
        </div>
      </div>

      <div class="card mb-3">
      <div class="row">
          <div class="col-4 align-self-center">
            <img src="assets/images/camiseta.png" class="w-100"/>
          </div>
          <div class="col-8 px-3 align-self-center position-relative">
              <a href="#.">
                  <i class="flaticon-multiply"></i>
              </a>
            <div class="card-block px-3">
              <h5 class="card-title">Camiseta de corrida Ecosport fit de lã</h5>
              <h4 class="card-title">R$ 33,90</h4>
                <div class="form-group select">
                    <select class="form-control" id="exampleSelect1">
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                    </select>
                </div>
            </div>
          </div>
        </div>
      </div>

      <div class="card mb-3">
      <div class="row">
          <div class="col-4 align-self-center">
            <img src="assets/images/camiseta.png" class="w-100"/>
          </div>
          <div class="col-8 px-3 align-self-center position-relative">
              <a href="#.">
                  <i class="flaticon-multiply"></i>
              </a>
            <div class="card-block px-3">
              <h5 class="card-title">Camiseta de corrida Ecosport fit de lã</h5>
              <h4 class="card-title">R$ 33,90</h4>
                <div class="form-group select">
                    <select class="form-control" id="exampleSelect1">
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                    </select>
                </div>
            </div>
          </div>
        </div>
      </div>

      <div class="card mb-3">
      <div class="row">
          <div class="col-4 align-self-center">
            <img src="assets/images/camiseta.png" class="w-100"/>
          </div>
          <div class="col-8 px-3 align-self-center position-relative">
              <a href="#.">
                  <i class="flaticon-multiply"></i>
              </a>
            <div class="card-block px-3">
              <h5 class="card-title">Camiseta de corrida Ecosport fit de lã</h5>
              <h4 class="card-title">R$ 33,90</h4>
                <div class="form-group select">
                    <select class="form-control" id="exampleSelect1">
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                    </select>
                </div>
            </div>
          </div>
        </div>
      </div>
    
      <div class="row">
        <div class="col-12 text-center">
          <div class="box-address">
            <h5 class="address">Rua Atuaú - pinheiros, 237 </h5>
            <a href="endereco.php">Clique aqui para alterar o endereço</a>
          </div>
        </div>
      </div>

      <div class="row mt-5">
        <div class="col-12 align-self-center text-center">
            <h5 class="pedido">Valor do pedido:<span> R$80,00</span></h5>
            <h5 class="frete">Valor do frete:<span> R$13,50</span><a href="#"> Dividir frete</a></h5>
            <hr>
            <h2 class="total">Total <span>R$101,70</span></h2>
            <a href="forma-pagamento.php" class="defaut-btn-gradient mt-2 d-inline-block">Finalizar pedido</a>
        </div>
      </div>
  </div>
</header>


<?php include("includes/script.php")?>
</body>
</html>