<?php

namespace App\Controllers\Sistema;

use \App\Controllers\Sistema\Common;
use \App\Models\Entities\SiteServices;
use App\Services\ServiceServices;

/**
 * Description of Home
 *
 * @author oseas
 */
class Services extends Common{

    protected $service;
    protected $pagelink = 'sistema/services';
    protected $obj;
    protected $id_class = 'usuarios';

    public function __construct(){
        $this->start_session();
        $this->view = new \stdClass();
        $this->service = new ServiceServices();
        $this->obj = new SiteServices();
        $this->obj->status = 'a';

    }

    public function index() {
        $this->start_session();

        $this->titulo_pagina = 'Serviços';
        $objs = SiteServices::where('status', '<>', 'd')->get();

        $breadcrumb['url'][0] = '#';
        $breadcrumb['label'][0] = 'Lista de Serviços ';
        $breadcrumb['active'][0] = 'active';
        $this->view->breadcrumb = $breadcrumb;
        $this->view->url = '/'.$this->pagelink;
        $this->view->titulo = 'Lista de Serviços';
        $this->view->objs = $objs;

        if(isset($_SESSION['message'])){
            $this->view->errormessage = $_SESSION['message'];
            $this->view->classe = $_SESSION['classe'];
            unset($_SESSION['message']);
            unset($_SESSION['classe']);
        }

        $this->render('index', $this->folder, $this->page);
    }

    public function create(){
        $this->start_session();
        $this->titulo_pagina = 'Serviços - Cadastro';
        $this->id_class  = 'cadastrar-anuncio';

        $breadcrumb['url'][0] = '/'.$this->pagelink;
        $breadcrumb['label'][0] = 'Lista de Serviços';
        $breadcrumb['active'][0] = '';
        $breadcrumb['url'][1] = '#';
        $breadcrumb['label'][1] = 'Cadastrar';
        $breadcrumb['active'][1] = 'active';
        $this->view->breadcrumb = $breadcrumb;

        $this->view->url = '/'.$this->pagelink;
        $this->view->action = '/'.$this->pagelink.'/create';
        $this->view->titulo = 'Lista de Serviços';
        $this->view->label = 'Cadastrar';
        $this->view->class_btn = 'btn btn-success';
        $this->view->obj = $this->obj;

        if($_POST){
            $request = $this->service->create($_POST);
            $obj = $request['success'] ? $request['data'] : null;

            if($request['success']){
                $_SESSION['message'] = "Item <strong>{$obj->text1}</strong> Inserido com sucesso!";
                $_SESSION['classe'] = 'alert-success';
                $return['response']['mensagem'] = $_SESSION['message'];
                $return['response']['classe'] = $_SESSION['classe'];
                $return['response']['result'] = 'success';
                $return['response']['redirect'] = '/'.$this->pagelink;
            }else{
                $return['response']['mensagem'] = $request['message'];
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';
            }

            print_r(json_encode($return));
            exit();
        }

        $this->render('form', $this->folder, $this->page);
    }

    public function update($id, $data = []){
        $this->start_session();
        $this->titulo_pagina = 'Serviços - Editar';
        $this->id_class  = 'cadastrar-anuncio';
        $obj = SiteServices::find($id);

        $breadcrumb['url'][0] = '/'.$this->pagelink;
        $breadcrumb['label'][0] = 'Lista de Serviços ';
        $breadcrumb['active'][0] = '';
        $breadcrumb['url'][1] = '#';
        $breadcrumb['label'][1] = 'Alterar';
        $breadcrumb['active'][1] = 'active';
        $this->view->breadcrumb = $breadcrumb;

        $this->view->url = '/'.$this->pagelink;
        $this->view->action = '/'.$this->pagelink.'/update/'.$id;
        $this->view->titulo = 'Lista de Serviços';
        $this->view->label = 'Alterar';
        $this->view->class_btn = 'btn btn-warning';
        $this->view->obj = $obj;

        if($_POST){
            $request = $this->service->update($_POST);
            $obj = $request['success'] ? $request['data'] : null;

            if($request['success']){
                $_SESSION['message'] = "Item <strong>{$obj->text1}</strong> alterado com sucesso!";
                $_SESSION['classe'] = 'alert-success';
                $return['response']['mensagem'] = $_SESSION['message'];
                $return['response']['classe'] = $_SESSION['classe'];
                $return['response']['result'] = 'success';
                $return['response']['redirect'] = '/'.$this->pagelink;
            }else{
                $return['response']['mensagem'] = $request['message'];
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';
            }

            print_r(json_encode($return));
            exit();
        }


        $this->render('form', $this->folder, $this->page);
    }

    public function destroy($id){
        $this->start_session();
        $obj = SiteServices::find($id);
        $request = $this->service->destroy($id);

        if($request['success']){
            $_SESSION['message'] = "Item <strong>{$obj->text1}</strong> excluido com sucesso!";
            $_SESSION['classe'] = 'alert-success';
        }else{
            $_SESSION['message'] = "Não foi possivel excluir o Item <strong>{$obj->text1}</strong>";
            $_SESSION['classe'] = 'alert-danger';
        }

        print_r(json_encode($request));
        exit();

    }

    public function uploads(){
      if(isset($_FILES['file']['name']) && $_FILES['file']['name'] <> ''){
        $responseimg = $this->upload($_FILES, 'item');

        if($responseimg['success']){
            $return['response']['classe'] = 'alert-success';
            $return['response']['result'] = 'success';
            $return['response']['image'] = $responseimg['image'];
            $return['response']['status'] = 1;
            $return['response']['redirect'] = '/'.$this->pagelink;
        }else{
            $return['response']['classe'] = 'alert-danger';
            $return['response']['result'] = 'error';
            $return['response']['redirect'] = '';
            $return['response']['status'] = 0;
        }

        $return['response']['mensagem'] = $responseimg['message'];
        print_r(json_encode($return));
        exit();
      }
    }
}
