<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("includes/head.php")?>
</head>
<body>

<?php include("includes/menu-side-bar.php")?>
<?php include("includes/menu-bottom.php")?>

<header id="header-checklist-pessoas">
  <div class="d-flex justify-content-between menu-top">
      <div class="p-2 align-self-center">
          <a class="btn-back" href="#"><i class="material-icons">arrow_back</i> Voltar</a>
      </div>
      <div class="p-2 align-self-center">
          <a href="#" class="peoples">Oséas Moreto</a>
      </div>
      <div class="p-2 align-self-center">
          <ul class="list-unstyled list-inline m-0 list-icons-menu">
              <li class="list-inline-item"><a href="#" class="edit-icon"><i class="material-icons">edit</i></a></li>
          </ul>
      </div>
  </div>

  <div class="container-fluid">
        <div class="row">
          <div class="col-12 text-center">
            <h4 class="title-header mt-2 mb-4">Sua lista de tarefas com <span class="bg-text-gradient">Oséas Moreto</span></h4>
          </div>
        </div>
      </div>
</header>

<section id="section-checklist-pessoas-01">
  <div class="container-fluid">
    <div class="row mt-4">
      <div class="col-10 align-self-center">
        <h4 class="title mt-0 mb-3">Tarefas em aberto:</h4>
      </div>
    </div>

    <div class="row ">
      <div class="col-12">
          <div class="card mb-5 position-relative">
            <div class="card-body">
              <h3 class="card-title mb-1">Oseas moreto - 2 tarefas</h3>
              <p class="card-subtitle checklist -complete m-0">Fazer arte do post</p>
              <p class="card-text checklist -incomplete m-0">Ligar para Edu</p>
            </div>
            <a href="#" class="btn-circle-gradient -aux"><i class="material-icons">add</i></a>
          </div>

          <div class="card mb-5 position-relative">
            <div class="card-body">
              <h3 class="card-title mb-1">Oseas moreto - 2 tarefas</h3>
              <p class="card-subtitle checklist -complete m-0">Fazer arte do post</p>
              <p class="card-text checklist -incomplete m-0">Ligar para Edu</p>
            </div>
            <a href="#" class="btn-circle-gradient -aux"><i class="material-icons">add</i></a>
          </div>
      </div>
    </div>
  </div>
</section>

<section id="section-checklist-pessoas-02 mb-5">
  <div class="container-fluid">
    <div class="row mb-100">
      <div class="col-12 text-center">
        <a href="#" class="defaut-btn-gradient2 border-0">Enviar para Oséas</a>
      </div>
    </div>
  </div>
</section>

<?php include("includes/script.php")?>
</body>
</html>