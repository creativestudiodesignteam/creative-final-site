<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("includes/head.php")?>
</head>
<body>
<?php include("includes/menu-top-bar.php")?>
<?php include("includes/menu-side-bar.php")?>
<?php include("includes/menu-bottom.php")?>

<header id="header-endereco">
  <div class="container-fluid">
    <div class="row">
        <div class="col-12 text-center">
            <div class="box-info">
                <div class="bar-big"></div>
                <h1 class="title"><span>olá Confira seu</span><br>Endereço de entrega</h1>
            </div>
        </div>
    </div>

    <form class="form-address mt-4">
        <div class="form-group row">
            <label for="staticEmail" class="col-2 col-form-label">Cep</label>
            <div class="col-10">
                <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="email@example.com">
            </div>
        </div>

        <div class="form-group row">
            <label for="staticEmail" class="col-4 col-form-label">Endereço</label>
            <div class="col-8">
                <input type="text" class="form-control-plaintext" id="staticEmail" value="02180-060">
            </div>
        </div>

        <div class="form-group row">
            <label for="staticEmail" class="col-3 col-form-label">Número</label>
            <div class="col-9">
                <input type="text" class="form-control-plaintext" id="staticEmail" value="696">
            </div>
        </div>

        <div class="form-group row">
            <label for="staticEmail" class="col-4 col-form-label">Complemento</label>
            <div class="col-8">
                <input type="text" class="form-control-plaintext" id="staticEmail">
            </div>
        </div>

        <div class="form-group row">
            <label for="staticEmail" class="col-3 col-form-label">Cidade</label>
            <div class="col-9">
                <input type="text" class="form-control-plaintext" id="staticEmail" value="São bernado do campo">
            </div>
        </div>
        
        <div class="form-group row">
            <label for="staticEmail" class="col-3 col-form-label">Estado</label>
            <div class="col-9">
                <input type="text" class="form-control-plaintext" id="staticEmail" value="São Paulo">
            </div>
        </div>

        <div class="form-group row">
            <div class="col-12 text-right">
                <input type="submit" class="border-0 defaut-btn-gradient" value="Salvar informações">
            </div>
        </div>
    </form>
  </div>
</header>


<?php include("includes/script.php")?>
</body>
</html>