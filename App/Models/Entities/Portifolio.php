<?php

namespace App\Models\Entities;

use App\Core\Models;
use App\Models\Entities\PortifolioImage;
use App\Models\Entities\PortifolioType;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Portifolio
 *
 * @author oseas
 */
class Portifolio extends Models {
    protected $table = 'portifolio';
    protected $primaryKey = 'idportifolio';
    public $timestamps = false;
    protected $fillable = [ 'title', 'description', 'image', 'status', 'bgpage', 'site', 'video', 'facebook', 'instagram', 'behance', 'idtype'];
    protected $guarded = [];

    public function images(){
        return $this->hasMany(PortifolioImage::class, $this->primaryKey);
    }

    public function category()
    {
        return $this->belongsTo(PortifolioType::class, 'idtype');
    }

}
