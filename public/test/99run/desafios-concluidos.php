<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("includes/head.php")?>
</head>
<body>
<?php include("includes/menu-top-bar.php")?>
<?php include("includes/menu-side-bar.php")?>
<?php include("includes/menu-bottom.php")?>

<header id="header-desafio-concluido">
  <div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="box-info mb-4">
                <h1 class="title">Veja aqui seus<br>desafios concluídos</h1>
                <div class="bar-little"></div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <a href="detalhe-desafio.php" class="box-line">
                <div class="row">
                    <div class="col-2 align-self-center">
                        <i class="flaticon-quality"></i>
                    </div>

                    <div class="col-6 align-self-center">
                        <p class="challenge-name"> Desafio<br><span>Fenômenos naturais</span></p>
                    </div>

                    <div class="col-4 align-self-center">
                        <p class="km">10km</p>
                    </div>
                </div>
            </a>

            <a href="detalhe-desafio.php" class="box-line">
                <div class="row">
                    <div class="col-2 align-self-center">
                        <i class="flaticon-quality"></i>
                    </div>

                    <div class="col-6 align-self-center">
                        <p class="challenge-name"> Desafio<br><span>Fenômenos naturais</span></p>
                    </div>

                    <div class="col-4 align-self-center">
                        <p class="km">10km</p>
                    </div>
                </div>
            </a>

            <a href="detalhe-desafio.php" class="box-line">
                <div class="row">
                    <div class="col-2 align-self-center">
                        <i class="flaticon-quality"></i>
                    </div>

                    <div class="col-6 align-self-center">
                        <p class="challenge-name"> Desafio<br><span>Fenômenos naturais</span></p>
                    </div>

                    <div class="col-4 align-self-center">
                        <p class="km">10km</p>
                    </div>
                </div>
            </a>

            <a href="detalhe-desafio.php" class="box-line">
                <div class="row">
                    <div class="col-2 align-self-center">
                        <i class="flaticon-quality"></i>
                    </div>

                    <div class="col-6 align-self-center">
                        <p class="challenge-name"> Desafio<br><span>Fenômenos naturais</span></p>
                    </div>

                    <div class="col-4 align-self-center">
                        <p class="km">10km</p>
                    </div>
                </div>
            </a>
        </div>
    </div>
  </div>
</header>


<?php include("includes/script.php")?>
</body>
</html>