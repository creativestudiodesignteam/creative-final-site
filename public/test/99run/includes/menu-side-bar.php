<div class="page-wrapper chiller-theme">
  <nav id="sidebar" class="sidebar-wrapper">
    <div class="sidebar-content">
      <div class="sidebar-brand">
        <a href="#"><i class="flaticon-power"></i> Sair</a>
        <div id="close-sidebar">
          <i class="flaticon-right"></i>
        </div>
      </div>
      <div class="sidebar-header">
        <div class="demo">
            <span class="box-level font-weight-bold">18 <strong>level</strong></span>
        </div>
        <span class="text-center d-block text-uppercase user-name">Felipe Henrique</span>

        <div class="infos-run text-center">
            <i class="flaticon-speed"></i>
            <div class="km"><h2 class="runner-km">130.25 km<br><span>Corridos no total</span></h2></div>
        </div>

        <div class="incomplete-record">
            <div class="progress" style="height: 15px;">
                <div class="progress-bar" role="progressbar" style="width: 77%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <span class="label">Cadastro está 77% completo</span>

            <div class="float-left">
                <ul class="list-record">
                    <li><i class="flaticon-multiply"></i> <span>Sexo</span></li>
                    <li><i class="flaticon-multiply"></i> <span>Endereço</span></li>
                    <li><i class="flaticon-multiply"></i> <span>Idade</span></li>
                </ul>
            </div>
            <div class="float-right mt-4">
                <a href="endereco.php" class="defaut-btn-gradient">Finalizar cadastro</a>
            </div>
            
        </div>
      </div>
      
      <div class="sidebar-menu mt-5">
        <ul>
          <li>
            <a href="carrinho.php">
              <i class="flaticon-shopping-cart"></i>
              <span>Meus pedidos</span>
            </a>
          </li>
          <li>
            <a href="desafios-concluidos.php">
              <i class="flaticon-winner"></i>
              <span>Desafios Concluídos</span>
            </a>
          </li>
          <li>
            <a href="contato.php">
              <i class="flaticon-headphones"></i>
              <span>Central de ajuda</span>
            </a>
          </li>
          <li>
            <a href="ganhe-6.php">
              <i class="flaticon-tag"></i>
              <span>Cupom de desconto</span>
            </a>
          </li>
          <li>
            <a href="ganhe-6.php">
              <i class="flaticon-piggy-bank"></i>
              <span>Ganhe R$6,00</span>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <!-- <main class="page-content">
    
  </main> -->
</div>