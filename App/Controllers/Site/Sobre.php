<?php

namespace App\Controllers\Site;

use \App\Controllers\Site\Common;
use App\Models\Entities\SiteInstitucional;
use App\Models\Entities\SiteServices;

/**
 * Description of Sobre
 *
 * @author oseas¹
 */
class Sobre extends Common{

    protected $pagelink = '';

    public function index() {
        $this->start_session();
        $this->view->page = 'header-home';
        $this->view->obj = SiteInstitucional::where('ativo', '=', 'a')->first();
        $this->view->services = SiteServices::where('status', '=', 'a')->get();
        $this->view->titlehead =  $this->view->obj->titulo;
        
        $this->render('index', $this->folder, $this->page);
    }
}
