<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("includes/head.php")?>
</head>
<body>
<?php include("includes/menu-top-bar.php")?>
<?php include("includes/menu-side-bar.php")?>
<?php include("includes/menu-bottom.php")?>

<header id="header-boleto">
  <div class="container-fluid">
    <div class="row">
        <div class="col-12 text-center">
            <div class="box-info">
                <h1 class="title"><span>Status do pedido: Aguardando pagamento<br>Valor da compra:</span><br>R$ 76,00</h1>
            </div>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-6"><span class="ticket-code">Nº Boleto:</span></div>
        <div class="col-6 text-right"><a href="#." class="open-code">Abrir boleto</a></div>
    </div>
    <div class="card mb-3 mt-2">
        <div class="row">
          <div class="col-12 align-self-center text-center">
            <span>03399.63290 64000.000006 00125.201020 4 56140000017832</span>
          </div>
        </div>
    </div>

    <div class="row mt-3">
        <div class="col-7 text-center">
            <a href="#" class="defaut-btn-gradient -btn mt-2 d-inline-block w-100">Enviar para e-mail</a>
        </div>

        <div class="col-5 text-center">
            <a href="#" class="defaut-btn-gradient -btn mt-2 d-inline-block w-100">Gerar 2º via</a>
        </div>
    </div>

    <div class="row">
        <div class="col-12 text-center">
            <div class="box-info-2">
                <div class="bar-big mt-5"></div>
                <h1 class="title"><span>Olá Felipe,</span><br>gostaria de colocar mais
                medalhas em seu pedido?</h1>
                <a href="loja.php" class="defaut-btn-gradient mt-2 d-inline-block">Refazer pedido</a>
            </div>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-12 text-center">
            <div class="box-info-2">
                <i class="flaticon-shopping-cart"></i>
                <h1 class="title"><span>Aqui está,</span><br>o resumo do seu pedido</h1>
            </div>
        </div>
        <div class="col-12">
            <div class="slider-resumo owl-carousel owl-theme">
                <div class="item">
                    <div class="row">
                        <div class="col-6">
                            <a href="produto.php">
                                <figure class="card card-product">
                                    <div class="img-wrap text-center">
                                        <img src="assets/images/arqueiro-site.png" class="img-fluid" width="100px" />
                                    </div>
                                    <figcaption class="info-wrap">
                                        <h5 class="title m-0">Medalha 6º Guerreiro</h5>
                                    </figcaption>
                                    <div class="bottom-wrap">
                                        <div class="price-wrap h5">
                                            <span class="price-new">R$36,00</span>
                                        </div> 
                                    </div>
                                </figure>
                            </a>
                        </div>

                        <div class="col-6">
                            <a href="produto.php">
                                <figure class="card card-product">
                                    <div class="img-wrap text-center">
                                        <img src="assets/images/arqueiro-site.png" class="img-fluid" width="100px" />
                                    </div>
                                    <figcaption class="info-wrap">
                                        <h5 class="title m-0">Medalha 6º Guerreiro</h5>
                                    </figcaption>
                                    <div class="bottom-wrap">
                                        <div class="price-wrap h5">
                                            <span class="price-new">R$36,00</span>
                                        </div> 
                                    </div>
                                </figure>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="item">
                    <div class="row">
                        <div class="col-6">
                            <a href="produto.php">
                                <figure class="card card-product">
                                    <div class="img-wrap text-center">
                                        <img src="assets/images/arqueiro-site.png" class="img-fluid" width="100px" />
                                    </div>
                                    <figcaption class="info-wrap">
                                        <h5 class="title m-0">Medalha 6º Guerreiro</h5>
                                    </figcaption>
                                    <div class="bottom-wrap">
                                        <div class="price-wrap h5">
                                            <span class="price-new">R$36,00</span>
                                        </div> 
                                    </div>
                                </figure>
                            </a>
                        </div>

                        <div class="col-6">
                            <a href="produto.php">
                                <figure class="card card-product">
                                    <div class="img-wrap text-center">
                                        <img src="assets/images/arqueiro-site.png" class="img-fluid" width="100px" />
                                    </div>
                                    <figcaption class="info-wrap">
                                        <h5 class="title m-0">Medalha 6º Guerreiro</h5>
                                    </figcaption>
                                    <div class="bottom-wrap">
                                        <div class="price-wrap h5">
                                            <span class="price-new">R$36,00</span>
                                        </div> 
                                    </div>
                                </figure>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="item">
                    <div class="row">
                        <div class="col-6">
                            <a href="produto.php">
                                <figure class="card card-product">
                                    <div class="img-wrap text-center">
                                        <img src="assets/images/arqueiro-site.png" class="img-fluid" width="100px" />
                                    </div>
                                    <figcaption class="info-wrap">
                                        <h5 class="title m-0">Medalha 6º Guerreiro</h5>
                                    </figcaption>
                                    <div class="bottom-wrap">
                                        <div class="price-wrap h5">
                                            <span class="price-new">R$36,00</span>
                                        </div> 
                                    </div>
                                </figure>
                            </a>
                        </div>

                        <div class="col-6">
                            <a href="produto.php">
                                <figure class="card card-product">
                                    <div class="img-wrap text-center">
                                        <img src="assets/images/arqueiro-site.png" class="img-fluid" width="100px" />
                                    </div>
                                    <figcaption class="info-wrap">
                                        <h5 class="title m-0">Medalha 6º Guerreiro</h5>
                                    </figcaption>
                                    <div class="bottom-wrap">
                                        <div class="price-wrap h5">
                                            <span class="price-new">R$36,00</span>
                                        </div> 
                                    </div>
                                </figure>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row mt-3">
        <div class="col-12 text-center">
            <a href="forma-pagamento.php" class="defaut-btn-gradient mt-2 d-inline-block">Finalizar pedido</a>
        </div>
    </div>
  </div>

</header>
<?php include("includes/script.php")?>
</body>
</html>