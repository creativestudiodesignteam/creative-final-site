<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("includes/head.php")?>
</head>
<body>

<?php include("includes/menu-side-bar.php")?>
<?php include("includes/menu-bottom.php")?>

<header id="header-editar-usuario">
  <div class="d-flex justify-content-between menu-top">
      <div class="p-2 align-self-center">
          <a class="btn-back" href="#"><i class="material-icons">arrow_back</i> Voltar</a>
      </div>
      <div class="p-2 align-self-center text-center">
          <a href="#" class="peoples">Editar</a>
      </div>
      <div class="p-2 align-self-center">
          <ul class="list-unstyled list-inline m-0 list-icons-menu">
              <li class="list-inline-item"><a href="#" class="edit-icon"><img src="assets/images/avatar-secretaria.png"></a></li>
          </ul>
      </div>
  </div>
</header>

<section id="section-detalhe-card-01">
  <div class="container-fluid">
    <div class="row mt-4">
      <div class="col-12 text-center align-self-center">
        <h4 class="title mt-0 mb-3">ou</h4>
      </div>
    </div>

    <div class="row mb-100">
      <div class="col-12 text-center">
      <form>
        <div class="form-row">
            <div class="form-group col-md-6">
            <input type="text" class="form-control" id="inputEmail4" placeholder="Digite seu nome">
            </div>
            <div class="form-group col-md-6">
            <input type="phone" class="form-control" id="inputPassword4" placeholder="Digite seu telefone">
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-6">
                <input type="text" class="form-control" id="inputEmail4" placeholder="Lembrete de nome">
            </div>
            <div class="form-group col-md-6">
                <select id="inputEstado" class="form-control">
                    <option selected>Assunto...</option>
                    <option>...</option>
                </select>
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-4">
                <select id="inputEstado" class="form-control">
                    <option selected>Detalhe...</option>
                    <option>...</option>
                </select>
            </div>
            <div class="form-group col-md-2">
                <select id="inputEstado" class="form-control">
                    <option selected>Urgência...</option>
                    <option>...</option>
                </select>
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-4">
                <select id="inputEstado" class="form-control">
                    <option selected>Status da conversa...</option>
                    <option>...</option>
                </select>
            </div>
            <div class="form-group col-md-2">
                <select id="inputEstado" class="form-control">
                    <option selected>Outra pessoa...</option>
                    <option>...</option>
                </select>
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-6">
            <input type="text" class="form-control" id="inputEmail4" placeholder="Valor">
            </div>
        </div>

        <button type="submit" class="defaut-btn-gradient2 border-0 ">Salvar</button>
        </form>
      </div>
    </div>
  </div>
</section>

<?php include("includes/script.php")?>
</body>
</html>