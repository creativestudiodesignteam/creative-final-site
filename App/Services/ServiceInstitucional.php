<?php
/**
 * Created by PhpStorm.
 * User: oseas
 * Date: 08/12/2018
 * Time: 00:38
 */

namespace App\Services;

use \App\Models\Entities\SiteInstitucional;


class ServiceInstitucional{

    public function create($request) {
        if($request){
            $obj = new SiteInstitucional();
            $this->save($request, $obj);

            $return = [];
            if($obj->idinstitucional <> ''){
                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel inserir o Institucional <strong>{$obj->titulo}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function update($request) {
        if($request){
            $obj = SiteInstitucional::find($request['id']);
            $this->save($request, $obj);

            $return = [];
            if($obj->idinstitucional <> ''){
                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel atualizar o Institucional <strong>{$obj->titulo}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function destroy($id){
        $obj = SiteInstitucional::find($id);
        $obj->ativo = 'd';
        $resp = $obj->save();

        $return = [];

        if($resp){
            $return['success'] = true;
        }else{
            $return['success'] = false;
        }

        return $return;
    }

    public function save($request, $obj){
        $obj->titulo = $request['titulo'];
        $obj->texto = $request['texto'];
        if(isset($request['imagem'])){
            $obj->imagem = $request['imagem'];
        }
        $obj->ativo = $request['ativo'];
        $obj->resumo = $request['resumo'];

        $obj->save();

    }

}
