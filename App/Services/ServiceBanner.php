<?php
/**
 * Created by PhpStorm.
 * User: oseas
 * Date: 08/12/2018
 * Time: 00:38
 */

namespace App\Services;

use \App\Models\Entities\SiteBanner;


class ServiceBanner{

    public function create($request) {
        if($request){
            $obj = new SiteBanner();
            $this->save($request, $obj);

            $return = [];
            if($obj->idbanner <> ''){
                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel inserir o Banner <strong>{$obj->text1}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function update($request) {
        if($request){
            $obj = SiteBanner::find($request['id']);
            $this->save($request, $obj);

            $return = [];
            if($obj->idbanner <> ''){

                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel atualizar o Banner <strong>{$obj->text1}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function destroy($id){
        $departamento = SiteBanner::find($id);
        $departamento->status = 'd';
        $resp = $departamento->save();

        $return = [];

        if($resp){
            $return['success'] = true;
        }else{
            $return['success'] = false;
        }

        return $return;
    }

    public function save($request, $obj){
        $obj->text = $request['text'];
        $obj->description = $request['description'];
        $obj->link = $request['link'];
        if(isset($request['image'])){
            $obj->image = $request['image'];
        }
        $obj->status = $request['status'];
        $obj->image_title = $request['image_title'];
        $obj->image_alt = $request['image_alt'];

        $obj->save();

    }

}
