<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("includes/head.php")?>
</head>
<body>
<?php include("includes/menu-top-bar.php")?>
<?php include("includes/menu-side-bar.php")?>
<?php include("includes/menu-bottom.php")?>

<header id="header-pagamento">
  <div class="container-fluid">
    <div class="row">
        <div class="col-12 text-center">
            <div class="box-info">
                <h1 class="title"><span>Valor da compra:</span><br>R$ 76,00</h1>
                <h2 class="subtitle">Aproveite!</h2>
            </div>
        </div>
    </div>

    <div class="card mb-3">
        <div class="row">
          <div class="col-12 align-self-center text-center">
            <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="customCheck1">
                <label class="custom-control-label -label-check" for="customCheck1">Adicionar por apenas R$2,99 o número de peito de cada medalha</label>
                <a href="#." class="video-btn" data-toggle="modal" data-src="https://www.youtube.com/embed/Jfrjeg26Cwk" data-target="#myModal">Ver vídeo clicando aqui</a>
            </div>
          </div>
        </div>

        <div class="row mt-3">
            <div class="col-6 align-self-center">
              <img src="assets/images/adesivo.jpeg" class="img-fluid"/>
            </div>

            <div class="col-6 align-self-center">
                <ul class="list-unstyled list-seal">
                    <li><span><i class="flaticon-tick"></i> 14 centímetros</span></li>
                    <li><span><i class="flaticon-tick"></i> Papel 90  gramas!</span></li>
                    <li><span><i class="flaticon-tick"></i> Impressão digital</span></li>
                    <li><span><i class="flaticon-tick"></i> Cores vivas</span></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 text-center mb-3">
            <h3 class="title-pay">Qual a forma de pagamento?</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-6 text-center">
            <a href="cartao-credito.php">
                <div class="box">
                    <i class="flaticon-credit-card"></i>
                    <p class="text"><span>Em até 12x</span><br>No cartao de crédito</p>
                </div>
            </a>
        </div>

        <div class="col-6 text-center">
            <a href="boleto.php">
                <div class="box">
                    <i class="fas fa-barcode"></i>
                    <p class="text"><span>A vista no</span><br>boleto</p>
                </div>
            </a>
        </div>
    </div>

    <div class="row mt-3">
        <div class="col-12 text-center">
            <a href="cartao-credito.php" class="defaut-btn-gradient mt-2 d-inline-block">pagar agora <i class="flaticon-right"></i></a>
        </div>
    </div>
  </div>

  
  <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>        
            <!-- 16:9 aspect ratio -->
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="" id="video"  allowscriptaccess="always" allow="autoplay"></iframe>
            </div>
        </div>
        </div>
    </div>
    </div> 
</header>
<?php include("includes/script.php")?>
</body>
</html>