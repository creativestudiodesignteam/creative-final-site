<?php

/**
 * Created by PhpStorm.
 * User: oseas
 * Date: 08/12/2018
 * Time: 00:38
 */

namespace App\Services;

use \App\Models\Entities\Portifolio;


class ServicePortifolio
{

    public function create($request)
    {
        if ($request) {
            $obj = new Portifolio();
            $this->save($request, $obj);

            $return = [];
            if ($obj->idportifolio <> '') {
                $return['success'] = true;
                $return['data'] = $obj;
            } else {
                $return['success'] = false;
                $return['message'] = "Não foi possivel inserir a Portifolio <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function update($request)
    {
        if ($request) {
            $obj = Portifolio::find($request['id']);
            $this->save($request, $obj);

            $return = [];
            if ($obj->idportifolio <> '') {

                $return['success'] = true;
                $return['data'] = $obj;
            } else {
                $return['success'] = false;
                $return['message'] = "Não foi possivel atualizar a Portifolio <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function destroy($id)
    {
        $departamento = Portifolio::find($id);
        $departamento->status = 'd';
        $resp = $departamento->save();

        $return = [];

        if ($resp) {
            $return['success'] = true;
        } else {
            $return['success'] = false;
        }

        return $return;
    }

    public function save($request, $obj)
    { 
        $obj->title     = $request['title'];
        $obj->idtype     = $request['idtype'];
        $obj->description    = $request['description'];
        $obj->image   = $request['image'];
        $obj->status     = $request['status'];
        $obj->bgpage     = $request['bgpage'];
        $obj->site = $request['site'];
        $obj->video     = $request['video'];
        $obj->facebook    = $request['facebook'];
        $obj->instagram      = $request['instagram'];
        $obj->behance      = $request['behance'];
        $obj->porder      = $request['porder'];

        $obj->save();
    }
}
