<?php

namespace App\Models\Entities;

use App\Core\Models;

/**
 * Description of Arquivos
 *
 * @author oseas
 */
class Team extends Models
{
    protected $table = 'site_team';
    protected $primaryKey = 'idteam';
    public $timestamps = false;
    protected $fillable = [ 'image', 'title', 'status', 'text', 'certificates', 'cro', 'cv', 'idtype'];
    protected $guarded = [];

}
