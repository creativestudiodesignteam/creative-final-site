<?php

namespace App\Controllers\Site;

use \App\Controllers\Site\Common;
use App\Models\Entities\Portifolio;
use App\Models\Entities\PortifolioType;
/**
 * Description of Portifolios
 *
 * @author oseas¹
 */
class Portifolios extends Common{

    protected $pagelink = '';

    public function index() {
        $this->start_session();
        $this->view->page = 'header-home';
        $this->view->titlehead = 'Portifolios';
        $this->view->portifolio = PortifolioType::where('status', '=', 'a')->get();
        
        $this->render('index', $this->folder, $this->page);
    }

    public function post($text, $id){
        $this->start_session();
        $this->view->obj = Portifolio::find($id);

        $this->view->next = Portifolio::where('idportifolio', '>', $id)->first();
        $this->view->prev = Portifolio::where('idportifolio', '<', $id)->first();

        $this->view->page = 'header-home';
        $this->view->titlehead = $this->view->obj->title.' - Portifolios';
        $this->render('post', $this->folder, $this->page);

    }

}
