<!-- Modal Mensagem -->
<div class="modal fade" id="modal-mensagem" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="d-flex justify-content-between menu-top">
            <div class="pr-0 align-self-center">
                <ul class="list-unstyled list-inline m-0 list-icons-menu">
                    <li class="list-inline-item"><a href="#" class="edit-icon"><img src="assets/images/avatar-secretaria.png"></a></li>
                </ul>
            </div>
            <div class="pl-0 align-self-center text-left">
                <a href="#" class="secretaria">Sofia<br><span class="secretaria">Secretária virtual</span></a>
            </div>
            
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h4 class="title-header mt-2 mb-3">E ai Felipe, você acabou de
        falar com o <span class="bg-text-gradient">Oséas Moreto</span></h4>

        <p class="text-modal">
             E agora, qual o próximo passo na conversa
            com o <strong>Oséas Moreto?</strong>
        </p>

        <a href="#" class="defaut-btn-gradient2 mb-1 border-0 d-block text-center">Nada mais está tudo resolvido</a>
        <a href="#" class="defaut-btn-gradient3 mb-1 border-0 d-block text-center">Esquece não vou mais falar</a>
        <a href="#" class="defaut-btn-gradient4 mb-1 border-0 d-block text-center">Me lembre de chamar Oséas dia...</a>

        <p class="mt-3">
            Mais opções:
            <a href="#" class="defaut-btn-gradient2 mb-1 border-0 d-block text-center">Mudar responsável</a>
        </p>
      </div>
    </div>
  </div>
</div>


<!-- Modal Lembrete -->
<div class="modal fade" id="modal-lembrete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="d-flex justify-content-between menu-top">
            <div class="pr-0 align-self-center">
                <ul class="list-unstyled list-inline m-0 list-icons-menu">
                    <li class="list-inline-item"><a href="#" class="edit-icon"><img src="assets/images/avatar-secretaria.png"></a></li>
                </ul>
            </div>
            <div class="pl-0 align-self-center text-left">
                <a href="#" class="secretaria">Sofia<br><span class="secretaria">Secretária virtual</span></a>
            </div>
            
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h4 class="title-header mt-2 mb-3">Beleza! Felipe que dia você quer que eu <span class="bg-text-gradient">lembre você</span> ? </h4>

        <form class="text-center">
        <div class="form-row">
            <div class="form-group col-md-6">
            <input type="date" class="form-control" id="inputEmail4" placeholder="Digite seu nome">
            </div>
            <div class="form-group col-md-6">
            <input type="hour" class="form-control" id="inputPassword4" placeholder="digite um horário">
            </div>
        </div>

        <button type="submit" class="defaut-btn-gradient2 border-0 ">Continuar</button>
        </form>
    
      </div>
    </div>
  </div>
</div>

<!-- Modal Lembrete Descrição -->
<div class="modal fade" id="modal-lembrete-descricao" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="d-flex justify-content-between menu-top">
            <div class="pr-0 align-self-center">
                <ul class="list-unstyled list-inline m-0 list-icons-menu">
                    <li class="list-inline-item"><a href="#" class="edit-icon"><img src="assets/images/avatar-secretaria.png"></a></li>
                </ul>
            </div>
            <div class="pl-0 align-self-center text-left">
                <a href="#" class="secretaria">Sofia<br><span class="secretaria">Secretária virtual</span></a>
            </div>
            
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h4 class="title-header mt-2 mb-3">Combinado!! Você tem alguma coisa em específico para falar com o <span class="bg-text-gradient">Oséas</span> ? </h4>

        <form class="text-center">
        <div class="form-row">
            <div class="form-group col-12">
                <textarea name="status" rows="3" class="form-control w-100" placeholder="Digite aqui"></textarea>
            </div>
        </div>

        <button type="submit" class="defaut-btn-gradient2 border-0 ">Continuar</button>
        </form>
    
      </div>
    </div>
  </div>
</div>

<!-- Modal Final 1 -->
<div class="modal fade" id="modal-final-1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="d-flex justify-content-between menu-top">
            <div class="pr-0 align-self-center">
                <ul class="list-unstyled list-inline m-0 list-icons-menu">
                    <li class="list-inline-item"><a href="#" class="edit-icon"><img src="assets/images/avatar-secretaria.png"></a></li>
                </ul>
            </div>
            <div class="pl-0 align-self-center text-left">
                <a href="#" class="secretaria">Sofia<br><span class="secretaria">Secretária virtual</span></a>
            </div>
            
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h4 class="title-header mt-2 mb-3">Pronto <span class="bg-text-gradient">Felipe</span></h4>
        <p class="mb-0">Na Quinta, 20/08/2019 eu te aviso de:</p>
        <h4 class="title-header mt-2 mb-3">“Ver se ele já assinou o contrato”</h4>

        <div class="text-center">
            <button type="submit" class="defaut-btn-gradient2 border-0 ">Continuar</button>
        </div>
        <div class="row">
            <div class="col-12 text-center">
                <a href="#" class="btn-decoration mt-2">Editar data ou lembrete clique aqui</a>
            </div>
        </div>
    
      </div>
    </div>
  </div>
</div>

<!-- Modal Final 2 -->
<div class="modal fade" id="modal-final-2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="d-flex justify-content-between menu-top">
           <!--  <div class="pr-0 align-self-center">
                <ul class="list-unstyled list-inline m-0 list-icons-menu">
                    <li class="list-inline-item"><a href="#" class="edit-icon"><img src="assets/images/avatar-secretaria.png"></a></li>
                </ul>
            </div> -->
            <div class="pl-0 align-self-center text-left">
                <h5 class="title-header mt-2 mb-3">Tarefa de <span class="bg-text-gradient">Felipe</span></h5>
            </div>
            
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <!-- <h4 class="title-header mt-2 mb-3">Pronto <span class="bg-text-gradient">Felipe</span></h4>
        <p class="mb-0">Na Quinta, 20/08/2019 eu te aviso de:</p>
        <h4 class="title-header mt-2 mb-3">“Ver se ele já assinou o contrato”</h4> -->

        <form class="text-center">
        <div class="form-row">
        <div class="form-group col-md-6">
            <input type="text" class="form-control" id="inputPassword4" placeholder="Tarefa">
            </div>
            <div class="form-group col-md-6">
                <select id="inputEstado" class="form-control">
                    <option selected>Não concluida...</option>
                    <option>...</option>
                </select>
            </div>
            <div class="form-group col-md-6">
            <input type="date" class="form-control" id="inputPassword4" placeholder="Data de entrega">
            </div>
        </div>

        <button type="submit" class="defaut-btn-gradient2 border-0 ">Continuar</button>
        </form>

        <div class="row">
            <div class="col-12 text-center">
                <a href="#" class="btn-decoration mt-2">Editar data ou lembrete clique aqui</a>
            </div>
        </div>
    
      </div>
    </div>
  </div>
</div>