<?php

namespace App\Models\Entities;

use App\Core\Models;
use App\Models\Entities\AccessGroup;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Arquivos
 *
 * @author oseas
 */
class AccessMenu extends Models {
    protected $table = 'access_menu';
    protected $primaryKey = 'idmenu';
    public $timestamps = false;
    protected $fillable = ['title', 'status', 'function', 'level', 'order', 'icon'];
    protected $guarded = [];

    public function groups(){
        return $this->belongsToMany(AccessGroup::class, 'access_groupxmenu', $this->primaryKey, 'idgroup');
    }
}
