<?php

namespace App\Models\Entities;

use App\Core\Models;
use App\Models\Entities\AccessGroup;
use App\Models\Entities\Blog;
use App\Models\Entities\Video;

/**
 * Description of Arquivos
 *
 * @author oseas
 */
class AccessUser extends Models
{
    protected $table = 'access_user';
    protected $primaryKey = 'iduser';
    public $timestamps = false;
    protected $fillable = ['idgroup', 'user', 'email', 'password', 'name', 'status', 'image', 'phone', 'date_create'];
    protected $guarded = [];

    public function group()
    {
        return $this->belongsTo(AccessGroup::class, 'idgroup');
    }

    public function blogs(){
        return $this->hasMany(Blog::class, $this->primaryKey);
    }

    public function videos(){
        return $this->hasMany(Video::class, $this->primaryKey);
    }
}
