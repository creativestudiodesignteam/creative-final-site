jQuery(function ($) {

  $(".sidebar-dropdown > a").click(function() {
  $(".sidebar-submenu").slideUp(200);
  if (
    $(this)
      .parent()
      .hasClass("active")
  ) {
    $(".sidebar-dropdown").removeClass("active");
    $(this)
      .parent()
      .removeClass("active");
  } else {
    $(".sidebar-dropdown").removeClass("active");
    $(this)
      .next(".sidebar-submenu")
      .slideDown(200);
    $(this)
      .parent()
      .addClass("active");
  }
});

$("#close-sidebar").click(function() {
  $(".page-wrapper").removeClass("toggled");
});
$("#show-sidebar").click(function() {
  $(".page-wrapper").addClass("toggled");
});
});

var myProgress = $(".demo").imgProgress(
  {
      img_url: "assets/images/menu/avatar.jpg",
      size: 150,
      backgroundColor: '#4a4a4a',
      foregroundColor: '#ea7214',
      percent: 23
  }
);

$(".demo").imgProgressTo(70);
/* $( "#animateTo" ).click(function() {
}); */

/* $( "#update" ).click(function() {
  $(".demo").imgProgressUpdateOf(30);
});

$( "#get" ).click(function() {
  $(".demo").imgProgressGet();
}); */

$(window).scroll(function() {    
  var scroll = $(window).scrollTop();

  if (scroll >= 10) {
      $(".menu-top").addClass("fixed-top");
  } else {
      $(".menu-top").removeClass("fixed-top");
  }
});

$('.slider-challenges').owlCarousel({
    loop:false,
    nav: true,
    margin:10,
    autoplay:true,
    autoplayTimeout:5000,
    navText: ["<i class='flaticon-right'></i>","<i class='flaticon-right'></i>"],
    responsive:{
        400:{
            items:1
        },

        300:{
          items:1,
          nav: false
      }
    }
});

$('.slider-product').owlCarousel({
  loop:true,
  nav: true,
  margin:10,
  autoplay:true,
  autoplayTimeout:5000,
  navText: ["<i class='flaticon-right'></i>","<i class='flaticon-right'></i>"],
  responsive:{
      400:{
          items:1
      },

      300:{
        items:1,
        nav: false
    }
  }
});

$('.slider-product-page').owlCarousel({
  loop:true,
  nav: false,
  dots: true,
  margin:10,
  autoplay:false,
  autoplayTimeout:5000,
  responsive:{
      400:{
          items:1
      },

      300:{
        items:1,
        nav: false
    }
  }
});

$('.slider-depositions').owlCarousel({
  loop:true,
  nav: false,
  dots: true,
  margin:10,
  autoplay:false,
  autoplayTimeout:5000,
  responsive:{
      400:{
          items:1
      },

      300:{
        items:1,
        nav: false
    }
  }
});

$('.slider-resumo').owlCarousel({
  loop:true,
  nav: false,
  dots: true,
  margin:10,
  autoplay:false,
  autoplayTimeout:5000,
  responsive:{
      400:{
          items:1
      },

      300:{
        items:1,
        nav: false
    }
  }
});

$(document).ready(function() {

  // Gets the video src from the data-src on each button
  
  var $videoSrc;  
  $('.video-btn').click(function() {
      $videoSrc = $(this).data( "src" );
  });
  console.log($videoSrc);
    
  // when the modal is opened autoplay it  
  $('#myModal').on('shown.bs.modal', function (e) {
      
  // set the video src to autoplay and not to show related video. Youtube related video is like a box of chocolates... you never know what you're gonna get
  $("#video").attr('src',$videoSrc + "?autoplay=1&amp;modestbranding=1&amp;showinfo=0" ); 
  })
    
  // stop playing the youtube video when I close the modal
  $('#myModal').on('hide.bs.modal', function (e) {
      // a poor man's stop video
      $("#video").attr('src',$videoSrc); 
  }) 
    
  // document ready  
  });
  

  $("#getting-started")
  .countdown("2019/04/30", function(event) {
    $(this).text(
      event.strftime(' %H:%M:%S')
    );
  });


  var header = document.getElementById("kit");
var btns = header.getElementsByClassName("challenges-bg");
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function() {
  var current = document.getElementsByClassName("active");
  current[0].className = current[0].className.replace(" active", "");
  this.className += " active";
  });
}

var header = document.getElementById("medal");
var btns = header.getElementsByClassName("medal-selection");
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function() {
  var current = document.getElementsByClassName("active");
  current[0].className = current[0].className.replace(" active", "");
  this.className += " active";
  });
}
  
  
function readURL(input) {
  if (input.files && input.files[0]) {

    var reader = new FileReader();

    reader.onload = function(e) {
      $('.image-upload-wrap').hide();

      $('.file-upload-image').attr('src', e.target.result);
      $('.file-upload-content').show();

      $('.image-title').html(input.files[0].name);
    };

    reader.readAsDataURL(input.files[0]);

  } else {
    removeUpload();
  }
}

function removeUpload() {
  $('.file-upload-input').replaceWith($('.file-upload-input').clone());
  $('.file-upload-content').hide();
  $('.image-upload-wrap').show();
}
$('.image-upload-wrap').bind('dragover', function () {
        $('.image-upload-wrap').addClass('image-dropping');
    });
    $('.image-upload-wrap').bind('dragleave', function () {
        $('.image-upload-wrap').removeClass('image-dropping');
});

var navItems = document.querySelectorAll(".mobile-bottom-nav__item");
navItems.forEach(function(e, i) {
	e.addEventListener("click", function(e) {
		navItems.forEach(function(e2, i2) {
			e2.classList.remove("mobile-bottom-nav__item--active");
		})
		this.classList.add("mobile-bottom-nav__item--active");
	});
});
