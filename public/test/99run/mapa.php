<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("includes/head.php")?>
</head>
<body>
<?php include("includes/menu-top-bar.php")?>
<?php include("includes/menu-side-bar.php")?>
<?php include("includes/menu-bottom.php")?>

<header id="header-mapa">
  <div class="container-fluid">
      <div class="row">
          <div class="col-12 text-center">
                <img src="assets/images/bg-mapa.jpg" class="img-fluid img-dynamic-upload"/>
          </div>
      </div>
    <div class="row">
        <div class="col-12 text-center">
            <div class="box-info">
                <div class="bar-big"></div>
                <h1 class="title"><span>olá Felipe</span><br>Preencha os campos com a distância, tempo
e com a data que você fez a corrida</h1>
            </div>
        </div>
    </div>

    <form class="form-address mt-4">
        <div class="form-group row bg-input">
            <label for="staticEmail" class="col-7 col-form-label"><i class="flaticon-speed"></i> Km corridos:</label>
            <div class="col-5">
                <input type="text" class="form-control-plaintext" id="staticEmail">
            </div>
        </div>

        <div class="form-group row bg-input">
            <label for="staticEmail" class="col-5 col-form-label"><i class="flaticon-stopwatch"></i> Tempo:</label>
            <div class="col-7">
                <input type="text" class="form-control-plaintext" id="staticEmail">
            </div>
        </div>
        
        <div class="form-group row bg-input">
            <label for="staticEmail" class="col-4 col-form-label"><i class="flaticon-calendar"></i> Data:</label>
            <div class="col-8">
                <input type="text" class="form-control-plaintext" id="staticEmail">
            </div>
        </div>

        <div class="form-group row">
            <div class="col-12 text-center">
                <input type="submit" class="border-0 defaut-btn-gradient" value="Finalizar Validação">
            </div>
        </div>
    </form>
  </div>
</header>


<?php include("includes/script.php")?>
</body>
</html>