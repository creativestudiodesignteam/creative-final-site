<nav class="mobile-bottom-nav">
	<div class="mobile-bottom-nav__item">
		<div class="mobile-bottom-nav__item-content">
    <i class="material-icons">layers</i>
		
		</div>		
	</div>
	<div class="mobile-bottom-nav__item mobile-bottom-nav__item--active">		
		<div class="mobile-bottom-nav__item-content">
			<i class="material-icons">message</i>
		
		</div>
	</div>
	<div class="mobile-bottom-nav__item">
		<div class="mobile-bottom-nav__item-content">
			<i class="material-icons">keyboard</i>
			
		</div>		
	</div>
	
	<div class="mobile-bottom-nav__item">
		<div class="mobile-bottom-nav__item-content">
			<i class="material-icons">folder</i>
			
		</div>		
	</div>
</nav>