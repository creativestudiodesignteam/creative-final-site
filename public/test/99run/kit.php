<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("includes/head.php")?>
</head>
<body>
<?php include("includes/menu-top-bar.php")?>
<?php include("includes/menu-side-bar.php")?>
<?php include("includes/menu-bottom.php")?>

<header id="header-kit">
  <div class="container-fluid">
    <div class="row">
        <div class="col-12 text-center">
            <div class="box-info mb-4">
                <div class="bar-big"></div>
                <h1 class="title">21km<br><a href="inscricao.php">Mudar distância</a></h1>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12" id="kit">
            <div class="box">
                <img src="assets/images/kit-01.jpg" alt="Avatar" class="image" style="width:100%">
                <div class="infos">
                    <span class="badget-price -aux-price"><strong>R$19,90</strong></span>
                    <h5 class="title">Kit básico</h5>
                    <a href="produto.php" class="defaut-btn-gradient">Ver detalhes <i class="flaticon-right"></i></a>
                </div>

                <div class="middle">
                    <a href="carrinho.php" class="button-cart"><i class="flaticon-tick"></i> Escolhido!<br><span>Adicione agora</span><br><span>clique aqui</span></a>
                </div>
            </div>

            <div class="box">
                <img src="assets/images/kit-01.jpg" alt="Avatar" class="image" style="width:100%">
                <div class="infos">
                    <span class="badget-price -aux-price"><strong>R$29,90</strong></span>
                    <h5 class="title">Kit metal</h5>
                    <a href="produto.php" class="defaut-btn-gradient">Ver detalhes <i class="flaticon-right"></i></a>
                </div>

                <div class="middle">
                    <a href="carrinho.php" class="button-cart"><i class="flaticon-tick"></i> Escolhido!<br><span>Adicione agora</span><br><span>clique aqui</span></a>
                </div>
            </div>

            <div class="box">
                <img src="assets/images/kit-01.jpg" alt="Avatar" class="image" style="width:100%">
                <div class="infos">
                    <span class="badget-price -aux-price"><strong>R$18,90</strong></span>
                    <h5 class="title">Kit premium</h5>
                    <a href="produto.php" class="defaut-btn-gradient">Ver detalhes <i class="flaticon-right"></i></a>
                </div>

                <div class="middle">
                    <a href="carrinho.php" class="button-cart"><i class="flaticon-tick"></i> Escolhido!<br><span>Adicione agora</span><br><span>clique aqui</span></a>
                </div>
            </div>

            <!-- <div class="box">
                <span class="badget-price -aux-price"><strong>R$29,90</strong></span>
                <img src="assets/images/kit-01.jpg" class="img-fluid challenges-bg"/>
                <div class="infos text-center">
                    <h6 class="title">Kit metal</h6>
                    <a href="produto.php" class="defaut-btn-gradient">ver detalhes <i class="flaticon-right"></i></a>
                </div>
            </div>

            <div class="box">
                <span class="badget-price -aux-price"><strong>R$39,90</strong></span>
                <img src="assets/images/kit-01.jpg" class="img-fluid challenges-bg"/>
                <div class="infos text-center">
                    <h6 class="title">Kit premium</h6>
                    <a href="produto.php" class="defaut-btn-gradient">ver detalhes <i class="flaticon-right"></i></a>
                </div>
            </div> -->
        </div>
    </div> 
    
    <div class="row">
        <div class="col-12 text-center mt-4">
            <a href="carrinho.php" class="defaut-btn-gradient">Inscreva-se no desafio <i class="flaticon-right"></i></a>
        </div>
    </div>
  </div>
</header>


<?php include("includes/script.php")?>
</body>
</html>