<?php

namespace App\Models\Entities;

use App\Core\Models;
use App\Models\Entities\Portifolio;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Arquivos
 *
 * @author oseas
 */
class PortifolioType extends Models {
    protected $table = 'portifolio_type';
    protected $primaryKey = 'idtype';
    public $timestamps = false;
    protected $fillable = ['title', 'status'];
    protected $guarded = [];

    public function portifolios()
    {
        return $this->hasMany(Portifolio::class, $this->primaryKey);
    }


}
