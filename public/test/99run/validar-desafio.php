<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("includes/head.php")?>
</head>
<body>
<?php include("includes/menu-top-bar.php")?>
<?php include("includes/menu-side-bar.php")?>
<?php include("includes/menu-bottom.php")?>

<header id="header-validar-desafio">
  <div class="container-fluid">
    <div class="row">
        <div class="col-12 text-center">
            <div class="box-info">
                <div class="bar-big"></div>
                <h1 class="title"><span>Olá Felipe,</span><br>Você esta inscrito no desafio elementos 10km</h1>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 text-center">
            <div class="box-info-2">
                <h1 class="title"><span>meta</span><br>do desafio</h1>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-6 text-center align-self-center">
            <div class="box-challenges">
                <i class="flaticon-stopwatch timer"></i>
                <p id="getting-started"></p>
                <span>tempo</span>
            </div>
        </div>

        <div class="col-6 text-center align-self-center">
            <div class="box-challenges">
                <i class="flaticon-speed"></i>
                <p>12.0 km</p>
                <span>Correr</span>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 text-center">
            <div class="box-info2">
                <div class="bar-big"></div>
                <h1 class="title">Saiba seus próximos passos<br><span>o que você utiliza para correr</h1>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-4 text-center">
            <a href="celuar.php">
                <div class="box-pass" data-mh="my-group">
                    <i class="flaticon-smartphone"></i>
                </div>
                <p class="title-pass">Celuar</p>
            </a>
        </div>

        <div class="col-4 text-center">
            <a href="gps.php">
                <div class="box-pass" data-mh="my-group">
                    <i class="flaticon-smartwatch"></i>
                </div>
                <p class="title-pass">Relogio GPS</p>
            </a>
        </div>

        <div class="col-4 text-center">
            <a href="esteira.php">
                <div class="box-pass" data-mh="my-group">
                    <i class="flaticon-treadmill"></i>
                </div>
                <p class="title-pass">Esteira</p>
            </a>
        </div>
    </div>
  </div>
</header>


<?php include("includes/script.php")?>
</body>
</html>