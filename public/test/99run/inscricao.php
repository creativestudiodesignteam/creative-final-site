<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("includes/head.php")?>
</head>
<body>
<?php include("includes/menu-top-bar.php")?>
<?php include("includes/menu-side-bar.php")?>
<?php include("includes/menu-bottom.php")?>

<header id="header-inscricao">
  <div class="container-fluid">
  <div class="row">
        <div class="col-12 text-center">
            <div class="box-info">
                <div class="bar-big"></div>
                <h1 class="title"><span>Parabéns Felipe!</span><br>Agora falta pouco para Encarar o seu desafio</h1>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 text-center">
            <div class="box-medal">
                <img src="assets/images/medal.png" width="200" alt="img-fluid" />
                <div class="circle"></div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 text-center">
        <div class="param param-inline">
            <h6 class="title">Escolha a distência</h6>
            <div class="bar-little"></div>

            <div class="btn-group btn-group-toggle sizes mt-4" data-toggle="buttons">
                <label class="btn btn-size active">
                    <input type="radio" name="options" id="option1" autocomplete="off" checked> 3km
                </label>

                <label class="btn btn-size">
                    <input type="radio" name="options" id="option2" autocomplete="off"> 5km
                </label>

                <label class="btn btn-size">
                    <input type="radio" name="options" id="option3" autocomplete="off"> 10km
                </label>

                <label class="btn btn-size">
                    <input type="radio" name="options" id="option4" autocomplete="off"> 21km
                </label>

                <label class="btn btn-size">
                    <input type="radio" name="options" id="option5" autocomplete="off"> 41km
                </label>
            </div>
        </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-12 text-center mt-4">
            <a href="kit.php" class="defaut-btn-gradient">Inscreva-se no desafio <i class="flaticon-right"></i></a>
        </div>
    </div>
  </div>
</header>

<?php include("includes/script.php")?>
</body>
</html>