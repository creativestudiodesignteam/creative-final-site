<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("includes/head.php")?>
</head>
<body>
<?php include("includes/menu-top-bar.php")?>
<?php include("includes/menu-side-bar.php")?>
<?php include("includes/menu-bottom.php")?>

<header id="header-strava">
  <div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="box">
                <img src="assets/images/fenomenos-naturais.jpg" class="img-fluid challenges-bg"/>
                <div class="infos">
                    <h6 class="title">Desafio<br><span>Fenêmenos naturais</span></h6>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-6 align-self-center">
            <div class="box-info">
                <h1 class="title">Importe sua corrida do strava</h1>
                <div class="bar-little"></div>
            </div>
        </div>

        <div class="col-6 align-self-center text-right">
            <a href="#" class="defaut-btn-gradient">Acessar strava</a>
        </div>
    </div>

    <div class="row">
        <div class="col-12 text-center">
            <p class="title-upload">Se preferir insira uma foto ou print screen</p>
        </div>
    </div>

    <div class="row">
        <div class="col-12 text-center">           
            <div class="file-upload">
                <div class="image-upload-wrap">
                    <input class="file-upload-input" type='file' onchange="readURL(this);" accept="image/*" />
                    <div class="drag-text">
                        <i class="flaticon-photo"></i>
                        <p>Escolha uma foto clicando aqui</p>
                    </div>
                </div>
                <div class="file-upload-content">
                    <img class="file-upload-image" src="#" alt="your image" />
                    <div class="image-title-wrap">
                    <button type="button" onclick="removeUpload()" class="remove-image">Remover <span class="image-title">Upar imagem</span></button>
                    </div>
                </div>
            </div>

            <a href="#" class="defaut-btn-gradient d-inline-block mt-4">Enviar</a>
        </div>
    </div>
  </div>
</header>


<?php include("includes/script.php")?>
</body>
</html>