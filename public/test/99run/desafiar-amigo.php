<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("includes/head.php")?>
</head>
<body>
<?php include("includes/menu-top-bar.php")?>
<?php include("includes/menu-side-bar.php")?>
<?php include("includes/menu-bottom.php")?>

<header id="header-deasfiar-amigo">
  <div class="container-fluid">
    <div class="row">
        <div class="col-12 text-center">
            <div class="box-info">
                <div class="bar-big"></div>
                <h1 class="title">
                    <span>Eita agora vai!!!</span><br>Você foi desafiado pelo daniel ludwig para o desafio
                    <br><span class="challenge-name">ice run 4km</span>
                </h1>
            </div>

            <img src="assets/images/logo-mascote.png" width="200" class="img-fluid"/>
            <p class="challenge">Corra 4km  em menos de 01 hora e 10min</p>
            <div class="mt-4">
                <a href="#" class="defaut-btn-gradient">Aceitar desafio</a>
                <a href="#" class="defaut-btn-outline ml-4">Recusar desafio</a>
            </div>
        </div>
    </div>
  </div>
</header>


<?php include("includes/script.php")?>
</body>
</html>